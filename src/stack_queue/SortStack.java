package stack_queue;

import java.rmi.Remote;
import java.util.Stack;

public class SortStack {

    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(4);
        stack.push(2);
        stack.push(1);
        stack.push(2);

//        System.out.println(stack.toString());
//        sort(stack);
//        System.out.println("result = " + stack.toString());

//        RecursiveSolution.sort(stack);
        sort2(stack);
        System.out.println(stack.toString());
    }


    /**
     * task  -> sort such that smallest is on top
     * <p>
     * approach -
     * 1. find max and frequency while popping from main stack and pushing to temp stack
     * 2. Push max frequency times on main stack and increment element sorted
     * 3. pop from temp stack and push to main stack discarding max item
     * 4. repeat process until all elements  sorted
     */
    public static void sort(Stack<Integer> stack) {
        if (stack.isEmpty() || stack.size() == 1)
            return;

        Stack<Integer> tempStack = new Stack<>();
        int elementSorted = 0;
        int max;
        int frequency;
        int number;

        while (true) {
            System.out.println(stack.toString());
//            System.out.println("temp - "+tempStack.toString());

            max = Integer.MIN_VALUE;
            frequency = 1;
            //pop from main and push to temp
            while (stack.size() != elementSorted) {
                number = stack.pop();
                if (number > max) {
                    max = number;
                    frequency = 1;
                } else if (number == max) {
                    frequency++;
                }

                //push to temp stack
                tempStack.push(number);
            }

            //push max respecting frequency to main stack
            for (int i = 0; i < frequency; i++) {
                stack.push(max);
                elementSorted++;
            }

            //pop from temp and push to main stack
            while (!tempStack.isEmpty()) {
                number = tempStack.pop();
                if (number != max)
                    stack.push(number);
            }

            //all element sorted
            if (elementSorted == stack.size())
                break;
        }

    }

    //optimisation on already sorted sequence
    public static void sort1(Stack<Integer> stack) {
        if (stack.isEmpty() || stack.size() == 1)
            return;

        Stack<Integer> tempStack = new Stack<>();
        int elementSorted = 0;
        int max = Integer.MIN_VALUE;
        int frequency = 0;
        int number = 0;
        boolean alreadySorted = false;
        int maxChanged = 0;

        while (true) {
            System.out.println(stack.toString());
//            System.out.println("temp - "+tempStack.toString());

            max = Integer.MIN_VALUE;
            frequency = 1;
            maxChanged = 0;
            //pop from main and push to temp
            while (stack.size() != elementSorted) {
                number = stack.pop();
                if (number > max) {
                    max = number;
                    frequency = 1;
                    maxChanged++;
                } else if (number == max) {
                    frequency++;
                    maxChanged++;
                }

                //push to temp stack
                tempStack.push(number);
            }

            //check if already sorted
            if (maxChanged == tempStack.size())
                alreadySorted = true;

            //push max respecting frequency to main stack
            for (int i = 0; i < frequency; i++) {
                stack.push(max);
                elementSorted++;
            }

            //pop from temp and push to main stack
            while (!tempStack.isEmpty()) {
                number = tempStack.pop();
                if (number != max)
                    stack.push(number);
            }

            //all element sorted
            if (alreadySorted || elementSorted == stack.size())
                break;
        }

    }

    //from ctci --o(n2) time and o(n) space
    public static void sort2(Stack<Integer> stack) {
        Stack<Integer> temp = new Stack<>();

        while (!stack.isEmpty()) {
            //insert each element in stack in sorted order in temp
            int num = stack.pop();
            while (!temp.isEmpty() && temp.peek()>num) {
                stack.push(temp.pop());
            }
            temp.push(num);
        }

        while (!temp.isEmpty()) {
            stack.push(temp.pop());
        }
    }

    //o(n2) time and o(n) space due to recursive calls
    static class RecursiveSolution {

        static boolean alreadySorted = false;

        public static void sort(Stack<Integer> stack) {
            int size = stack.size();
            for (int i = 0; i < size && !alreadySorted; i++) {
                System.out.println(stack.toString());
                pushMax(stack, i, Integer.MIN_VALUE, 1,0,0);
            }

        }

        private static int pushMax(Stack<Integer> stack, int elementSorted, int max, int frequency, int maxChanged,int depth) {
            if (stack.size() == elementSorted) {
                for (int i = 0; i < frequency; i++)
                    stack.push(max);
                if (maxChanged == depth)
                    alreadySorted = true;
                return max;
            }
            int num = stack.pop();
            if (num > max) {
                max = num;
                frequency = 1;
                maxChanged++;
            }
            else if (max == num) {
                maxChanged++;
                frequency++;
            }

            int temp = pushMax(stack, elementSorted, max, frequency,maxChanged,depth+1);

            if (num != temp)
                stack.push(num);

            return temp;
        }
    }

}
