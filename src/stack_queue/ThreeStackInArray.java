package stack_queue;

public class ThreeStackInArray {
    int[] stack;
    int stackSize;
    int stack1Top;
    int stack2Top;
    int stack3Top;

    public ThreeStackInArray(int size) {
        this.stack = new int[size];
        stackSize = size / 3;
        stack1Top = -1;
        stack2Top = stackSize-1;
        stack3Top = 2 * stackSize -1;
    }

    public void push(int item, int stackNumber) {
        //check the top
        switch (stackNumber) {
            case 1:
                if (stack1Top < stackNumber * stackSize) {
                    //insert
                    stack[++stack1Top] = item;
                    return;
                }
                break;

            case 2:
                if (stack2Top < stackNumber * stackSize) {
                    //insert
                    stack[++stack2Top] = item;
                    return;
                }
                break;

            case 3:
                if (stack3Top < stackNumber * stackSize) {
                    //insert
                    stack[++stack3Top] = item;
                    return;
                }
                break;
        }

        throw new StackOverflowError();
    }

    public int pop(int stackNumber) throws Exception {
        //check the top
        switch (stackNumber) {
            case 1:
                if (stack1Top >= (stackNumber-1) * stackSize) {
                    //pop
                    return stack[stack1Top--];
                }
                break;

            case 2:
                if (stack2Top >= (stackNumber-1) * stackSize) {
                    //pop
                    return stack[stack2Top--];
                }
                break;

            case 3:
                if (stack3Top >= (stackNumber-1) * stackSize) {
                    //pop
                    return stack[stack3Top--];
                }
                break;
        }

        throw new Exception("stack is empty");
    }

    public int peek(int stackNumber) throws Exception {
        //check the top
        switch (stackNumber) {
            case 1:
                if (stack1Top >= (stackNumber-1) * stackSize) {
                    return stack[stack1Top];
                }
                break;

            case 2:
                if (stack2Top >= (stackNumber-1) * stackSize) {
                    return stack[stack2Top];
                }
                break;

            case 3:
                if (stack3Top >= (stackNumber-1) * stackSize) {
                    return stack[stack3Top];
                }
                break;
        }

        throw new Exception("stack is empty");
    }

    public boolean isEmpty(int stackNumber) {
        switch (stackNumber) {
            case 1:
                return stack1Top <  (stackNumber-1) * stackSize;

            case 2:
                return stack2Top <  (stackNumber-1) * stackSize;

            case 3:
                return stack3Top <  (stackNumber-1) * stackSize;
        }
        return false;
    }

}
class  Driver {
    public static void main(String[] args) throws Exception {
        ThreeStackInArray stack = new ThreeStackInArray(9);
        System.out.println(stack.isEmpty(1));
        System.out.println(stack.isEmpty(2));
        System.out.println(stack.isEmpty(3));

        stack.push(1,1);
        stack.push(2,1);

        stack.push(3,2);
        stack.push(4,2);

        stack.push(5,3);
        stack.push(6,3);

        stack.push(7,1);
        stack.push(8,2);
        stack.push(9,3);

        System.out.println(stack.isEmpty(1));
        System.out.println(stack.isEmpty(2));
        System.out.println(stack.isEmpty(3));

        System.out.println(stack.pop(1));
        System.out.println(stack.pop(1));
        System.out.println(stack.pop(1));

        System.out.println(stack.pop(2));
        System.out.println(stack.pop(2));
        System.out.println(stack.pop(2));

        System.out.println(stack.pop(3));
        System.out.println(stack.pop(3));
        System.out.println(stack.pop(3));

        System.out.println(stack.isEmpty(1));
        System.out.println(stack.isEmpty(2));
        System.out.println(stack.isEmpty(3));

        stack.push(7,1);
        stack.push(8,2);
        stack.push(9,3);
    }
}