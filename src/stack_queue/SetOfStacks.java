package stack_queue;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class SetOfStacks {
    int capacityEach;
    List<Stack<Integer>> stacks;
    int currentStackIndex;

    public SetOfStacks(int capacityEach) {
        this.capacityEach = capacityEach;
        stacks = new ArrayList<>();
        currentStackIndex = 0;
    }

    public void push(int data) {
        if (stacks.isEmpty()) {
            stacks.add(new Stack<>());
            currentStackIndex = 0;
        }
        if (stacks.get(currentStackIndex).size() < capacityEach) {
            stacks.get(currentStackIndex).push(data);
        } else {
            //current stack full add new
            stacks.add(new Stack<>());
            currentStackIndex++;
            stacks.get(currentStackIndex).push(data);
        }
    }

    public int peek() throws Exception {
        if (stacks.size() == 0) throw new Exception("Stack is empty");

        return stacks.get(currentStackIndex).peek();
    }

    public int popAt(int stackIndex) throws Exception {
        //check whether sub-stack exists
        if (stackIndex - 1 <= currentStackIndex && stackIndex > 0) {
            int item = stacks.get(stackIndex - 1).pop();
            if (stacks.get(stackIndex - 1).size() == 0) {
                stacks.remove(stackIndex - 1);
                currentStackIndex--;
            }
            return item;
        } else throw new Exception("Out of bounds");
    }

    public int pop() throws Exception {
        if (stacks.size() == 0) throw new Exception("Stack is empty");
        int item = 0;
        if (currentStackIndex < stacks.size()) {
            item = stacks.get(currentStackIndex).pop();
            if (stacks.get(currentStackIndex).size() == 0) {
                stacks.remove(currentStackIndex);
                currentStackIndex--;
            }
        }
        return item;
    }

    public int popAtWithShift(int stackIndex) throws Exception {
        //check whether sub-stack exists
        if (stackIndex - 1 <= currentStackIndex && stackIndex > 0) {
            int item = stacks.get(stackIndex - 1).pop();

            //check for the last element only one stack is there
            if (!(stackIndex - 1 == currentStackIndex && stacks.get(currentStackIndex).isEmpty()))
                shift(stackIndex - 1, currentStackIndex, stacks.get(currentStackIndex).pop());

            if (stacks.get(currentStackIndex).isEmpty()) {
                stacks.remove(currentStackIndex);
                currentStackIndex--;
            }
            return item;
        } else throw new Exception("Out of bounds");
    }

    //substackindex is 0 based
    public void shift(int subStackIndex, int stackIndex, int item) {
        if (subStackIndex + 1 == stackIndex && stacks.get(stackIndex).isEmpty()) {
            stacks.get(subStackIndex).push(item);
            return;
        }

        if (stacks.get(stackIndex).isEmpty()) {
            // all popped
            shift(subStackIndex, stackIndex - 1, stacks.get(stackIndex - 1).pop());
        } else {
            shift(subStackIndex, stackIndex, stacks.get(stackIndex).pop());
        }

        if (stacks.get(stackIndex - 1).size() < capacityEach) {
            stacks.get(stackIndex - 1).push(item);
        } else {
            stacks.get(stackIndex).push(item);
        }
    }

    public static void main(String[] args) throws Exception {
        SetOfStacks stacks = new SetOfStacks(1);
        stacks.push(1);
        System.out.println(stacks.stacks.toString());
        stacks.push(2);
        System.out.println(stacks.stacks.toString());
        stacks.push(3);
        System.out.println(stacks.stacks.toString());
        stacks.push(4);
        System.out.println(stacks.stacks.toString());
        stacks.push(5);

        System.out.println(stacks.stacks.toString());

        System.out.println(stacks.popAtWithShift(1));
        System.out.println(stacks.stacks.toString());
        System.out.println(stacks.popAtWithShift(1));
        System.out.println(stacks.stacks.toString());
        System.out.println(stacks.popAtWithShift(1));
        System.out.println(stacks.stacks.toString());
        System.out.println(stacks.popAtWithShift(1));
        System.out.println(stacks.stacks.toString());
        System.out.println(stacks.popAtWithShift(1));
        System.out.println(stacks.stacks.toString());


    }
}
