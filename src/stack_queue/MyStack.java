package stack_queue;

import java.util.EmptyStackException;

public class MyStack<T> {

    private StackNode<T> top;

    public void push(T item) {
        StackNode<T> node = new StackNode<>(item);
        node.next = top;
        top = node;
    }

    public T pop() {
        if (top == null) throw new EmptyStackException();
        T item = top.data;
        top = top.next;
        return item;
    }

    public T peek() {
        if (top == null) throw new EmptyStackException();
        return top.data;
    }

    public boolean isEmpty() {
        return top == null;
    }


    static class StackNode<T> {
        T data;
        StackNode<T> next;

        public StackNode(T data) {
            this.data = data;
        }
    }
}

class StackDriver {
    public static void main(String[] args) throws Exception {
        MyStack<Integer> stack = new MyStack<>();
        System.out.println(stack.isEmpty());
        stack.push(5);
        stack.push(6);
        stack.push(7);
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.isEmpty());
        stack.peek();
    }
}
