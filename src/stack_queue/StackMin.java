package stack_queue;


import java.util.Stack;

public class StackMin {
    private StackNode head;
    private int size;

    public void push(int data) {
        if (head == null) {
            head = new StackNode(data,data,null);
        } else {
            int min = Math.min(head.min, data);
            head = new StackNode(min, data, head);
        }
    }

    public int getMin() throws Exception {
        if (head == null) throw new Exception("Stack is empty");
        return head.min;
    }

    public int pop() throws Exception {
        if (head == null) throw new Exception("Stack is empty");
        int item = head.data;
        head = head.next;
        return item;
    }


    static class StackNode {
        int min;
        int data;
        StackNode next;

        public StackNode(int min, int data, StackNode next) {
            this.min = min;
            this.data = data;
            this.next = next;
        }
    }

    public static void main(String[] args) throws Exception {
        StackMin stack = new StackMin();
        stack.push(10);
        System.out.println(stack.getMin());
        stack.push(8);
        System.out.println(stack.getMin());
        stack.push(9);
        System.out.println(stack.getMin());
        stack.push(1);
        System.out.println(stack.getMin());

        stack.pop();
        System.out.println(stack.getMin());
        stack.pop();
        stack.push(-1);
        System.out.println(stack.getMin());

        System.out.println("----------------");

        StackMin1 stack1 = new StackMin1();
        stack1.push(10);
        System.out.println(stack1.getMin());
        stack1.push(8);
        System.out.println(stack1.getMin());
        stack1.push(9);
        System.out.println(stack1.getMin());
        stack1.push(1);
        System.out.println(stack1.getMin());

        stack1.pop();
        System.out.println(stack1.getMin());
        stack1.pop();
        stack1.push(-1);

        System.out.println(stack1.getMin());

    }
}


//from ctci
class StackMin1 {
    Stack<Integer> minStack;
    Stack<Integer> stack;

    public StackMin1() {
        minStack = new Stack<>();
        stack = new Stack<>();
    }

    public void push(int data) {
        if (minStack.isEmpty() || data <= minStack.peek()) {
            minStack.push(data);
        }
        stack.push(data);
    }

    public int pop() {
        int item = stack.pop();
        if (item == minStack.peek())
            minStack.pop();
        return item;
    }

    public int getMin() {
        return minStack.peek();
    }
}