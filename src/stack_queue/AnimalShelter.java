package stack_queue;

import java.util.LinkedList;

public class AnimalShelter {

    private int priority;
    private LinkedList<AnimalNode> dogHead;
    private LinkedList<AnimalNode> catHead;

    public AnimalShelter() {
        this.priority = 1;
        dogHead = new LinkedList<>();
        catHead = new LinkedList<>();
    }

    public void enqueue(char animal) {
        if (animal == 'c') {
            catHead.addLast(new AnimalNode('c', priority++));
        } else {
            dogHead.addLast(new AnimalNode('d', priority++));
        }

    }

    public char dequeueAny() throws Exception {
        if (catHead.isEmpty() && dogHead.isEmpty()) throw new Exception("No animals left");

        AnimalNode animalNode = null;

        if (!dogHead.isEmpty() && !catHead.isEmpty()) {
            //give the animal with highest priority and lowest number
            if (dogHead.peekFirst().priority < catHead.peekFirst().priority) {
                animalNode = dogHead.removeFirst();
            } else {
                animalNode = catHead.removeFirst();
            }
        } else if (dogHead.isEmpty()) {
            animalNode = catHead.removeFirst();
        } else {
            animalNode = dogHead.removeFirst();
        }

        return animalNode.animal;
    }

    public char dequeueCat() throws Exception {
        if (catHead.isEmpty()) throw new Exception("No cats left");

        return catHead.removeFirst().animal;
    }

    public char dequeueDog() throws Exception {
        if (dogHead.isEmpty()) throw new Exception("No dogs left");

        return dogHead.removeFirst().animal;
    }



    static class AnimalNode {
        public char animal;
        public AnimalNode next;
        public int priority;

        public AnimalNode(char animal) {
            this.animal = animal;
            next = null;
        }

        public AnimalNode(char animal, int priority) {
            this.animal = animal;
            next = null;
            this.priority = priority;
        }

        public AnimalNode(char animal, AnimalNode next) {
            this.animal = animal;
            this.next = next;
        }

        public void printNodes() {
            System.out.println();

            AnimalNode temp = this;
            while (temp != null) {
                System.out.print(temp.animal + " -> ");
                temp = temp.next;
            }
        }
    }

    public static void main(String[] args) throws Exception {
        AnimalShelter shelter = new AnimalShelter();

        shelter.enqueue('c');
        shelter.enqueue('c');
        shelter.enqueue('d');
        shelter.enqueue('c');
        shelter.enqueue('d');

        System.out.println(shelter.dequeueDog());
        System.out.println(shelter.dequeueAny());
        System.out.println(shelter.dequeueCat());
        System.out.println(shelter.dequeueDog());
        System.out.println(shelter.dequeueAny());
    }
}
