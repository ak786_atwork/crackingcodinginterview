package stack_queue;

public class MyQueue<T> {

    private QueueNode<T> front;
    private QueueNode<T> rear;

    public void add(T item) {
        QueueNode<T> node = new QueueNode<>(item);
        //add to rear
        if (rear != null) {
            rear.next = node;
            rear = node;
        } else {
            //first item
            rear = node;
            front = rear;
        }
    }

    public T peek() throws Exception {
        if (front == null) throw new Exception("Queue is empty");

        return front.item;
    }

    public T remove() throws Exception {
        if (front == null) throw new Exception("Queue is empty");
        T item = front.item;

        //if only one item reset the pointers
        if (front == rear) {
            front = null;
            rear = null;
        } else {
            front = front.next;
        }
        return item;
    }

    public boolean isEmpty() {
        return front == rear && front == null;
    }


    static class QueueNode<T> {
        T item;
        QueueNode<T> next;

        public QueueNode(T item) {
            this.item = item;
        }
    }
}

class QueueDriver {
    public static void main(String[] args) throws Exception {
        MyQueue<Integer> queue = new MyQueue<>();
        System.out.println(queue.isEmpty());

        queue.add(4);
        queue.add(5);
        queue.add(6);

        System.out.println(queue.remove());
        System.out.println(queue.remove());
        System.out.println(queue.remove());

        System.out.println(queue.isEmpty());

        queue.add(9);
        System.out.println(queue.remove());

        System.out.println(queue.isEmpty());


    }
}