package stack_queue;

import java.util.Stack;

public class QueueUsingStacks {

    public static void main(String[] args) throws Exception {
        MyQueue queue = new MyQueue();

        queue.add(1);
        queue.add(2);
        System.out.println(queue.remove());
        System.out.println(queue.remove());
        queue.add(1);
        System.out.println(queue.remove());
        queue.add(4);
        System.out.println(queue.remove());
        System.out.println(queue.remove());
    }


    static class MyQueue {

        Stack<Integer> stack1;
        Stack<Integer> stack2;
        int size;

        public MyQueue() {
            stack1 = new Stack<>();
            stack2 = new Stack<>();
        }

        public void add(int data){
            stack2.push(data);
            size++;
        }

        public int remove() throws Exception {
            if (size == 0) throw  new Exception("stack is empty" );

            //using s1 to remove
            if (stack1.isEmpty()) {
                while (!stack2.isEmpty()) {
                    stack1.push(stack2.pop());
                }
            }
            size--;
            return stack1.pop();
        }

        public int peek() throws Exception {
            if (size == 0) throw  new Exception("stack is empty" );

            return stack1.peek();
        }


    }
}
