package c13_java;

/**
 *
 * */

public class SuperClass {
    private SuperClass() {

    }

    class B extends SuperClass{

    }

    class A {
        private A() {

        }
    }

    class C extends A {

    }
}

//only inner class can extend the class which has private constructor
/*class  ChildClass extends SuperClass {

}*/
