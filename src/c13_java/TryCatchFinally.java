package c13_java;

/**
 * 1. finally won't execute if vm exits i.e. using System.exit() in try
 * 2. If thread which will execute try catch finally get killed inside try
 * */

public class TryCatchFinally {
    public static void main(String[] args) {
        try {
            System.out.println("inside try"+1/0);
//            System.exit(0);

            return;
        } catch (Exception e) {
            System.out.println("inside catch");
        }
        finally {
            System.out.println("inside finally");
        }

    }
}
