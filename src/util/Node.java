package util;

public class Node {
    public int data;
    public Node next;

    public Node(int d) {
        data = d;
        next = null;
    }

    public Node(int data, Node next) {
        this.data = data;
        this.next = next;
    }

    public void printNodes() {
        System.out.println();

        Node temp = this;
        while (temp != null) {
            System.out.print(temp.data +" -> ");
            temp = temp.next;
        }
    }

}
