package graph_tree;

import java.util.*;

public class BuildOrder {

    public static void main(String[] args) throws Exception {

        int vertices = 2;
        List<Integer>[] verticesList = new List[vertices];

        for (int i = 0; i<vertices;i++) {
            verticesList[i] = new ArrayList<>();
        }

        verticesList[0].add(1);
//        verticesList[1].add(3);
//        verticesList[2].add(1);

//        verticesList[3].add(2);
//        verticesList[5].add(0);
//        verticesList[5].add(1);

        printOrder(getOrder(verticesList, vertices));

    }

    private static void printOrder(Stack<Integer> stack) {
        System.out.println();
        while (!stack.isEmpty()) {
            System.out.print(stack.pop() +" ");
        }
    }


    static boolean isCycle = false;

    public static  Stack<Integer> getOrder(List<Integer>[] verticesList, int vertices) throws Exception {

        boolean[] visited = new boolean[vertices];
        Stack<Integer> resultStack = new Stack<>();
        Set<Integer> vertexSet = new HashSet<>();
        isCycle = false;


        for (int i = 0;i<vertices;i++) {
            if (isCycle) throw new Exception("error : cycle detected");
                vertexSet.clear();
                vertexSet.add(i);
                solve(i, verticesList, visited, resultStack, vertexSet);
        }

        return resultStack;
    }

    private static void solve(int vertex, List<Integer>[] verticesList, boolean[] visited, Stack<Integer> resultStack, Set<Integer> vertexSet) {
        if (visited[vertex] || isCycle)
            return;

        for (int childVertex : verticesList[vertex]) {
            if (vertexSet.contains(childVertex)) {
                isCycle = true;
                return;
            }
            if (!visited[childVertex]) {
                vertexSet.add(childVertex);
                solve(childVertex,verticesList,visited, resultStack, vertexSet);
            }
        }
        resultStack.push(vertex);
        visited[vertex] =true;
    }


}

//not completed laziness
class BuildOrderSolution {



    static class Graph {
        private ArrayList<Project> nodes = new ArrayList<>();
        private HashMap<String, Project> map = new HashMap<>();

        public Project getOrCreateNode(String name) {
            if (!map.containsKey(name)) {
                Project node = new Project(name);
                nodes.add(node);
                map.put(name, node);
            }

            return map.get(name);
        }

        public void addEdge(String startName, String endName) {
            Project start = getOrCreateNode(startName);
            Project end = getOrCreateNode(endName);

        }


    }

    static class Project {
        public enum State {COMPLETE, BLANK, PARTIAL}
        private State state = State.BLANK;
        private String name;

        public Project(String name) {
            this.name = name;
        }

        public void addNeighbour(Project node) {
        }

        public void setState(State state) {
            this.state = state;
        }

        public State getState() {
            return state;
        }
    }
}
