package graph_tree;

import util.Node;

import java.util.LinkedList;
import java.util.Queue;

public class RouteBwTwoNodes {

    public static void main(String[] args) {
        Graph g = new Graph(3);
        g.addEdge(0,2);
        g.addEdge(1,0);
        g.addEdge(1,2);
//        g.addEdge(2,1);

        System.out.println(g.isReachableByDfs(0,1, new boolean[3]));
        System.out.println(g.isReachableByDfs(0,2, new boolean[3]));

        System.out.println(g.isReachableByDfs(1,0, new boolean[3]));
        System.out.println(g.isReachableByDfs(1,2, new boolean[3]));

        System.out.println(g.isReachableByDfs(2,0, new boolean[3]));
        System.out.println(g.isReachableByDfs(2,1, new boolean[3]));

    }


    static class Graph{
        int nodes;
        Node[] childNodes;
        public Graph(int nodes) {
            this.nodes = nodes;
            childNodes = new Node[nodes];
        }

        public void addEdge(int sNode, int dNode) {
//            System.out.println("adding edge source = "+sNode+" destination node = "+dNode);
            if (childNodes[sNode] == null) {
                childNodes[sNode] = new Node(dNode);
            } else {
                Node node = new Node(dNode);
                node.next = childNodes[sNode];
                childNodes[sNode] = node;
            }
        }


        public boolean isReachable(int sNode, int dNode) {
            Queue<Integer> queue = new LinkedList<>();
            queue.add(sNode);
            int node = -1;

            while (!queue.isEmpty()) {
                node = queue.poll();
                if (node == dNode)
                    return true;
                else {
                    //add neighbours
                    Node head = childNodes[node];
                    while (head != null) {
                        queue.add(head.data);
                        head = head.next;
                    }
                }
            }
            return false;
        }

        public boolean isReachableByDfs(int sNode, int dNode, boolean[] visited) {
//            System.out.println("snode = "+sNode+" dnode = "+dNode);

            if (!visited[sNode]) {
                if (sNode == dNode) {
                    return true;
                }
                visited[sNode] = true;

                Node head = childNodes[sNode];
                while (head != null) {
                    if (!visited[head.data]) {
                        if (isReachableByDfs(head.data, dNode,visited))
                            return true;
                    }
                    head = head.next;
                }
            }
            return false;
        }
    }
}
