package graph_tree;

import util.TreeNode;

import java.util.Random;

public class RandomNode {
    int currentSize;
    int maxSize;
    int[] nodes;

    public RandomNode(int size) {
        this.maxSize = size;
        nodes = new int[maxSize];
    }

    public int getRandomNode() {
        return nodes[(new Random()).nextInt(currentSize)];
    }

    public void insert(int data) throws Exception {
        if (currentSize >= maxSize) throw new Exception("tree is out of capacity handle it");
        nodes[currentSize++] = data;
    }

    public void delete(int data) {
        for(int i = 0;i<currentSize;i++) {
            if (nodes[i] == data) {
                nodes[i] = nodes[currentSize-1];
                currentSize--;
                System.out.println("node "+data+" deleted");
                return;
            }
        }
    }

    public boolean find(int data) {
        for(int i = 0;i<currentSize;i++) {
            if (nodes[i] == data)
                return true;
        }
        return false;
    }

}
