package graph_tree;

import util.TreeNode;

//submitted on leetcode
public class BuildBST_FromSortedArray {

    public static void main(String[] args) {
        int[] sortedArray = {1,2,3,4,5,6,7,8};
        TreeNode root = build(0, sortedArray.length-1,sortedArray);

        printInorder(root);
    }

    private static void printInorder(TreeNode root) {
        if (root != null) {
            printInorder(root.left);
            System.out.print(root.data+" ");
            printInorder(root.right);
        }
    }

    private static TreeNode build(int l, int r, int[] array) {
        if (l>r)
            return null;

        int mid = (l+r)/2;
        TreeNode node = new TreeNode(array[mid]);
        node.left = build(l,mid-1,array);
        node.right = build(mid+1,r,array);

        return node;
    }
}
