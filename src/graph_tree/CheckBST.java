package graph_tree;

import util.TreeNode;

public class CheckBST {

    static int max = Integer.MIN_VALUE;
    static boolean notBalanced = false;
    static boolean firstNumber = true;

    public static void main(String[] args) {

        //[-2147483648,null,2147483647]

        //2147483647
        //-2147483648

        TreeNode root = new TreeNode(Integer.MIN_VALUE);
//        root.left = new TreeNode(0);
//        root.right = new TreeNode(Integer.MAX_VALUE);

        System.out.println(isBST(root));

    }

    public static boolean isBST(TreeNode root) {
        max = Integer.MIN_VALUE;
        notBalanced = false;
        firstNumber = true;

        check(root);

        return !notBalanced;
    }

    public static void check(TreeNode root) {
        if (root == null || notBalanced) return;

        check(root.left);

        if (firstNumber) {
            max = root.data;
            firstNumber = false;
        } else {
            if (root.data > max)
                max = root.data;
            else {
                notBalanced = true;
                return;
            }
        }

        check(root.right);
    }

    //from ctci
    static class Solution {

        public static boolean isValidBST(TreeNode root) {
            return check(root, null, null);
        }

        private static boolean check(TreeNode root, Integer min, Integer max) {
            if (root == null)
                return true;

            if (((min != null) && min < root.data) || ((max != null) && root.data > max))
                return false;

            if (!check(root.left, min, root.data) || !check(root.right, root.data, max))
                return false;

            return true;
        }
    }
}
