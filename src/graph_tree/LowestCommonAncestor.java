package graph_tree;

import util.TreeNode;

//submitted on leetcode
public class LowestCommonAncestor {

    public static void main(String[] args) {
        TreeNode treeNode = new TreeNode(4);
        treeNode.left = new TreeNode(9);
        treeNode.right = new TreeNode(19);
        treeNode.left.right = new TreeNode(1);
        treeNode.left.left = new TreeNode(12);
        treeNode.left.right.left = new TreeNode(5);

        System.out.println(lowestCommonAncestor(treeNode, treeNode.left.right.left, treeNode.left.left).data);
    }



    static TreeNode commonNode;

    public static TreeNode lowestCommonAncestor(TreeNode root, TreeNode node1, TreeNode node2) {
        if (root == null || node1 == root || node2 == root) return root;

        commonNode = null;
        checkNode(root, node1, node2);
        return commonNode;
    }

    public static boolean checkNode(TreeNode node, TreeNode node1, TreeNode node2) {
        if (node == null || commonNode != null) return false;

        boolean nodeFound = false;
        if (node == node1 || node == node2)
            nodeFound = true;

        boolean isNodeExistOnLeft = checkNode(node.left,node1,node2);
        boolean isNodeExistOnRight = checkNode(node.right,node1,node2);

        if ((isNodeExistOnLeft && isNodeExistOnRight) || ((isNodeExistOnLeft && nodeFound) || (isNodeExistOnRight && nodeFound))) {
            commonNode = node;
        }

        return isNodeExistOnLeft || isNodeExistOnRight || nodeFound;
    }

    //from leetcode
    public TreeNode lowestCommonAncestor1(TreeNode root, TreeNode p, TreeNode q) {
        if(root == null || root == p || root == q) {
            return root;
        }

        TreeNode left = lowestCommonAncestor(root.left, p, q);
        TreeNode right = lowestCommonAncestor(root.right, p, q);

        if(left != null && right != null) {
            return root;
        }

        return left == null ? right : left;
    }
}
