package graph_tree;

import sun.awt.image.ImageWatched;
import util.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class BSTSequences {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(5);
        root.left = new TreeNode(3);
        root.left.left = new TreeNode(2);
        root.left.right = new TreeNode(4);
        root.right = new TreeNode(10);
        root.right.left = new TreeNode(6);

//        System.out.println(find(root));
        System.out.println(Solution.allSequences(root));
    }


    //this is wrong as i interpreted the problem in wrong way
    private static List<List<Integer>> find(TreeNode node) {
        if (node == null) return null;

        List<List<Integer>> leftList = find(node.left);
        List<List<Integer>> rightList = find(node.right);

        List<List<Integer>> resultList = mergeLists(node, leftList, rightList);
//        System.out.println(resultList.toString());
        return resultList;
    }

    private static List<List<Integer>> mergeLists(TreeNode node, List<List<Integer>> leftList, List<List<Integer>> rightList) {
        List<List<Integer>> resultList = new LinkedList<>();

        if (leftList == null && rightList == null) {
            List<Integer> newList = new LinkedList<>();
            newList.add(node.data);
            resultList.add(newList);
            return resultList;
        } else if (leftList == null || rightList == null) {
            leftList = leftList == null ? rightList : leftList;

            for (List<Integer> list : leftList){
                list.add(0,node.data);
                resultList.add(list);
            }
            return resultList;
        } else {
            for (List<Integer> l1 : leftList) {
                for (List<Integer> l2 : rightList) {
                    List<Integer> newList = new LinkedList<>();
                    newList.add(0,node.data);
                    newList.addAll(l1);
                    newList.addAll(l2);

                    List<Integer> newList1 = new LinkedList<>();
                    newList1.add(0,node.data);
                    newList1.addAll(l2);
                    newList1.addAll(l1);

                    resultList.add(newList);
                    resultList.add(newList1);

                }
            }
            return resultList;
        }

    }

    //this is correct and from ctci
    static class Solution {
        public static ArrayList<LinkedList<Integer>> allSequences(TreeNode root) {
            ArrayList<LinkedList<Integer>> resultList = new ArrayList<>();

            if (root == null) {
                resultList.add(new LinkedList<>());
                return resultList;
            }

            LinkedList<Integer> prefix = new LinkedList<>();
            prefix.addLast(root.data);

            ArrayList<LinkedList<Integer>> leftSeq = allSequences(root.left);
            ArrayList<LinkedList<Integer>> rightSeq = allSequences(root.right);

            for (LinkedList<Integer> left : leftSeq) {
                for (LinkedList<Integer> right : rightSeq) {
                    ArrayList<LinkedList<Integer>> weaved = new ArrayList<>();
                    weaveLists(left,right,weaved,prefix);
                    resultList.addAll(weaved);
                }
            }

            System.out.println("left list = "+leftSeq.toString()+" right = "+rightSeq.toString());
            System.out.println("merged list = "+resultList.toString());
            return resultList;
        }

        private static void weaveLists(LinkedList<Integer> first, LinkedList<Integer> second, ArrayList<LinkedList<Integer>> results, LinkedList<Integer> prefix) {
            if (first.size() == 0 || second.size() == 0) {
                LinkedList<Integer> result = (LinkedList<Integer>) prefix.clone();
                result.addAll(first);
                result.addAll(second);
                results.add(result);
                return;
            }

            int headFirst = first.removeFirst();
            prefix.addLast(headFirst);
            weaveLists(first, second,results,prefix);
            prefix.removeLast();
            first.addFirst(headFirst);

            int headSecond = second.removeFirst();
            prefix.addLast(headSecond);
            weaveLists(first,second,results,prefix);
            prefix.removeLast();
            second.addFirst(headSecond);

        }
    }
}
