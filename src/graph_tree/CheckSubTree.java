package graph_tree;

import util.TreeNode;

public class CheckSubTree {



    //submitted to leetcode from ctci
    static class Solution {
        public static boolean isSubtree(TreeNode s, TreeNode t) {
            if (t == null) return true;     //empty tree is always a subtree

            StringBuilder s1 = new StringBuilder();
            StringBuilder t1 = new StringBuilder();

            getPreOrderTraversal(s, s1);
            getPreOrderTraversal(t, t1);
            System.out.println(s1.toString());
            System.out.println(t1.toString());

            return s1.indexOf(t1.toString()) != -1;
        }

        private static void getPreOrderTraversal(TreeNode node, StringBuilder stringBuilder) {
            if (node == null) {
                stringBuilder.append("n");
                return;
            }

            stringBuilder.append(" " + node.data);
            getPreOrderTraversal(node.left, stringBuilder);
            getPreOrderTraversal(node.right, stringBuilder);
        }
    }

    //submitted to leetcode from ctci
    static class Solution1 {
        public static boolean isSubtree(TreeNode s, TreeNode t) {
            if (t == null) return true;

            return subTreeHelper(s, t);
        }

        private static boolean subTreeHelper(TreeNode node1, TreeNode node2) {
            if (node1 == null)
                return false;
            else if (node1.data == node2.data && matchTree(node1, node2))
                return true;
            else
                return subTreeHelper(node1.left, node2) || subTreeHelper(node1.right, node2);
        }

        private static boolean matchTree(TreeNode node1, TreeNode node2) {
            if (node1 == null && node2 == null)
                return true;
            else if (node1 == null || node2 == null)
                return false;  //no exact match
            else if (node1.data != node2.data)
                return false;
            else
                return matchTree(node1.left, node2.left) && matchTree(node1.right, node2.right);
        }
    }

    //from leetcode
    static class BestSolution {
        public boolean isSubtree(TreeNode s, TreeNode t) {

            if (s == null && t == null)
                return true;
            if (s == null || t == null)
                return false;
            if (s.data == t.data) {
                //   System.out.println(s.val);
                if (!(s.left != null && t.left != null && s.left.data != t.left.data) &&
                        !(s.right != null && t.right != null && s.right.data != t.right.data) &&
                        isSubtree(s.left, t.left) && isSubtree(s.right, t.right))
                    return true;
            }
            if (isSubtree(s.left, t) || isSubtree(s.right, t))
                return true;

            return false;
        }
    }
}
