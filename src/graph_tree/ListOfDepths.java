package graph_tree;

import util.Node;
import util.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ListOfDepths {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.left.left = new TreeNode(8);

        root.right = new TreeNode(3);
        root.right.left = new TreeNode(7);

        List<Node> nodeList = getListByDepth(root);
        for (Node head : nodeList)
            head.printNodes();

        List<LinkedList<Integer>> linkedLists = getListUsingDFS(root);
        for (LinkedList<Integer> linkedList : linkedLists) {
            System.out.println(linkedList.toString());
        }
    }

    public static List<Node> getListByDepth(TreeNode root) {
        if (root == null)
            return null;

        List<Node> nodeList = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        int levelSize = 0;
        TreeNode currentTreeNode = null;
        while (!queue.isEmpty()) {
            levelSize = queue.size();
            Node head = null;
            for (int i =0;i<levelSize;i++) {
                currentTreeNode = queue.remove();
                //adding childs
                if (currentTreeNode.left != null)
                    queue.add(currentTreeNode.left);
                if (currentTreeNode.right != null)
                    queue.add(currentTreeNode.right);

                //level
                if (head == null) {
                    head = new Node(currentTreeNode.data);
                    //add to list
                    nodeList.add(head);
                } else {
                    Node node = new Node(currentTreeNode.data);
                    head.next = node;
                    head = node;
                }
            }
        }
        return nodeList;
    }

    public static List<LinkedList<Integer>> getListUsingDFS(TreeNode root) {
        //use modified preorder with dfs
        List<LinkedList<Integer>> linkedLists = new ArrayList<>();
        addLevels(linkedLists, root, 0);

        return linkedLists;
    }

    private static void addLevels(List<LinkedList<Integer>> lists, TreeNode parentNode, int level) {
        if (parentNode == null)
            return;

        LinkedList<Integer> linkedList = null;
        if (level == lists.size()) {
            //add new linked list
            linkedList = new LinkedList<>();
            lists.add(linkedList);
        } else {
            linkedList = lists.get(level);
        }
        //preorder
        linkedList.addLast(parentNode.data);
        addLevels(lists,parentNode.left,level+1);
        addLevels(lists,parentNode.right,level+1);
    }
}
