package graph_tree;

import util.TreeNode;

import java.util.HashMap;

public class PathSum {

    public static void main(String[] args) {
    }


    public int pathSum(TreeNode root, int sum) {
        if (root == null) return 0;

        return count(root,0,sum);
    }

    private int count(TreeNode root, int currentSum, int sum) {
        if (root == null) return 0;
        int pathCount = 0;
        if (currentSum + root.data == sum) {
            pathCount++;
        }

        return pathCount + count(root,0,sum) + count(root.left,currentSum+root.data,sum) + count(root.right, currentSum + root.data, sum);
    }

    private int count(TreeNode node, int sum) {
        if (node == null) return 0;
        int paths = 0;
        if (sum-node.data==0)
            paths++;

        if (sum == 0) {
            return paths + count(node.left, node.data) + count(node.right,node.data);
        } else {
            return paths + count(node.left, node.data) + count(node.right,node.data)+
                    count(node.left,sum-node.data) + count(node.right, sum-node.data);

        }
    }

    //submitted on leetcode
    static class Solution{
        private static int count = 0;

        public static int pathSum(TreeNode root, int sum) {
            count = 0;
            pathSumHelper(root, sum);
            return count;
        }

        public static int pathSumHelper(TreeNode root, int sum) {
            if (root == null) return 0;

            traverseAllNode(root,0,sum);
            pathSumHelper(root.left,sum);
            pathSumHelper(root.right,sum);

            return count;
        }


        private static void traverseAllNode(TreeNode root, int currentSum, int sum) {
            if (root == null)
                return;

            currentSum += root.data;
            if (currentSum == sum)
                count++;

            traverseAllNode(root.left,currentSum,sum);
            traverseAllNode(root.right,currentSum,sum);
        }
    }

    //from ctci
    static class Solution1 {
        public static int pathSum(TreeNode root, int sum) {
            return pathSum(root,0,sum,new HashMap<Integer, Integer>());
        }

        private static int pathSum(TreeNode node, int runningSum, int targetSum, HashMap<Integer, Integer> pathCountMap) {
            if (node == null) return 0;

            runningSum += node.data;
            int totalPaths = 0;
            if (runningSum == targetSum) {
                //one path found
                totalPaths++;
            }

            totalPaths += pathCountMap.getOrDefault(runningSum-targetSum,0);
            incrementHashMap(pathCountMap, runningSum, 1);      // increment the count
            totalPaths += pathSum(node.left,runningSum,targetSum,pathCountMap);
            totalPaths += pathSum(node.right,runningSum,targetSum,pathCountMap);
            incrementHashMap(pathCountMap, runningSum, -1); //decrement the count, so that it can be used by other nodes

            return totalPaths;
        }

        private static void incrementHashMap(HashMap<Integer, Integer> pathCountMap, int key, int data) {
            int count = pathCountMap.getOrDefault(key, 0) + data;
            if (count == 0) {
                //remove key to reduce space
                pathCountMap.remove(key);
            } else {
                pathCountMap.put(key,count);
            }
        }
    }
}
