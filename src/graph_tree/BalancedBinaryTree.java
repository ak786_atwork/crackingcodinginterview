package graph_tree;

import util.TreeNode;

//submitted on leetcode
public class BalancedBinaryTree {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.left.left = new TreeNode(3);
        root.left.right = new TreeNode(4);
//        root.right = new TreeNode(5);
//        root.right.left = new TreeNode(6);

        System.out.println(isBalanced(root));
        System.out.println(Solution.isBalanced(root));
    }

    static boolean isBalanced = true;
    public static boolean isBalanced(TreeNode root) {
        isBalanced = true;
        count(root);
        return  isBalanced;
    }

    private static int count(TreeNode root) {
        if (root == null || !isBalanced) {
            return 0;
        }
        int h1 = count(root.left);
        int h2 = count(root.right);

        if (Math.abs(h1-h2) > 1)
            isBalanced = false;
        return 1 + Math.max(h1,h2);
    }

    //from ctci
    static class Solution {
        public static boolean isBalanced(TreeNode root) {
            return checkHeight(root) != Integer.MIN_VALUE;
        }

        private static int checkHeight(TreeNode root) {
            if (root == null) {
                return -1;
            }
            int h1 = checkHeight(root.left);
            int h2 = checkHeight(root.right);

            if (h1 == Integer.MIN_VALUE || h2 == Integer.MIN_VALUE || Math.abs(h1-h2) > 1)
                return Integer.MIN_VALUE;
            return 1 + Math.max(h1,h2);
        }
    }

}
