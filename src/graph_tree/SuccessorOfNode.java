package graph_tree;


import util.Node;
import util.TreeNode;

import java.util.Stack;

public class SuccessorOfNode {


    public static TreeNode getSuccessor(TreeNode node) {
        if (node == null)
            return null;

        TreeNode successorNode = node;

        if (node.right == null) {
            //go above and get the successor
            TreeNode parent = node.parent;
            while (parent != null) {
                if (node.data < parent.data)
                    return parent;
                parent = parent.parent;
            }
        } else {
            // go down
            node = node.right;
            successorNode = node;
            while (node != null) {
                successorNode = node;
                node = node.left;
            }
        }


        return successorNode;
    }


    private static class TreeNode {
        public int data;
        public TreeNode left;
        public TreeNode right;
        public TreeNode parent;

        public TreeNode(int data) {
            this.data = data;
        }

        public TreeNode(int data, TreeNode left, TreeNode right) {
            this.data = data;
            this.left = left;
            this.right = right;
        }
    }

    //submitted on geeksforgeeks
    private static class GeeksSolution {
        public util.TreeNode inorderSuccessor(util.TreeNode root, util.TreeNode k) {
            if (root == null)
                return null;

            Stack<util.TreeNode> parentStack = new Stack<>();
            util.TreeNode temp = root;

            while (temp != null && temp != k) {
                parentStack.push(temp);
                if (k.data < temp.data)
                    temp = temp.left;
                else
                    temp = temp.right;
            }

            if (temp == k) {
                if (temp.right != null)
                    return leftMostChild(temp.right);
                else {
                    //use stack
                    while (!parentStack.empty()) {
                        temp = parentStack.pop();
                        if (k.data < temp.data)
                            return temp;
                    }
                }

            }

            return null;
        }

        private util.TreeNode leftMostChild(util.TreeNode node) {
            util.TreeNode successorNode = node;
            while (node != null) {
                successorNode = node;
                node = node.left;
            }
            return successorNode;
        }
    }

}
