package bits;

public class FlipBitToWin {

    public static void main(String[] args) {
        int x = Integer.MAX_VALUE;
        StringBuilder bits = new StringBuilder("0");
        bits.append(Integer.toBinaryString(x));

        System.out.println(bits+" = "+ count(false, 0, 0, bits.toString()));
        System.out.println(bits+" = "+ count(bits.toString()));
        System.out.println(bits+" = "+ count(x));
    }

    //check for 15 it gives 5
    public static int count(boolean replaced, int count, int index, String bits) {
        if (index >= bits.length())
            return count;

        if (bits.charAt(index) == '1') {
            return count(replaced, count+1, index+1, bits);
        } else {
            if (replaced) {
                //bit has already been switched no choice left
                return Math.max(
                        count,
                        count(false, 0, index+1, bits)
                );
            } else {
                //we can flip on bit
                return Math.max(
                        count(replaced, 0, index+1, bits),
                        count(true, count+1, index+1, bits)
                );
            }
        }

    }

    public static int count(String bits) {
        int max = 1;  //always 1 we can get
        int cmax = 0;
        int pmax = 0;

        for (int i=0;i<bits.length();i++) {
            if (bits.charAt(i) == '1') {
                cmax++;
            } else {
                //merge adjacent sequence of 1 and check
                max = Math.max(cmax+pmax+1, max);
                pmax = cmax;
                cmax = 0;
            }
        }

        return Math.max(cmax+pmax+1, max);
    }

    //from ctci
    public static int count(int n) {
        //if all ones
        if (~n == 0)
            return Integer.BYTES * 8;

        int currentLength = 0;
        int maxLength = 1;      //since we can flip 1 0bit so we get 1
        int previousLength = 0;

        while (n != 0) {
            if ((n & 1) == 1) {
                currentLength++;
            } else {
                //check whether next bit is 0 or not if 0 then we have 2 subsequent 0s, we have to ignore adjacent subsequence of 1s
                previousLength = (n & 2) == 0 ? 0:currentLength;
                currentLength = 0;
            }
            maxLength = Math.max(currentLength+previousLength+1, maxLength);
            n = n>>>1;
        }

        return maxLength;
    }

}
