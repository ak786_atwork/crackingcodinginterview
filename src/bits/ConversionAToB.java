package bits;

//submitted on geeksforgeeks
public class ConversionAToB {

    public static void main(String[] args) {
        int a = 94;
        int b = 7;

        System.out.println(a +" "+Integer.toBinaryString(a));
        System.out.println(b +" "+Integer.toBinaryString(b));

        System.out.println("bit flips = "+getBitsToBeFlipped(a,b));
        System.out.println("bit flips = "+getBitsToBeFlipped1(a,b));
        System.out.println("bit flips = "+getBitsToBeFlipped2(a,b));
    }

    public static int getBitsToBeFlipped(int a, int b) {
        int bitFlips = 0;
        while (a != 0 || b != 0) {

            //check if bits are same or not
            if (((a & 1)==1 && (b & 1) == 0) || ((a & 1)==0 && (b & 1) == 1)) {
                bitFlips++;
            }
            a >>>= 1;
            b >>>= 1;
        }
        return bitFlips;
    }

    //from ctci
    public static int getBitsToBeFlipped1(int a, int b) {
        int bitFlips = 0;
        for (int i = a ^ b; i != 0; i >>= 1)
            bitFlips += i & 1;

        return bitFlips;
    }

    //from ctci
    public static int getBitsToBeFlipped2(int a, int b) {
        int bitFlips = 0;
        for (int i = a^b;i != 0; i = i & (i-1))
            bitFlips++;

        return bitFlips;
    }
}
