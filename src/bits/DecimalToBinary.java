package bits;

public class DecimalToBinary {

    public static void main(String[] args) throws Exception {
        double number = Double.MAX_VALUE;
        System.out.println(convert(number));
    }

    //if it exceeds 32 chars it should throw exception
    public static String convert(double number) throws Exception {
        int n = (int) number;
        double fraction = number - n;

        StringBuilder sb = new StringBuilder();
        //convert integer part to binary
        while (n != 0) {
            sb.append(n % 2);
            n /= 2;
        }
        sb.reverse();
        if (fraction != 0)
            sb.append(".");
        //convert fraction part to binary
        int remainingChars = 32 - sb.length();
        if (remainingChars <= 0) throw new Exception("characters exceeded");

        int temp = 0;
        while (remainingChars > 0 && fraction != 0) {
            fraction *= 2;
            temp = (int) fraction;
            sb.append(temp);

            fraction = fraction - temp;
            remainingChars--;
        }

        // can't be accom
        if (fraction != 0) throw new Exception("characters exceeded");

        return sb.toString();
    }
}
