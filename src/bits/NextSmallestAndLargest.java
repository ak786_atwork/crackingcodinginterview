package bits;

//for positive integer
public class NextSmallestAndLargest {

    public static void main(String[] args) {
        int n = Integer.MAX_VALUE;
        System.out.println("next smallest of " + n + " = " + nextSmallest(n));

        int nextLargest = nextLargest(n);
        System.out.println("next Largest of " + n + " = " + nextLargest);

        System.out.println("smallest == " + getPrev(n));
        System.out.println("largest == " + getNext(n));

        System.out.println("smallest == " + ArithmeticSolution.getPrev(n));
        System.out.println("largest == " + ArithmeticSolution.getNext(n));

        generateBits(nextLargest);
    }


    //my approach
    public static int nextSmallest(int number) {
        //find first sequence of 10 from right to left and swap with 01 them
        int mask = 0;
        int n = number;
        int bitPosition = 0;
        boolean nextSmallestFound = false;

        while (n > 0) {
            if (((n & 1) == 0) && (n & (1 << 1)) >= 1) {
                //swap 1. clear bitposition+2
                mask = ~(1 << (bitPosition + 1));

                //clearing
                number = number & mask;
                //setting bitposition 1
                mask = 1 << bitPosition;
                number = number | mask;
                nextSmallestFound = true;
                break;
            }
            n >>>= 1;
            bitPosition++;
        }
        if (!nextSmallestFound)
            return -1;
        return number;
    }

    //my approach
    public static int nextLargest(int number) {
        //find first sequence of 01 and swap to 10 if it exists otherwise solution will be 1000...1...
        int bitPosition = 0;
        int n = number;
        int mask = 0;
        boolean nextFound = false;
        int ones = 0;

        while (n > 0) {
            if ((n & 1) == 1)
                ones++;
            if (n != 1 && ((n & 1) == 1) && ((n & (1 << 1)) == 0)) {
                //sequence found
                mask = ~(1 << bitPosition);
                //clearing bit
                number &= mask;
                //setting bit at bitposition + 2
                mask = 1 << (bitPosition + 1);
                number |= mask;

                nextFound = true;
                break;
            }
            n >>>= 1;
            bitPosition++;
        }

        if (!nextFound) {
            //if number is like 11...1100...0 or 10..0 and total bits = 31 excluding sign bit, then we can't generate next largest with same number of 1
            //if number is 0 then also no bigger number exists
            if (bitPosition == 31 || number == 0)
                return -1;

            //maximum possible number given with given set bits eg 15 = 1111 or 1110
            number = (1 << bitPosition) | (-1 + (1 << (ones - 1)));
        }

        return number;
    }

    public static void generateBits(int number) {
        int i = 0;
        while (i <= number) {
            System.out.println(i + " = " + Integer.toBinaryString(i));
            i++;
        }
    }

    //from ctci
    public static int getNext(int n) {
        int c = n;
        int c0 = 0;
        int c1 = 0;

        while (((c & 1) == 0) && (c != 0)) {
            c0++;
            c >>= 1;
        }

        while ((c & 1) == 1) {
            c1++;
            c >>= 1;
        }

        if (c0 + c1 == 31 || c0 + c1 == 0)
            return -1;

        int p = c0 + c1;

        n |= (1 << p);
        n &= ~((1 << p) - 1);
        n |= (1 << (c1 - 1)) - 1;

        return n;
    }

    //from ctci
    public static int getPrev(int n) {
        int temp = n;
        int c0 = 0;
        int c1 = 0;

        while ((temp & 1) == 1) {
            c1++;
            temp >>= 1;
        }

        if (temp == 0)
            return -1;

        while (((temp & 1) == 0) && (temp != 0)) {
            c0++;
            temp >>= 1;
        }

        int p = c0 + c1;

        n &= ((~0) << (p + 1));

        int mask = (1 << (c1 + 1)) - 1;
        n |= mask << (c0 - 1);

        return n;
    }

    static class ArithmeticSolution {
        // Main Function to find next
        // Bigger number Smaller than n
        static int getPrev(int n) {
            /* Compute c0 and
                c1 and store N*/
            int temp = n;
            int c0 = 0;
            int c1 = 0;

            while ((temp & 1) == 1) {
                c1++;
                temp = temp >> 1;
            }

            if (temp == 0)
                return -1;

            while (((temp & 1) == 0) &&
                    (temp != 0)) {
                c0++;
                temp = temp >> 1;
            }

            return n - (1 << c1) -
                    (1 << (c0 - 1)) + 1;
        }

        // Function to find next smallest
        // number bigger than n
        static int getNext(int n)
        {
            // Compute c0  and c1
            int c = n;
            int c0 = 0;
            int c1 = 0;

            while (((c & 1) == 0) && (c != 0))
            {
                c0 ++;
                c >>= 1;
            }
            while ((c & 1) == 1)
            {
                c1++;
                c >>= 1;
            }

            // If there is no bigger number
            // with the same no. of 1's
            if (c0 + c1 == 31 || c0 + c1 == 0)
                return -1;

            return n + (1 << c0) +
                    (1 << (c1 - 1)) - 1;
        }
    }
}
