package bits;

public class InsertM_IntoN {

    public static void main(String[] args) {
        System.out.println(insert(8, 2,2,1));
        System.out.println(insert1(8, 2,2,1));
    }

    /**
     * m,n 32 bit numbers
     * n - number to which m is inserted from position j to i
     *
     * steps:-
     * 1. clear the bits from j to i in n
     * 2. shift m to left by i places
     * 3. merge cleared_n to shifted_m
     * */
    public static int insert(int n, int m, int j, int i) {
        //to clear bits from j to i, we have to prepare mask like 1110001111
        int mask = ~((-1 << i) + (1 << j+1));

        //clear bits from j to i in n
        int cleared_n = n & mask;

        //left shift m by i
        int shifted_m = m << i;

        //merge cleared_n and shifted_m
        return cleared_n | shifted_m;
    }


    public static int insert1(int n, int m, int j, int i) {
        int allOnes = ~0;
        int left = allOnes << (j+1);
        int right = ((1<<i) - 1);
        int mask = left | right;
        int n_cleared = n & mask;
        int m_shifted = m << i;

        return n_cleared | m_shifted;
    }
}
