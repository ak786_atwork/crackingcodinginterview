package bits;

public class PairwiseSwap {

    public static void main(String[] args) {
        int n = 5;
        int res  = swapBits(n);
        System.out.println(n +" = "+Integer.toBinaryString(n) + " res = "+res + " = "+Integer.toBinaryString(res));
    }


    /**
     * we have to swap bits in pairs eg 0 and 1, 2 and 3 and so on
     *
     *
     * */
    public static int swapBits(int n) {
        //nothing will change after swapping
        return ((n & 0xaaaaaaaa) >>> 1) | ((n & 0x55555555) << 1);
    }
}
