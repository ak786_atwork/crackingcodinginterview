package bits;

public class DrawLineOnScreen {


    public static void main(String[] args) {
        byte[] screen = new byte[30];
        int width = 24;
        int y = 1;
        int x1 = 1;
        int x2 = 17;

        printPixels(screen, width);
        drawLine(screen,width,x1,x2,y);
        printPixels(screen,width);

    }

    private static void printPixels(byte[] screen, int width) {
        System.out.println();

        for (int i = 0; i < screen.length / (width/8); i++) {
            for (int j = 0; j < width / 8; j++) {
                System.out.print(getBinary(screen[i * (width/8) + j]) + " ");
            }
            System.out.println();
        }
    }

    private static String getBinary(byte b) {
        String s1 = String.format("%8s", Integer.toBinaryString(b & 0xFF)).replace(' ', '0');
        return s1;
    }


    /**
     *
     */
    public static void drawLine(byte[] screen, int width, int x1, int x2, int y) {

        int bitsNeededToCompleteByte = 0;
        int screenIndex;
        byte mask;

        while (x1 <= x2) {
            bitsNeededToCompleteByte = 8 - x1 % 8;
            screenIndex = (width/8) * y + x1 / 8;

            if (x1 + bitsNeededToCompleteByte <= x2) {
                if (bitsNeededToCompleteByte == 8) {
                    //entire byte
                    screen[screenIndex] = -1;
                    x1 += 8;
                } else {
                    //shift and set part
                    mask = (byte) (-1 + (1 << bitsNeededToCompleteByte));
                    screen[screenIndex] = mask;
                    x1 += bitsNeededToCompleteByte;
                }
            } else {
                //single byte
//                mask = (byte) ((-1 << (8 - x2 % 8)) + (1 << (8 - x1 % 8)));
                mask = (byte) ((-1 << (7 - x2 % 8)) >> (x1 % 8));
                screen[screenIndex] = mask;
                break;
            }
        }
    }
}
