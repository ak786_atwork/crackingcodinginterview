package iq;

import java.util.Stack;

public class ShipProblem {
    static int noDestroyDay(int[] ships) {
        if (ships.length > 1) {
            Stack<Integer> lootStack = new Stack<>();
            int maxDays = 0;
            int length = ships.length;
            lootStack.push(ships[length - 1]);
            int tempMaxDays = 0;

            for (int i = length - 2; i >= 0; i--) {
                tempMaxDays = 0;
                if (ships[i] < lootStack.peek()) {
                    while (!lootStack.empty() && ships[i] < lootStack.peek()) {
                        ++tempMaxDays;
                        lootStack.pop();
                    }
                }

                lootStack.push(ships[i]);

                if (tempMaxDays > maxDays) {
                    maxDays = tempMaxDays;
                }
            }
            return maxDays;
        }
        return 0;
    }
}
