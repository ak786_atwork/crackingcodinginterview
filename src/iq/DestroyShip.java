package iq;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class DestroyShip {

    public static void main(String[] args) {
//        Integer[] lootLevel = {1, 2, 3, 4, 5};
        int[] lootLevel = {3};
//        int[] lootLevel = {3, 6,2,7,5};
//        int[] lootLevel = {3, 5, 4, 4, 7, 5};
//        int[] lootLevel = {5,4,3,2,1};
//        int[] lootLevel = {1,2,3,1,1};
//        int[] lootLevel = {4,2,3,8,10,6,2};
//        int[] lootLevel = {7,6,2,1,4,1};
//        Integer[] lootLevel = {1,4,3,2,6,7,3,2};
//        int[] lootLevel = {1,1,1,1,0,0,0};
//        int[] lootLevel = {0,0,2,4,3,8,1};
//        Integer[] lootLevel = {0,-1};
//        Integer[] lootLevel = {1,2,3,4,5,5,5,9};
//        int[] lootLevel = {1, 8, 3, 7, 9, 3, 7, 9, 3};
//        System.out.println(getDays(lootLevel));
//        System.out.println(getDays1(lootLevel));

//        System.out.println(PirateShipProblem1.noDestroyDay(lootLevel));
    }


    public static int getDays(int[] lootLevel) {
        if (lootLevel.length < 2) return 0;

        int days = 0;
        Stack<Integer> stack = new Stack<>();
        stack.add(lootLevel[0]);

        boolean isShipDestroyed = false;

        for (int i = 1; i < lootLevel.length; i++) {
            while (i < lootLevel.length && stack.peek() < lootLevel[i]) {
                i++;
                isShipDestroyed = true;
            }
            if (isShipDestroyed) {
                days++;
                isShipDestroyed = false;
            }
        }

        return days;
    }


    public static int getDays1(int[] lootLevel) {
        if (lootLevel.length < 2) return 0;

        int days = 0;
        int leftLootLevel = lootLevel[0];
        boolean isShipDestroyed = false;

        for (int i = 1; i < lootLevel.length; i++) {
            while (i < lootLevel.length && leftLootLevel < lootLevel[i]) {
                i++;
                isShipDestroyed = true;
            }

            if (i < lootLevel.length)
                leftLootLevel = lootLevel[i];

            if (isShipDestroyed) {
                days++;
                isShipDestroyed = false;
            }
        }

        return days;
    }
}

class DestroyPirateShips {
    public static int getDays(int[] lootLevel) {
        if (lootLevel.length < 2) return 0;

        int days = 0;
        int leftLootLevel = lootLevel[0];
        boolean isShipDestroyed = false;

        for (int i = 1; i < lootLevel.length; i++) {
            //checking if ships can be destroyed continuously
            while (i < lootLevel.length && leftLootLevel < lootLevel[i]) {
                i++;
                isShipDestroyed = true;
            }

            // updating the left ship loot level
            if (i < lootLevel.length)
                leftLootLevel = lootLevel[i];

            //updating days
            if (isShipDestroyed) {
                days++;
                isShipDestroyed = false;
            }
        }

        return days;
    }
}



class PirateShipProblem {
    List<Integer> mShips;

    public PirateShipProblem(List<Integer> ships) {
        mShips = ships;
    }

    int noDestroyDay() {
        if (mShips != null && mShips.size() > 1) {
            Stack<Integer> lootStack = new Stack<Integer>();
            Integer maxDays = 0;
            Integer length = mShips.size();
            lootStack.push(mShips.get(length - 1));
            for (int i = length - 2; i >= 0; i--) {
                int tempMaxDays = 0;
                if (mShips.get(i) < lootStack.peek()) {
                    while (!lootStack.empty() && mShips.get(i) < lootStack.peek()) {
                        ++tempMaxDays;
                        lootStack.pop();
                    }
                }
                lootStack.push(mShips.get(i));
                if (tempMaxDays > maxDays) {
                    maxDays = tempMaxDays;
                }
            }
            return maxDays;
        } else {
            return 0;
        }
    }
  /*public static PirateShipProblem getPirateShipProblemObject(List array) {
            return new PirateShipProblem((ArrayList)array);
        } */
}
