package season2.sorting_searching;


//submitted to leetcode
public class SearchInRotatedArray {

    public static void main(String[] args) {
//        int[] a = {4,5,6,7,8,1,2,3};
        int[] a = {5, 1};
        System.out.println(search(a, 5));
    }


    public static int search(int[] a, int number) {
        return searchHelper(a, number, 0, a.length - 1);
    }

    private static int searchHelper(int[] a, int number, int beg, int end) {
        if (beg > end) return -1;

        int mid = (beg + end) / 2;
        if (number == a[mid])
            return mid;

        if (a[beg] <= a[end]) {
            //this subarray is not rotated
            if (number < a[mid])
                return searchHelper(a, number, beg, mid - 1);
            else
                return searchHelper(a, number, mid + 1, end);
        } else {
            //this is rotated --- here one subarray must be sorted and other must not be
            if (a[beg] <= a[mid]) {
                if (number >= a[beg] && number < a[mid])
                    return searchHelper(a, number, beg, mid - 1);
                else
                    return searchHelper(a, number, mid + 1, end);
            } else {
                if (number > a[mid] && number <= a[end])
                    return searchHelper(a, number, mid + 1, end);
                else
                    return searchHelper(a, number, beg, mid - 1);
            }
        }
    }
}
