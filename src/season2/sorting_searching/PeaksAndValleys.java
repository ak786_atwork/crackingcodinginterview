package season2.sorting_searching;

import util.Utils;

import javax.rmi.CORBA.Util;
import java.util.Arrays;

//make a sequence of peaks and valleys eg pvpvp....
public class PeaksAndValleys {

    public static void main(String[] args) {
        int[] nums1 = {3, 5, 1, 7, 2};
        int[] nums2 = {3, 5, 1, 7, 2};

        Utils.printArray(nums1);
        Solution1.getSequence(nums1);
        Utils.printArray(nums1);

        Solution2.getSequence(nums2);
        Utils.printArray(nums2);

    }

    //from ctci
    private static class Solution2 {
        public static void getSequence(int[] nums) {
            for (int i = 0; i < nums.length; i += 2) {
                int bigIndex = maxIndex(nums, i - 1, i, i + 1);

                if (bigIndex != i) {
                    Solution1.swap(nums, i, bigIndex);
                }

            }
        }

        private static int maxIndex(int[] nums, int a, int b, int c) {
            int aValue = (a >= 0 && a < nums.length) ? nums[a] : Integer.MIN_VALUE;
            int bValue = (b >= 0 && b < nums.length) ? nums[b] : Integer.MIN_VALUE;
            int cValue = (c >= 0 && c < nums.length) ? nums[c] : Integer.MIN_VALUE;

            int maxValue = Math.max(aValue, Math.max(bValue, cValue));
            if (maxValue == aValue) return a;
            else if (maxValue == bValue) return b;
            else return c;
        }
    }
    private static class Solution1 {

        public static void getSequence(int[] nums) {
            Arrays.sort(nums);
            for (int i = 1; i < nums.length; i += 2) {
                swap(nums, i - 1, i);
            }
        }

        private static void swap(int[] nums, int left, int right) {
            int temp = nums[left];
            nums[left] = nums[right];
            nums[right] = temp;
        }

    }


}
