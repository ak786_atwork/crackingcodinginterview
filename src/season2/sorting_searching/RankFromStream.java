package season2.sorting_searching;


public class RankFromStream {

    public static void main(String[] args) {
        int[] stream = {5, 1, 4, 2, 3, 6};

        StreamSolution streamSolution = new StreamSolution();

        Solution2 solution2 = new Solution2();
        //test
        for (int i = 0; i < stream.length; i++) {
            System.out.println("inserting "+stream[i]);
//            streamSolution.insertIntoBST(stream[i]);
            solution2.track(stream[i]);
            printRank(stream, solution2);
//            printRank(stream, streamSolution);
        }

    }

    private static void printRank(int[] stream, StreamSolution streamSolution) {
        int rank = 0;
        for (int i = 0; i < stream.length; i++) {
            rank = streamSolution.getRankOfNumber(stream[i]);
            System.out.println("number = " + stream[i] + " rank = " + rank);
        }

        System.out.println("------------------------------------------------");
    }
    private static void printRank(int[] stream, Solution2 streamSolution) {
        int rank = 0;
        for (int i = 0; i < stream.length; i++) {
            rank = streamSolution.getRank(stream[i]);
            System.out.println("number = " + stream[i] + " rank = " + rank);
        }

        System.out.println("------------------------------------------------");
    }


    //my if node is not present it returns -1 else it return rank and rank of smallest number is 0
    private static class StreamSolution {
        Node root;

        public void setStreamNumbers(int[] array) {
            for (int num : array) {
                insertIntoBST(num);
            }
        }

        public void setStreamNumber(int num) {
            insertIntoBST(num);
        }

        public int getRankOfNumber(int num) {
            if (root == null) return -1;

            int rank = 0;
            Node temp = root;

            while (temp != null) {
                if (num == temp.data) {
                    rank += temp.rank;
                    return rank;
                } else if (num > temp.data) {
                    rank += temp.rank + 1;
                    temp = temp.right;
                } else {
                    //left
                    temp = temp.left;
                }
            }

            //number doesn't exist
            return -1;
        }

        private void insertIntoBST(int num) {
            if (root == null) {
                root = new Node(num, 0);
                return;
            }

            Node temp = root;
            Node parent = null;

            while (temp != null) {
                parent = temp;

                if (num < temp.data) {
                    temp.rank++;
                    temp = temp.left;
                } else if (num > temp.data) {
                    temp = temp.right;
                } else {
                    //data same so increase the degree
                    temp.rank++;
                    break;
                }
            }

            if (temp == null) {
                //number not inserted ie number not equal
                if (num > parent.data) {
                    //means right child
                    parent.right = new Node(num, 0);
                } else {
                    parent.left = new Node(num, 0);
                }
            }
        }

    }

    private static class Node {
        int data;
        int rank;
        Node left, right;

        public Node(int data, int smallerEqualNodeCount) {
            this.data = data;
            this.rank = smallerEqualNodeCount;
        }
    }

    //from ctci
    private static class Solution2 {
        RankNode root;

        public void track(int number) {
            if (root == null) {
                root = new RankNode(number);
            } else {
                root.insert(number);
            }
        }

        public int getRank(int num) {
            return root.getRank(num);
        }

    }
    private static class RankNode {
        int data;
        int left_size;
        RankNode left, right;

        public RankNode(int data) {
            this.data = data;
        }

        public void insert(int num) {
            if (num <= data) {
                if (left != null) left.insert(num);
                else left = new RankNode(num);
                left_size++;
            } else {
                if (right != null) right.insert(num);
                else right = new RankNode(num);
            }
        }

        public int getRank(int num) {
            if (num == data)
                return left_size;
            if (num < data) {
                if (left == null) return -1;
                else return left.getRank(num);
            } else {
                if (right == null) return -1;
                else return right.getRank(num) + left_size + 1;
            }
        }

    }


}
