package season2.sorting_searching;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;


//from ctci
/* 4 billion non negative integers are given. find missing int with memory of 1 gb*/
public class MissingInt {

    public static void main(String[] args) throws FileNotFoundException {
        Solution solution = new Solution();
        solution.findOpenNumber();
    }


    private static class Solution {
        long numberOfInts = ((long) Integer.MAX_VALUE) + 1;
        byte[] bitField = new byte[(int) (numberOfInts / 8)];
        String fileName = "number.txt";

        void findOpenNumber() throws FileNotFoundException {
            Scanner in = new Scanner(new FileReader(fileName));

            while (in.hasNext()) {
                int n = in.nextInt();
                /*finds the corresponding number in bitfield by using OR operator to set the nth bit of a byte
                 * (e.g 10 would correspond to 2nd bit of index 2 in the byte array)
                 * */
                bitField[n / 8] |= 1 << (n % 8);
            }

            for (int i = 0; i < bitField.length; i++) {
                for (int j = 0; j < 8; j++) {
                    /*retrieves the individual bits. When 0 bit is found, print the corresponding value*/
                    if ((bitField[i] & 1 << j) == 0) {
                        System.out.println(i * 8 + j);
                        return;
                    }
                }
            }

        }

    }


    /*same task by using 10mb of memory and all values are distinct*/
    private static class Solution2 {

        public static int findOpenNumber(String fileName) throws FileNotFoundException {
            int rangeSize = (1 << 20); // 2^20 bits (2^17 bytes)

            /*get count of number of values within each block*/
            int[] blocks = getCountPerBlock(fileName, rangeSize);

            /*find a block with missing value*/
            int blockIndex = findBlockWithMissing(blocks, rangeSize);

            if (blockIndex < 0) return -1;

            /*create a bit vector for item within this range*/
            byte[] bitVector = getBitVectorForRange(fileName, blockIndex, rangeSize);

            /*find a zero in the bit vector*/
            int offset = findZero(bitVector);

            if (offset < 0) return -1;

            /*compute missing value*/
            return blockIndex * rangeSize + offset;
        }

        /*find bit index that is 0 within byte*/
        private static int findZero(byte b) {
            for (int i = 0; i < Byte.SIZE; i++) {
                int mask = 1 << i;

                if ((b & mask) == 0) {
                    return i;
                }
            }
            return -1;
        }

        private static int findZero(byte[] bitVector) {
            for (int i = 0; i < bitVector.length; i++) {
                if (bitVector[i] != ~0) {   //if not all 1s
                    int bitIndex = findZero(bitVector[i]);
                    return i * Byte.SIZE + bitIndex;
                }
            }

            return -1;
        }

        /*create a bit vector for the values within specific range*/
        private static byte[] getBitVectorForRange(String fileName, int blockIndex, int rangeSize) throws FileNotFoundException {
            int startRange = blockIndex * rangeSize;
            int endRange = startRange + rangeSize;
            byte[] bitVector = new byte[rangeSize / Byte.SIZE];

            Scanner in = new Scanner(new FileReader(fileName));

            while (in.hasNext()) {
                int value = in.nextInt();

                /*if number is inside the block which is missing number, we record it*/
                if (startRange <= value && value < endRange) {
                    int offset = value - startRange;
                    int mask = (1 << (offset % Byte.SIZE));
                    bitVector[offset / Byte.SIZE] |= mask;
                }
            }

            in.close();
            return bitVector;
        }

        private static int findBlockWithMissing(int[] blocks, int rangeSize) {
            for (int i = 0; i < blocks.length; i++) {
                if (blocks[i] < rangeSize)
                    return i;
            }

            return -1;
        }

        /*get count of items within each range*/
        private static int[] getCountPerBlock(String fileName, int rangeSize) throws FileNotFoundException {
            int arraySize = Integer.MAX_VALUE / rangeSize + 1;
            int[] blocks = new int[arraySize];
            Scanner in = new Scanner(new FileReader(fileName));

            while (in.hasNext()) {
                int value = in.nextInt();
                blocks[value / rangeSize]++;
            }

            in.close();
            return blocks;
        }


    }

}
