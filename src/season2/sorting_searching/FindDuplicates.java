package season2.sorting_searching;


//integers from 1 to N where n <= 32000, and you have given 4kb of memory , print duplicates
public class FindDuplicates {

    public static void main(String[] args) {
        int[] array = {1,1,1,2,3,4,8,8};

        findDuplicates(array,10);

    }



    public static void findDuplicates(int[] array, int range) {
        int mask;
        byte[] bitVector = new byte[range/Byte.SIZE + 1];

        for (int num : array) {
            mask = 1 << (num % 8);

            if ((bitVector[num/8] & mask) != 0) {
                //already set duplicate found
                System.out.println(num+" ");
            } else {
                bitVector[num/8] |= mask;
            }
        }

    }

}
