package season2.sorting_searching;


/*
 * all number should be +ve and if index is out of bounds it return -1
 * */
public class FindNumberWithoutSize {

    public static void main(String[] args) {
        int[] array = {1,2,3,6,7,9};
        Listy listy = new Listy(array);
        int number = 90;

        System.out.println(Solution.findNumberIndex(number,listy));
    }

    private static class Solution {
        public static int findNumberIndex(int number, Listy listy) {
            return helper(number, 0, 1, listy);
        }

        private static int helper(int number, int beg, int end, Listy listy) {
            if (beg > end) return -1;

            int mid = (beg + end) / 2;

            if (listy.getNumber(mid) == -1) {
                //out of bounds
                if (listy.getNumber(beg) == -1 || number < listy.getNumber(beg))
                    return -1;
                return helper(number, beg, mid - 1, listy);
            } else {
                if (listy.getNumber(mid) == number) {
                    return mid;
                } else if (number > listy.getNumber(mid)) {
                    if (number > listy.getNumber(end))
                        return helper(number, end + 1, 2 * end + 1, listy);
                    else
                        return helper(number,mid+1,end,listy);
                } else {
                    if (number < listy.getNumber(beg))
                        return -1; //number not exist
                    else
                        return helper(number,beg,mid-1,listy);
                }
            }

        }
    }

    private static class Listy {
        int[] array;

        public Listy(int[] array) {
            this.array = array;
        }

        public int getNumber(int index) {
            if (index < 0 || index >= array.length)
                return -1;
            return array[index];
        }

    }

}
