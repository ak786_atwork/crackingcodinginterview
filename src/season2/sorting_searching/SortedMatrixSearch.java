package season2.sorting_searching;

public class SortedMatrixSearch {

    public static void main(String[] args) {
        int[][] matrix = {
                {1,2,3,4},
                {6,7,8,19}
        };

        System.out.println(searchMatrix(matrix,9));
        System.out.println(findElement(matrix,19));
    }





    public static boolean searchMatrix(int[][] matrix, int target) {
        //check if target is out of bound
        if (matrix[matrix.length-1][matrix[0].length-1] < target || target < matrix[0][0])
            return false;

        int colLen = matrix[0].length-1;
        int row = 0;

        boolean outOfBound = true;

        while (row < matrix.length) {
            if (target >= matrix[row][0] && target <= matrix[row][colLen]) {
                outOfBound = false;
                break;
            }
            row++;
        }

        if (!outOfBound) {
            int begCol = 0;
            int endCol = colLen;
            int mid = 0;
            while (begCol <= endCol) {
                mid = (begCol +endCol)/2;
                if (matrix[row][mid] == target)
                    return true;
                else if (matrix[row][mid] > target)
                    endCol = mid-1;
                else
                    begCol = mid+1;
            }
        }

        return false;

    }

    //from ctci
    public static boolean findElement(int[][] matrix, int number) {
        int row = 0;
        int col = matrix[0].length-1;

        while (row < matrix.length && col >=0) {
            if (matrix[row][col] == number)
                return true;
            else if (matrix[row][col] > number)
                col--;
            else
                row++;
        }

        return false;
    }

}
