package season2.sorting_searching;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

public class GroupAnagram {


    public static void main(String[] args) {
        String[] words = {"abc","cba","xy","bac"};
        printWords(words);
        Solution2.groupAnagram(words);
        printWords(words);
    }

    private static void printWords(String[] words) {
        for (String word : words)
            System.out.print(word+" ");
        System.out.println();
    }


    private static class Solution1 {

        public static void groupAnagram(String[] words) {
            HashMap<String, ArrayList<String>> map = new HashMap<>();
            char[] temp;
            String key;

            for (String word : words) {
                temp = word.toCharArray();
                Arrays.sort(temp);

                key = new String(temp);
                if (map.containsKey(key)){
                    map.get(key).add(word);
                } else {
                    ArrayList<String> list = new ArrayList<>();
                    list.add(word);
                    map.put(key,list);
                }
            }

            orderArray(map, words);
        }

        private static void orderArray(HashMap<String, ArrayList<String>> map, String[] words) {
            int i = 0;
            for (String key : map.keySet()) {
                for (String word : map.get(key))
                    words[i++] = word;
            }
        }

    }

    private static class Solution2 {
        public static void groupAnagram(String[] words) {
            HashMap<String, ArrayList<String>> map = new HashMap<>();
            String key;

            for (String word : words) {
                key = getKey(word);
                if (map.containsKey(key)){
                    map.get(key).add(word);
                } else {
                    ArrayList<String> list = new ArrayList<>();
                    list.add(word);
                    map.put(key,list);
                }
            }

            orderArray(map, words);
        }

        private static void orderArray(HashMap<String, ArrayList<String>> map, String[] words) {
            int i = 0;
            for (String key : map.keySet()) {
                for (String word : map.get(key))
                    words[i++] = word;
            }
        }

        public static String getKey(String word) {
            HashMap<Character,Integer> mapFrequency = new HashMap<>();
            for (char c : word.toCharArray()) {
                if (mapFrequency.containsKey(c)) {
                    mapFrequency.replace(c, mapFrequency.get(c) + 1);
                } else {
                    mapFrequency.put(c,1);
                }
            }

            StringBuilder sb = new StringBuilder();

            for (char key : mapFrequency.keySet()) {
                sb.append(key).append(mapFrequency.get(key));
            }

            return sb.toString();
        }
    }

}
