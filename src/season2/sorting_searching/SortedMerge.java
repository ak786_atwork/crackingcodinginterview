package season2.sorting_searching;

public class SortedMerge {

    public static void main(String[] args) {
        int[] a = {1, 2, 4, 4, 4, 4};
        int[] b = {3, 10, 22};

        printArray(a);
        mergeSortedArray(a, b);
        printArray(a);
    }

    private static void printArray(int[] a) {
        for (int item : a)
            System.out.print(item + " ");
        System.out.println();
    }


    public static void mergeSortedArray(int[] a, int[] b) {

        int bIndex = b.length - 1;
        int aIndex = a.length - b.length - 1;

        for (int i = a.length - 1; i >= 0; i--) {
            if (bIndex >= 0 && aIndex >= 0) {
                if (b[bIndex] > a[aIndex]) {
                    //add b to last
                    a[i] = b[bIndex--];
                } else {
                    a[i] = a[aIndex--];
                }
            } else if (bIndex >= 0) {
                a[i] = b[bIndex--];
            } else {
                a[i] = a[aIndex--];
            }

        }

    }
}
