package season2.sorting_searching;


public class SparseSearch {

    public static void main(String[] args) {
        String[] words = {"ab","","","","","bfjk","","","def","dfe"};
        String word = "ab";
        System.out.println(Solution.findWord(word,words));

    }


/*either you first find the length try doubling it like 1, 2,4,8 ... and then use binary search on it
* or below implemented one
*  */
    private static class Solution {
        public static int findWord(String word, String[] array) {
            return helper(word, array, 0, array.length - 1);
        }

        private static int helper(String word, String[] array, int beg, int end) {
            if (beg > end) return -1;

            int mid = (beg + end) / 2;
            int movedIndex = Math.max(mid-1, beg);  // in case of less than 3 elements
            boolean isMoved = false;

            if (array[mid].equals("")) {
                //move to left until u get non-empty string
                while (movedIndex >= beg && array[movedIndex].equals("")) {
                    movedIndex--;
                    isMoved = true;
                }
            }

            if (isMoved && movedIndex + 1 == beg) {
                //all empty strings on left
               return helper(word, array, mid + 1, end);
            }

            //compare char and move to left or right
            int comparision = compare(word, array[movedIndex]);
            if (comparision == 0)
                return movedIndex;
            else if (comparision == 1) {
                //search right
                return helper(word,array,mid+1,end);
            } else {
                return helper(word,array,beg,Math.min(movedIndex,mid-1));
            }

        }

        private static int compare(String word1, String word2) {
            int len = Math.min(word1.length(), word2.length());
            for (int i = 0; i < len; i++) {
                if (word1.charAt(i) > word2.charAt(i)) {
                    return 1;
                } else if (word1.charAt(i) < word2.charAt(i)) {
                    return -1;
                }
            }
            if (word1.length() < word2.length()) {
                return -1;
            } else if (word1.length() > word2.length())
                return 1;

            return 0;
        }
    }
}
