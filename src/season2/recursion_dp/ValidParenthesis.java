package season2.recursion_dp;

import java.util.ArrayList;
import java.util.List;

public class ValidParenthesis {

    public static void main(String[] args) {
        generateAllParenthesis(4);
    }

    public static List<String> generateAllParenthesis(int n) {
        List<String> list = new ArrayList<>();
        findAllPossibleParenthesis(n, 0,0, list,"");
        System.out.println(list.toString());
        return list;
    }

    private static void findAllPossibleParenthesis(int max, int open, int closed, List<String> list, String currentString) {
        if (closed == max) {
            list.add(currentString);
            return;
        }

        if (open < max) {
            findAllPossibleParenthesis(max, open+1, closed, list, currentString+"(");
        }
        if (closed<open) {
            findAllPossibleParenthesis(max, open, closed+1, list, currentString+")");
        }
    }


    //from geeksforgeeks
    static class GFG
    {
        // Function that print all combinations of
        // balanced parentheses
        // open store the count of opening parenthesis
        // close store the count of closing parenthesis
        static void _printParenthesis(char str[], int pos, int n, int open, int close)
        {
            if(close == n)
            {
                // print the possible combinations
                for(int i=0;i<str.length;i++)
                    System.out.print(str[i]);
                System.out.println();
                return;
            }
            else
            {
                if(open > close) {
                    str[pos] = '}';
                    _printParenthesis(str, pos+1, n, open, close+1);
                }
                if(open < n) {
                    str[pos] = '{';
                    _printParenthesis(str, pos+1, n, open+1, close);
                }
            }
        }

        // Wrapper over _printParenthesis()
        static void printParenthesis(char str[], int n)
        {
            if(n > 0)
                _printParenthesis(str, 0, n, 0, 0);
            return;
        }

        // driver program
        public static void main (String[] args)
        {
            int n = 3;
            char[] str = new char[2 * n];
            printParenthesis(str, n);
        }
    }

}
