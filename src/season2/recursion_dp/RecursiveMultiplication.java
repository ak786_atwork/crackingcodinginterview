package season2.recursion_dp;

public class RecursiveMultiplication {

    public static void main(String[] args) {
        System.out.println(Solution1.minProduct(9,9));
        System.out.println(Solution3.minProduct(9,9));
        System.out.println(Solution3.minProduct(8,8));
    }

    public int getMinimumOperation(int currentNumber, int m, int n) {
        return 0;
    }


    //good approach
    static class Solution1 {

        static int minProduct(int a, int b) {
            int bigger = Math.max(a, b);
            int smaller = Math.min(a, b);

            return minProductHelper(smaller, bigger);
        }

        private static int minProductHelper(int smaller, int bigger) {
            if (smaller == 0) return 0;
            if (smaller == 1) return bigger;

            /*compute half. if uneven compute other half*/
            int s = smaller >> 1;
            int side1 = minProduct(s, bigger);
            int side2 = side1;

            if (smaller % 2 == 1)
                side2 = minProductHelper(smaller - s, bigger);

            return side1 + side2;


        }

    }

    //better approach
    static class Solution2 {

        static int minProduct(int a, int b) {
            int bigger = Math.max(a, b);
            int smaller = Math.min(a, b);

            int[] memo = new int[smaller+1];

            return minProduct(smaller, bigger,memo);
        }

        private static int minProduct(int smaller, int bigger,int[] memo) {
            if (smaller == 0) return 0;
            else if (smaller == 1) return bigger;
            else if (memo[smaller] > 0) return memo[smaller];

            /*compute half. if uneven compute other half*/
            int s = smaller >> 1;
            int side1 = minProduct(s, bigger);
            int side2 = side1;

            if (smaller % 2 == 1)
                side2 = minProduct(smaller - s, bigger,memo);

            memo[smaller] = side1+side2;
            return memo[smaller];
        }

    }

    //more better approach
    static class Solution3 {

        static int minProduct(int a, int b) {
            int bigger = Math.max(a, b);
            int smaller = Math.min(a, b);

            return minProductHelper(smaller, bigger);
        }

        private static int minProductHelper(int smaller, int bigger) {
            if (smaller == 0) return 0;
            if (smaller == 1) return bigger;

            /*compute half. if uneven compute other half*/
            int s = smaller >> 1;
            int side1 = minProduct(s, bigger);
            int side2 = side1;

            if (smaller % 2 == 1)
                side2 += bigger;

            return side1 + side2;


        }

    }

}
