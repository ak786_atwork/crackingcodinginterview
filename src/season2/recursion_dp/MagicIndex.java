package season2.recursion_dp;

public class MagicIndex {

    public static void main(String[] args) {
//        int[] array = {-4,2,2,3,6};
//        int[] array = {-5,-5,2,2,2,3,5,12};         // getMagicIndex = -1, getMagicIndex1 = 2
        int[] array = {-1,-1,-1,1,2,3,5,7};         // getMagicIndex = 7, getMagicIndex1 = 7

        System.out.println(getMagicIndex(array));
        System.out.println(getMagicIndex1(array));
    }


    //this algo wont work in case of duplicated elements eg. -5,-5,2,2,2,3,5,7,7,12   on left ,  on right =  -1,-1,-1,1,2,3,5,7
    // when A[mid] < mid we can't conclude whether magic index is on left or right
    public static int getMagicIndex(int[] array) {
        int beg = 0;
        int end = array.length-1;
        int mid;

        while (beg <= end) {
            mid = (beg+end)/2;
            if (mid == array[mid])
                return mid;
            else if (mid > array[mid]) {
                //go right
                beg = mid+1;
            } else {
                end = mid-1;
            }
        }
        return -1;
    }


    //this will work in case of duplicate also
    public static int magicIndexHelper(int[] array, int beg, int end) {
        if (beg > end) return -1;

        int mid = (beg + end)/2;

        if (mid == array[mid]) return mid;

        //search left
        int leftIndex = Math.min(mid-1, array[mid]);
        int left = magicIndexHelper(array, beg, leftIndex);
        if (left >= 0) return left;

        //search right
        int rightIndex = Math.max(mid+1, array[mid]);
        int right = magicIndexHelper(array, rightIndex, end);
        if (right >= 0)
            return right;

        return -1;
    }

    public static int getMagicIndex1(int[] array) {
        int beg = 0;
        int end = array.length-1;

        return magicIndexHelper(array,beg,end);
    }
}
