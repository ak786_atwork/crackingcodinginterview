package season2.recursion_dp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class BooleanEvaluation {

    public static void main(String[] args) {
//        String input = "1^0|0|1";  // false - 2
//        String input = "T^F|F|T";
        String input = "0&0&0&1^1|0"; // true - 10
        System.out.println(Solution2.countEval(input,true,new HashMap<>()));


    /*    Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        String input;
        while (test-- > 0) {
            sc.nextInt();
            sc.nextLine();
            input = sc.nextLine();
            System.out.println(Solution.diffWaysToCompute(input,true));
        }*/

    }


    //inspired from differentways to add parens
    private static class Solution {
        public static int diffWaysToCompute(String input,boolean desiredVal) {
            List<Integer> list = helper(0,input.length()-1,input);
            int val = desiredVal ? 1 : 0;
            int count = 0;

            for (int i : list) {
                if (i == val)
                    count++;
            }

            return count;
        }

        private static int getNumberFromExp(int lastNum, char lastOperator, int currentNumber) {
            switch (lastOperator) {
                case '|' : return lastNum | currentNumber;
                case '&' : return lastNum & currentNumber;
                case '^' : return lastNum ^ currentNumber;
            }
            return 0;
        }


        private static List<Integer> helper(int left, int right, String input) {
            List<Integer> resultList = new ArrayList<>();
            List<Integer> leftList;
            List<Integer> rightList;

            for (int i = left;i<right;i++) {
                if (isOperator(input.charAt(i))) {
                    //partition
                    leftList = helper(left,i-1,input);
                    rightList = helper(i+1,right,input);

                    for (int l : leftList) {
                        for (int r: rightList) {
                            resultList.add(getNumberFromExp(l,input.charAt(i),r));
                        }
                    }
                }
            }

            if (resultList.isEmpty()) {
                if (input.substring(left,right+1).equals("T"))
                    resultList.add(1);
                else resultList.add(0);
            }

            return resultList;
        }

        private static boolean isOperator(char c) {
            return c == '|' || c == '&' || c == '^';
        }
    }


    //input = "1^0|0|1"
    private static class Solution1 {

        public static int countEval(String s, boolean result) {
            if (s.length() == 0) return 0;
            if (s.length() == 1) return stringToBool(s) == result? 1 : 0;

            int ways = 0;

            for (int i = 1;i<s.length();i +=2) {
                char c = s.charAt(i);
                String left = s.substring(0,i);
                String right = s.substring(i+1, s.length());

                //evaluate each side for each result
                int leftTrue = countEval(left,true);
                int rightTrue = countEval(right,true);
                int rightFalse = countEval(right,false);
                int leftFalse = countEval(left,false);

                int total = (leftFalse + leftTrue) * (rightFalse + rightTrue);
                int totalTrue = 0;

                if (c == '^') {
                    //required : one true one false
                    totalTrue = leftTrue * rightFalse + leftFalse * rightTrue;
                } else if (c == '&') {
                    //required both true
                    totalTrue = rightTrue * leftTrue;
                } else if (c == '|') {
                    //required one true or both true
                    totalTrue = leftTrue * rightFalse + leftFalse * rightTrue + leftTrue * rightTrue;
                }

                int subways = result ? totalTrue : total -totalTrue;
                ways += subways;
            }

            return ways;
        }

        private static boolean stringToBool(String s) {
            return s.equals("1");
        }

    }

    //input = "1^0|0|1"
    private static class Solution2 {

        public static int countEval(String s, boolean result, HashMap<String, Integer> map) {
            if (s.length() == 0) return 0;
            if (s.length() == 1) return stringToBool(s) == result? 1 : 0;
            if (map.containsKey(result + s))
                return map.get(result + s);

            int ways = 0;

            for (int i = 1;i<s.length();i +=2) {
                char c = s.charAt(i);
                String left = s.substring(0,i);
                String right = s.substring(i+1, s.length());

                //evaluate each side for each result
                int leftTrue = countEval(left,true,map);
                int rightTrue = countEval(right,true,map);
                int rightFalse = countEval(right,false,map);
                int leftFalse = countEval(left,false,map);

                int total = (leftFalse + leftTrue) * (rightFalse + rightTrue);
                int totalTrue = 0;

                if (c == '^') {
                    //required : one true one false
                    totalTrue = leftTrue * rightFalse + leftFalse * rightTrue;
                } else if (c == '&') {
                    //required both true
                    totalTrue = rightTrue * leftTrue;
                } else if (c == '|') {
                    //required one true or both true
                    totalTrue = leftTrue * rightFalse + leftFalse * rightTrue + leftTrue * rightTrue;
                }

                int subways = result ? totalTrue : total -totalTrue;
                ways += subways;
            }
            map.put(result + s,ways);
            return ways;
        }

        private static boolean stringToBool(String s) {
            return s.equals("1");
        }

    }

}
