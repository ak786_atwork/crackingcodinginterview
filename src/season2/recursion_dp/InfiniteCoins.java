package season2.recursion_dp;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;


public class InfiniteCoins {

    public static void main(String[] args) {
        /*Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        int arraySize;
        int[] coins;
        int cents;
        while (test-- > 0) {
            arraySize = sc.nextInt();
            coins = new int[arraySize];
            for (int i = 0; i < arraySize; i++)
                coins[i] = sc.nextInt();

            cents = sc.nextInt();
            Arrays.sort(coins);
            System.out.println(Solution1.getAllPossibleCoinChange(cents,coins));
        }*/

        int[] coins = {3,5,7,8,9,10,11};
        int amount  = 500;
        System.out.println(Solution1.getAllPossibleCoinChange(amount,coins));

    }

    //this didn't work
    /*public static int getAllPossibleCoinChange(int n, int[] coins) {
        if (n == 0) return 0;

        return getChange(n, coins);
    }

    private static int getChange(int n, int[] coins) {
        if (n == 0) return 1;
        else if (n < 0) return 0;
        else {
            int count = 0;
            for (int i = 0; i < coins.length; i++)
                count += getChange(n - coins[i], coins);

            return count;
        }
    }*/


    //accepted solution 1468 ms submitted on leetcode
    static class Solution1 {
        static HashMap<String, Integer> map = new HashMap<>();
        public static int getAllPossibleCoinChange(int n, int[] coins) {
            map.clear();
            return getChange(n, coins,coins.length-1);
        }

        private static int getChange(int n, int[] coins,int index) {
            if (n == 0) return 1;
            else if (n < 0 || index < 0) return 0;
            else {
                if (map.containsKey(index+":"+n))
                    return map.get(index+":"+n);

                int count = 0;
                for (int i = 0; i <= n; i += coins[index]) {
                    count += getChange(n-i,coins,index-1);
                }

                map.put(index+":"+n,count);

                return count;
            }
        }
    }

    //from leetcode
    static class Solution {
        public int change(int amount, int[] coins) {
            int[] dp = new int[amount + 1];
            dp[0] = 1;
            for (int coin : coins) {
                for (int i = coin; i <= amount; i++) {
                    dp[i] += dp[i-coin];
                }
            }
            return dp[amount];
        }
    }
}
