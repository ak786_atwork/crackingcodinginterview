package season2.recursion_dp;

import java.util.ArrayList;
import java.util.List;

public class DifferentWaysToAddParenthesis {

    public static void main(String[] args) {
        String input = "2*3-4*5";
        System.out.println(Solution.diffWaysToCompute(input).toString());
    }

    //submitted to leetcode
    private static class Solution {
        public static List<Integer> diffWaysToCompute(String input) {
            return helper(0,input.length()-1,input);
        }

        private static int getNumberFromExp(int lastNum, char lastOperator, int currentNumber) {
            switch (lastOperator) {
                case '*' : return lastNum * currentNumber;
                case '+' : return lastNum + currentNumber;
                case '-' : return lastNum - currentNumber;
            }
            return 0;
        }


        private static List<Integer> helper(int left, int right, String input) {
            List<Integer> resultList = new ArrayList<>();
            List<Integer> leftList;
            List<Integer> rightList;

            for (int i = left;i<right;i++) {
                if (isOperator(input.charAt(i))) {
                    //partition
                    leftList = helper(left,i-1,input);
                    rightList = helper(i+1,right,input);

                    for (int l : leftList) {
                        for (int r: rightList) {
                            resultList.add(getNumberFromExp(l,input.charAt(i),r));
                        }
                    }
                }
            }

            if (resultList.isEmpty()) {
                resultList.add(Integer.parseInt(input.substring(left,right+1)));
            }

            return resultList;
        }

        private static boolean isOperator(char c) {
            return c == '+' || c == '-' || c == '*';
        }
    }

    //100% faster from leetcode
    static class Solution2 {
        private List<Integer>[][] dp;
        public List<Integer> diffWaysToCompute(String input) {
            int n = input.length();
            dp = new List[n][n];

            return getResults(input, 0, n - 1);
        }

        private List<Integer> getResults(String input, int start, int end){
            if(dp[start][end] != null)
                return dp[start][end];
            List<Integer> res = new ArrayList<>();
            for (int i = start; i <= end; i++) {
                char ch = input.charAt(i);
                if(ch == '-' || ch == '+' || ch == '*'){
                    List<Integer> as = getResults(input, start, i - 1);
                    List<Integer> bs = getResults(input, i + 1, end);
                    for(int a : as){
                        for(int b : bs){
                            if(ch == '-')
                                res.add(a - b);
                            else if(ch == '+')
                                res.add(a + b);
                            else if(ch == '*')
                                res.add(a * b);
                        }
                    }
                }
            }

            if(res.size() == 0)
                res.add(Integer.parseInt(input.substring(start, end + 1)));

            dp[start][end] = res;
            return res;
        }
    }

}
