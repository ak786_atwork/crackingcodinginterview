package season2.recursion_dp;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class PowerSetOrSubset {

    public static void main(String[] args) {
        int[] nums = {1, 5, 3,4};

//        System.out.println(getSubset(nums).toString());
        System.out.println(getSubset2(nums));

//        System.out.println(getSubset(3).toString());
//        System.out.println(getSubset(4).toString());
//        System.out.println(getSubset(5).toString());
    }


    public static List<List<Integer>> getSubset(int n) {
        List<List<Integer>> resultList = new LinkedList<>();
        resultList.add(new LinkedList<>());
        List<List<Integer>> lastList = new LinkedList<>();

        for (int i = 1; i <= n; i++) {
            lastList.clear();
            for (int j = 0; j < resultList.size(); j++) {
                LinkedList<Integer> linkedList = new LinkedList<>(resultList.get(j));
                linkedList.add(i);
                lastList.add(linkedList);
            }
            resultList.addAll(lastList);
        }

        return resultList;
    }


    //submitted on leetcode
    public static List<List<Integer>> getSubset(int[] nums) {
        List<List<Integer>> resultList = new LinkedList<>();
        resultList.add(new LinkedList<>());
        List<List<Integer>> lastList = new LinkedList<>();

        for (int i = 0; i < nums.length; i++) {
            lastList.clear();
            for (int j = 0; j < resultList.size(); j++) {
                LinkedList<Integer> linkedList = new LinkedList<>(resultList.get(j));
                linkedList.add(nums[i]);
                lastList.add(linkedList);
            }
            resultList.addAll(lastList);
        }

        return resultList;
    }

    public static List<List<Integer>> getSubset1(int[] nums) {
        List<List<Integer>> resultList = new LinkedList<>();

        int totalSubsets = (int) Math.pow(2, nums.length);
        String binaryString;
        int currentIndex = 0;

        for (int i = 0; i < totalSubsets / 2; i++) {
            LinkedList<Integer> linkedList1 = new LinkedList<>();
            LinkedList<Integer> linkedList2 = new LinkedList<>();

            binaryString = Integer.toBinaryString(i);
            currentIndex = nums.length - 1;

            for (int j = binaryString.length() - 1; j >= 0; j--) {
                if (binaryString.charAt(j) == '1') {
                    linkedList1.addLast(nums[currentIndex]);
                } else {
                    linkedList2.addLast(nums[currentIndex]);
                }
                currentIndex--;
            }
            while (currentIndex >= 0){
                linkedList2.addLast(nums[currentIndex--]);
            }
            resultList.add(linkedList1);
            resultList.add(linkedList2);
        }

        return resultList;
    }

    public static List<List<Integer>> getSubset2(int[] nums) {
        List<List<Integer>> resultList = new LinkedList<>();

        int totalSubsets = 1 << nums.length;
        int currentIndex = 0;
        int temp;

        for (int i = 0; i < totalSubsets; i++) {
            LinkedList<Integer> linkedList1 = new LinkedList<>();
            currentIndex = nums.length - 1;
            temp = i;
            while (temp != 0) {
                if ((temp & 1) == 1) {
                    linkedList1.addLast(nums[currentIndex]);
                }
                temp = temp >> 1;
                currentIndex--;
            }
            resultList.add(linkedList1);
        }
        return resultList;
    }

    //from leetcode
    static class Solution {
        public List<List<Integer>> subsets(int[] nums) {
            List<List<Integer>> results = new ArrayList<>();
            int l = nums.length;
            for (int i = 0; i < Math.pow(2,l); i++) {
                int val = i, index = 0;
                List<Integer> current = new ArrayList<>();
                while (val > 0) {
                    if (val % 2 == 1) {
                        current.add(nums[index]);
                    }
                    val = val / 2;
                    index++;
                }
                results.add(current);
            }
            return results;
        }
    }
}
