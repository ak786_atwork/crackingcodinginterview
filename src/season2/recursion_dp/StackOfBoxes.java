package season2.recursion_dp;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


//from ctci
public class StackOfBoxes {

    public static void main(String[] args) {
        List<Box> boxes = new ArrayList<>();

        boxes.add(new Box(9,9,9));
        boxes.add(new Box(8,9,8));
        boxes.add(new Box(7,7,7));
        boxes.add(new Box(5,7,6));

        System.out.println(Solution.getMaxPossibleHeight(boxes));
        System.out.println(Solution1.getMaxPossibleHeight(boxes));
        System.out.println(Solution2.getMaxPossibleHeight(boxes));

    }



    /*
    * we can't get the max possible height in one pass after sorting as we are sorting wrt one dimension, and bottom box should have
    * all three dimension greater than above one
    * */
    private static class Solution {

        static Comparator<Box> comparator = (o1, o2) -> o2.depth - o1.depth;

        public static int getMaxPossibleHeight(List<Box> boxes) {
            boxes.sort(comparator);

            int maxHeight = 0;

            for (int i = 0;i<boxes.size();i++) {
                maxHeight = Math.max(maxHeight, createStack(boxes, i));
            }

            return maxHeight;
        }

        private static int createStack(List<Box> boxes, int bottomIndex) {
            Box bottomBox = boxes.get(bottomIndex);
            int maxHeight = 0;
            for (int j = bottomIndex+1; j<boxes.size();j++) {
                if (canBeAbove(bottomBox, boxes.get(j))) {
                    maxHeight = Math.max(maxHeight, createStack(boxes,j));
                }
            }

            maxHeight += bottomBox.height;

            return maxHeight;
        }

        private static boolean canBeAbove(Box bottomBox, Box box) {
            return bottomBox.depth > box.depth && bottomBox.height > box.height && bottomBox.width > box.width;
        }

    }

    private static class Box {
        int width;
        int height;
        int depth;

        public Box(int width, int height, int depth) {
            this.width = width;
            this.height = height;
            this.depth = depth;
        }
    }

    /*memoization*/
    private static class Solution1 {

        static Comparator<Box> comparator = (o1, o2) -> o2.depth - o1.depth;

        public static int getMaxPossibleHeight(List<Box> boxes) {
            boxes.sort(comparator);

            int maxHeight = 0;
            int[] memo = new int[boxes.size()];
            for (int i = 0;i<boxes.size();i++) {
                maxHeight = Math.max(maxHeight, createStack(boxes, i, memo));
            }

            return maxHeight;
        }

        private static int createStack(List<Box> boxes, int bottomIndex, int[] memo) {
            if (bottomIndex < boxes.size() && memo[bottomIndex] > 0) return memo[bottomIndex];

            Box bottomBox = boxes.get(bottomIndex);
            int maxHeight = 0;
            for (int j = bottomIndex+1; j<boxes.size();j++) {
                if (canBeAbove(bottomBox, boxes.get(j))) {
                    maxHeight = Math.max(maxHeight, createStack(boxes,j, memo));
                }
            }

            maxHeight += bottomBox.height;
            memo[bottomIndex] = maxHeight;

            return maxHeight;
        }

        private static boolean canBeAbove(Box bottomBox, Box box) {
            return bottomBox.depth > box.depth && bottomBox.height > box.height && bottomBox.width > box.width;
        }

    }

    /*memoization*/
    private static class Solution2 {

        static Comparator<Box> comparator = (o1, o2) -> o2.depth - o1.depth;

        public static int getMaxPossibleHeight(List<Box> boxes) {
            boxes.sort(comparator);
            int[] memo = new int[boxes.size()];

            return createStack(boxes,null,0,memo);
        }

        private static int createStack(List<Box> boxes, Box bottomBox, int offset, int[] memo) {
            if (offset >= boxes.size()) return 0;

            Box newBottom = boxes.get(offset);
            int heightWithBottom = 0;
            if (bottomBox == null || canBeAbove(bottomBox, newBottom)) {
                if (memo[offset] == 0) {
                    memo[offset] = createStack(boxes,newBottom,offset+1,memo);
                    memo[offset] += newBottom.height;
                }

                heightWithBottom = memo[offset];
            }

            int heightWithoutBottom = createStack(boxes, bottomBox, offset+1, memo);

            return Math.max(heightWithBottom,heightWithoutBottom);
        }


        private static boolean canBeAbove(Box bottomBox, Box box) {
            return bottomBox.depth > box.depth && bottomBox.height > box.height && bottomBox.width > box.width;
        }

    }

}
