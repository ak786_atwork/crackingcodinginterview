package season2.recursion_dp;

public class TripleStep {
    private static int bottomUpCount = 0;
    private static int topDownCount = 0;

    public static void main(String[] args) {
        bottomUpCount = 0;
        topDownCount = 0;
//        System.out.println(getAllPossibleWay(37) + " top down count = " + topDownCount);
//        System.out.println(getAllPossibleWay(0, 5) + " bottom up count = " + bottomUpCount);

        System.out.println("----");
        SolutionDP.getAllPossibleWay(37);

        /*
            at 37 this will exceed the integer bound, u can overcome this situation by using bigInteger class
        * -463960867 top down count = 103
          -463960867 bottom up count = 34
        *
        * */


    }


    // can take step of 1,2,3
    public static int getAllPossibleWay(int n) {
        topDownCount++;
        if (n == 0) return 1;
        else if (n < 0) return 0;
        else return getAllPossibleWay(n - 1) + getAllPossibleWay(n - 2) + getAllPossibleWay(n - 3);
    }


    //bottom up
    public static int getAllPossibleWay(int start, int n) {
        bottomUpCount++;
        if (start == n) return 1;
        else if (start > n) return 0;
        else return getAllPossibleWay(start + 1, n) + getAllPossibleWay(start + 2, n) + getAllPossibleWay(start + 3, n);
    }

    static class SolutionDP {

        static int bottomUpCount = 0;
        static int topDownCount = 0;

        public static void getAllPossibleWay(int n) {
            bottomUpCount = 0;
            topDownCount = 0;
            System.out.println(getAllPossibleWayTopDown(n, new int[n + 1]) + " top down count = " + topDownCount);
            System.out.println(getAllPossibleWayBottomUp(n) + " bottom up count = " + bottomUpCount);

        }

        private static int getAllPossibleWayBottomUp(int n) {
            if (n < 3) return n;
            else if (n == 3) return 4;

            int[] memo = new int[n + 1];
            memo[1] = 1;        //ways to get 1 is only 1 through 1
            memo[2] = 2;        // ways to get 2 - (1,1) , (2)
            memo[3] = 4;        //ways to get 3 - (1,1,1), (2,1), (1,2),(3)

            for (int i = 4; i <= n; i++) {
                bottomUpCount++;
                memo[i] = memo[i - 1] + memo[i - 2] + memo[i - 3];
            }

            return memo[n];
        }

        public static int getAllPossibleWayTopDown(int n, int[] memo) {
            topDownCount++;

            if (n == 0) return 0;
            else if (n == 1) return 1;
            else if (n == 2) return 2;
            else if (n == 3) return 4;

            if (memo[n] == 0)
                memo[n] = getAllPossibleWayTopDown(n - 1, memo) + getAllPossibleWayTopDown(n - 2, memo) + getAllPossibleWayTopDown(n - 3, memo);

            return memo[n];
        }
    }


    //submitted on leetcode
    static class Solution {
        public int climbStairs(int n) {
            return getAllPossibleWayBottomUp(n);
            // return getAllPossibleWayTopDown(n, new int[n+1]);

        }

        private static int getAllPossibleWayBottomUp(int n) {
            if (n < 3) return n;
            int[] memo = new int[n + 1];
            memo[1] = 1;        //ways to get 1 is only 1 through 1
            memo[2] = 2;        // ways to get 2 - (1,1) , (2)

            for (int i = 3; i <= n; i++) {
                memo[i] = memo[i - 1] + memo[i - 2];
            }

            return memo[n];
        }


        public static int getAllPossibleWayTopDown(int n, int[] memo) {
            if (n == 0) return 0;
            else if (n == 1) return 1;
            else if (n == 2) return 2;

            if (memo[n] == 0)
                memo[n] = getAllPossibleWayTopDown(n - 1, memo) + getAllPossibleWayTopDown(n - 2, memo);

            return memo[n];
        }
    }
}
