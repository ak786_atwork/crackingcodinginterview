package season2.recursion_dp;

import java.util.*;

public class PermutationWithDups {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        char[] chars = {'a', 'b', 'b', 'c', 'c'};

//        generatePermutation(0, chars, list);
//        System.out.println(list.size());
//        System.out.println(list.toString());
        String s = "abccc";
        System.out.println(Solution1.getPermutations(s).size());
        System.out.println(Solution1.getPermutations(s).toString());

    }


    public static void generatePermutation(int index, char[] chars, List<String> list) {
        if (index == chars.length) {
            list.add(new String(chars));
        }

        Set<String> set = new HashSet<>();
        String prefix;

        for (int i = index; i < chars.length; i++) {
            swap(chars, index, i);

            prefix = new String(chars, 0, index + 1);
            if (set.contains(prefix)) continue;
            else set.add(prefix);

            generatePermutation(index + 1, chars, list);
            swap(chars, index, i);
        }
    }

    private static void swap(char[] chars, int index, int i) {
        char temp = chars[index];
        chars[index] = chars[i];
        chars[i] = temp;
    }


    //this algo runs fast if it has more number of duplicates i.e if all chars is = aaaaaaaaaaaaaa  this will run only once
    private static class Solution1 {

        public static List<String> getPermutations(String s) {
            HashMap<Character, Integer> map = new HashMap<>();
            map = buildFrequencyTable(s);

            List<String> list = new ArrayList<>();
            generate(list, map, "", s.length());

            return list;
        }

        private static void generate(List<String> list, HashMap<Character, Integer> map, String prefix, int remainingLen) {
            if (remainingLen == 0) {
                list.add(prefix);
                return;
            }

            for (char c : map.keySet()) {
                if (map.get(c) > 0){
                    map.replace(c, map.get(c) - 1);
                    generate(list, map, prefix+c, remainingLen-1);
                    map.replace(c, map.get(c) + 1);
                }
            }

        }

        private static HashMap<Character, Integer> buildFrequencyTable(String s) {
            HashMap<Character, Integer> map = new HashMap<>();
            char c;
            for (int i = 0; i < s.length(); i++) {
                c = s.charAt(i);
                if (map.containsKey(c)) {
                    map.put(c, map.get(c) + 1);
                } else {
                    map.put(c, 1);
                }
            }
            return map;
        }

    }
}
