package season2.recursion_dp;

import java.util.Stack;

public class TowerOfHanoi {

    public static void main(String[] args) {
        int n = 5;
        Tower[] towers = new Tower[n];

        for (int i=0;i<n;i++)
            towers[i] = new Tower(i);

        for (int i=n-1;i>=0;i--){
            towers[0].add(i);
        }

        System.out.println("tower1 = "+ towers[0].toString());
        System.out.println("tower2 = "+ towers[1].toString());
        System.out.println("tower3 = "+ towers[2].toString());

        towers[0].moveDisks(n,towers[2],towers[1]);

        System.out.println("tower1 = "+ towers[0].toString());
        System.out.println("tower2 = "+ towers[1].toString());
        System.out.println("tower3 = "+ towers[2].toString());
    }



    private static class Tower {

        private Stack<Integer> disks;
        private int index;

        public Tower(int i) {
            disks = new Stack<>();
            index = i;
        }

        public int getIndex() {
            return index;
        }

        public void  add(int d) {
            if (!disks.isEmpty() && disks.peek() <= d) {
                System.out.println("ERROR IN PLACING DISKS");
            } else {
                disks.push(d);
            }
        }

        public void moveToTop(Tower tower) {
            tower.add(disks.pop());
        }


        public void moveDisks(int n, Tower destination, Tower buffer) {
            if (n>0) {
                moveDisks(n-1,buffer,destination);
                moveToTop(destination);
                buffer.moveDisks(n-1, destination, this);
            }
        }


        @Override
        public String toString() {
            return disks.toString();
        }
    }
}
