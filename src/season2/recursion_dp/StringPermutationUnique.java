package season2.recursion_dp;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StringPermutationUnique {

    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        char[] chars = {'a','b','c','d'};

        generatePermutation(0, chars,list);

        System.out.println(list.toString());
    }


    public static void  generatePermutation(int index, char[] chars, List<String> list) {
        if (index == chars.length) {
            list.add(new String(chars));
        }

        for (int i = index;i<chars.length;i++) {
            swap(chars, index, i);
            generatePermutation(index+1, chars, list);
            swap(chars, index, i);
        }
    }

    private static void swap(char[] chars, int index, int i) {
        char temp = chars[index];
        chars[index] = chars[i];
        chars[i] = temp;
    }

}
