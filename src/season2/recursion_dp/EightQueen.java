package season2.recursion_dp;

import java.util.ArrayList;
import java.util.HashMap;

public class EightQueen {

    public static void main(String[] args) {
        System.out.println(Solution.getWaysOfPlacingQueen());

        ArrayList<Integer[]> list = new ArrayList<>();
        Solution2.placeQueens(0,new Integer[8], list);
        System.out.println(list.size());
        System.out.println(list.toString());

    }


    //my approach
    private static class Solution {
        private static int BOARD_SIZE = 8;

        public static int getWaysOfPlacingQueen() {
            boolean[] queenPlaced = new boolean[BOARD_SIZE];

            boolean[][] board = new boolean[BOARD_SIZE][BOARD_SIZE];
            HashMap<String, Boolean> queenPosition = new HashMap<>();

            return helper(queenPosition, queenPlaced, 0);

        }

        private static int helper(HashMap<String, Boolean> queenPosition, boolean[] queenPlaced, int colIndex) {
            if (colIndex == BOARD_SIZE) return 1;

            int ways = 0;

            for (int rowIndex = 0; rowIndex < BOARD_SIZE; rowIndex++) {
                if (!queenPlaced[rowIndex] && canPlaceQueen(rowIndex, colIndex, queenPosition)) {
                    queenPlaced[rowIndex] = true;
                    queenPosition.put(rowIndex + "" + colIndex, true);

                    ways += helper(queenPosition, queenPlaced, colIndex + 1);

                    queenPosition.remove(rowIndex + "" + colIndex);
                    queenPlaced[rowIndex] = false;
                }
            }

            return ways;
        }

        private static boolean canPlaceQueen(int rowIndex, int colIndex, HashMap<String, Boolean> queenPosition) {
            //checking diagonal
            int currentRow = rowIndex;
            int currentCol = colIndex;

            //checking first possible diagonal
            while (currentCol >= 0 && currentRow >= 0) {
                if (queenPosition.containsKey(currentRow + "" + currentCol))
                    return false;
                currentCol--;
                currentRow--;
            }

            currentRow = rowIndex;
            currentCol = colIndex;

            //checking first possible diagonal
            while (currentCol >= 0 && currentRow < BOARD_SIZE) {
                if (queenPosition.containsKey(currentRow + "" + currentCol))
                    return false;
                currentCol--;
                currentRow++;
            }

            return true;
        }

    }

    //from ctci
    private static class Solution2 {
        private static int GRID_SIZE = 8;

        public static void placeQueens(int row, Integer[] columns, ArrayList<Integer[]> results) {
            if (row == GRID_SIZE) {
                results.add(columns.clone());
            } else {
                for (int col = 0;col<GRID_SIZE;col++) {
                    if (checkValid(columns,row,col)) {
                        columns[row] = col;
                        placeQueens(row+1,columns,results);
                    }
                }
            }
        }

        private static boolean checkValid(Integer[] columns, int row, int col) {
            for (int row2 = 0;row2<row;row2++) {
                int col2 = columns[row2];

                //check if rows have queen in the same column
                if (col2 == col) {
                    return false;
                }
                //check diagonal
                int columnDistance = Math.abs(col2-col);
                int rowDistance = row - row2;

                if (columnDistance == rowDistance)
                    return false;
            }

            return true;
        }


    }
}
