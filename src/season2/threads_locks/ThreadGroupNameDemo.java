package season2.threads_locks;

public class ThreadGroupNameDemo {
    public static void main(String[] args) {
        System.out.println(Thread.currentThread().getThreadGroup().getName());
        System.out.println(Thread.currentThread().getThreadGroup().getParent().getName());

        showThreadGroupPriorityAffect();

        printActiveThreadGroup();
    }

    private static void printActiveThreadGroup() {
        ThreadGroup system = Thread.currentThread().getThreadGroup().getParent();
        Thread[] threads = new Thread[system.activeCount()];

        system.enumerate(threads);

        for (Thread thread : threads) {
            System.out.println("thread name  = "+thread.getName()+" isDaemon = "+thread.isDaemon());
        }

    }

    public static void showThreadGroupPriorityAffect() {
        ThreadGroup g1 = new ThreadGroup("tg");
        Thread t1 = new Thread(g1, "thread 1");
        Thread t2 = new Thread(g1, "thread 2");

        g1.setMaxPriority(3);       //higher priority thread won't get affected, newly  created thread will inherit that 3

        Thread t3 = new Thread(g1, "thread 3");

        System.out.println(t1.getPriority());
        System.out.println(t2.getPriority());
        System.out.println(t3.getPriority());


    }
}
