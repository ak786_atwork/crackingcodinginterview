package season2.threads_locks;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockDemo3 {
    public static void main(String[] args) {

        MyThread thread = new MyThread("Dhoni");
        thread.start();

        MyThread thread2 = new MyThread("yuvi");
        thread2.start();

    }


    static class MyThread extends Thread {
        String name;

        static ReentrantLock l = new ReentrantLock();

        public MyThread(String name) {
            super(name);
            this.name = name;
        }

        @Override
        public void run() {

            do {
                try {
                    if (l.tryLock(2, TimeUnit.SECONDS)) {
                        System.out.println(Thread.currentThread().getName() + " got the lock and performing operations");
                        Thread.sleep(5000);
                        System.out.println(Thread.currentThread().getName() + " going to release lock ");
                        l.unlock();
                        break;
                    } else {
                        System.out.println(Thread.currentThread().getName() + " didn't get lock and will try again");

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } while (true);

        }
    }
}

