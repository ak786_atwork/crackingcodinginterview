package season2.threads_locks;

public class HybridApproachThreadCreation {

    public static void main(String[] args) {
        MyThread myThread = new MyThread();
        // here MyThread extends Thread and Thread implements Runnable so no issue
        Thread t1 = new Thread(myThread);
        t1.start();

        System.out.println("main thread");
    }


    static class MyThread extends Thread {
        @Override
        public void run() {
            System.out.println("child thread");
        }
    }
 }
