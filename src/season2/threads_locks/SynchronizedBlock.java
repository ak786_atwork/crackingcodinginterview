package season2.threads_locks;

public class SynchronizedBlock {
    public static void main(String[] args) {
        Display display = new Display();

        //here thread or thread1 can get the chance it depends on jvm
        MyThread thread = new MyThread(display, "Dhoni");
        thread.start();

        MyThread thread2 = new MyThread(new Display(), "yuvi");
        thread2.start();

    }


    static class MyThread extends Thread {
        Display display;
        String name;

        public MyThread(Display display, String name) {
            this.display = display;
            this.name = name;
        }

        @Override
        public void run() {
            display.wish(name);
        }
    }

    /**
     * Synchronization is needed only when same java object is being modified by multiple threads
     * 1. class level locks - ( e.g. to execute static synchronize methods )
     * 2. object level locks
     */

    static class Display {

        //class level lock and object level lock
        //lock is not applicable for primitive values eg int
        public void wish(String name) {
            ///1 lakh lines of code
            int i = 0;
            synchronized (Display.class) {
                while (i++ < 10) {
                    System.out.print("good morning: ");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(name);
                }
            }

            //1 lakh lines of code
        }
    }
}

