package season2.threads_locks;

public class WaitDemo {

    public static void main(String[] args) throws InterruptedException {
        MyThread thread = new MyThread();
        thread.start();

        synchronized (thread) {
            System.out.println("Main trying to call wait");
            thread.wait();
            System.out.println("result = " + thread.total);
        }

   /*  //main thread stuck if suppose main sleeps meanwhile child notifies it will stuck for forever
        Thread.sleep(5000);
        synchronized (thread) {
            System.out.println("Main trying to call wait");
            thread.wait();          //u can resolve this issue by using time waited time
            System.out.println("result = " + thread.total);
        }*/
    }


    static class MyThread extends Thread {
        int total = 0;

        @Override
        public void run() {

            synchronized (this) {
                System.out.println("child thread starts calculation");
                for (int i = 1; i <= 100; i++) {
                    total += i;
                }
                System.out.println("child trying to notify");
                this.notify();
                System.out.println("child called notify");
            }
        }
    }
}
