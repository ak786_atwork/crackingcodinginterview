package season2.threads_locks;

public class ThreadLocalWithInheritanceDemo {


    public static void main(String[] args) {
        ParentThread parentThread = new ParentThread();
        parentThread.start();
    }




    static class ParentThread extends Thread {

        //By default values set to threadlocal by parent thread is not available to child class if you want it to be available to child class use
        //InheritableThreadLocal
        static InheritableThreadLocal tl = new InheritableThreadLocal() {
            @Override
            protected Object childValue(Object parentValue) {
                //use it to give different value to child
                return "i am child";
            }
        };

        @Override
        public void run() {
            tl.set("parent");

            //create child thread
            ChildThread childThread = new ChildThread();
            childThread.start();

        }
    }


    static class ChildThread extends Thread {
        @Override
        public void run() {
            System.out.println("trying to access values set by parent thread  = "+ParentThread.tl.get());
        }
    }
}
