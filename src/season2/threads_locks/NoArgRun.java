package season2.threads_locks;

public class NoArgRun {

    public static void main(String[] args) {
        NoArgMethodExample thread = new NoArgMethodExample();
        thread.start();
    }


    static class NoArgMethodExample extends Thread {
        @Override
        public void run() {
            System.out.println("no -arg");
        }

        public void run(int i) {
            System.out.println("arg method");
        }
    }
}
