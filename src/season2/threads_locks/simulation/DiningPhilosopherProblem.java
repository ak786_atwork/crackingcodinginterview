package season2.threads_locks.simulation;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class DiningPhilosopherProblem {
    public static void main(String[] args) {
        /*DiningPhilosopherSolution2.ChopStick chopStick1 = new DiningPhilosopherSolution2.ChopStick();
        DiningPhilosopherSolution2.ChopStick chopStick2 = new DiningPhilosopherSolution2.ChopStick();
        DiningPhilosopherSolution2.ChopStick chopStick3 = new DiningPhilosopherSolution2.ChopStick();*/

        DiningPhilosopherSolution3.ChopStick chopStick1 = new DiningPhilosopherSolution3.ChopStick(0);
        DiningPhilosopherSolution3.ChopStick chopStick2 = new DiningPhilosopherSolution3.ChopStick(1);
        DiningPhilosopherSolution3.ChopStick chopStick3 = new DiningPhilosopherSolution3.ChopStick(2);

        DiningPhilosopherSolution3.Philosopher philosopher1 = new DiningPhilosopherSolution3.Philosopher(chopStick1, chopStick3);
        DiningPhilosopherSolution3.Philosopher philosopher2 = new DiningPhilosopherSolution3.Philosopher(chopStick2, chopStick1);
        DiningPhilosopherSolution3.Philosopher philosopher3 = new DiningPhilosopherSolution3.Philosopher(chopStick3, chopStick2);

        philosopher1.start();
        philosopher2.start();
        philosopher3.start();
    }
}

/**
 * this is deadlock free solution in every case.
 *
 * prioritized chopsticks - i.e now chopsticks are labeled from 0 - n. Everyone is supposed to pick left i.e lowest numbered chopstick first.
 * but for the last i.e lowest will be on right so breaking the cycle - no deadlock
 *
 * */
class DiningPhilosopherSolution3 {

    static class ChopStick {
        private Lock lock;
        private int number;

        public ChopStick(int number) {
            this.number = number;
            this.lock = new ReentrantLock();
        }

        public void pickUp() {
            lock.lock();
        }

        public void putDown() {
            lock.unlock();
        }

        public int getNumber() {
            return number;
        }

    }


    static class Philosopher extends Thread {
        private int bites = 10;
        private ChopStick lower, higher;

        public Philosopher(ChopStick left, ChopStick right) {
            if (left.getNumber() < right.getNumber()) {
                lower = left;
                higher = right;
            } else {
                lower = right;
                higher = left;
            }
        }

        public void eat() {
            pickUp();
            chew();
            putDown();
        }

        private void putDown() {
            lower.putDown();
            higher.putDown();
        }

        private void chew() {
            System.out.println(Thread.currentThread().getName() + "eating");
        }

        private void pickUp() {
            lower.pickUp();
            higher.pickUp();
        }

        public void run() {
            for (int i = 0; i < bites; i++) {
                eat();
            }
        }

    }

}

/**
 * Every philosopher will try to pick left and then right if he if able to pick up both then only he will start eating, otherwise will drop
 * held chopstick.
 *
 * this will stuck into a problem where all first get the left and they won't find right chopstick, so they will drop all, again if same
 * scenario repeats then they will stuck into deadlock.
 * */
class DiningPhilosopherSolution2 {

    static class ChopStick {
        private Lock lock;

        public ChopStick() {
            this.lock = new ReentrantLock();
        }

        public boolean pickUp() {
            return lock.tryLock();
        }

        public void putDown() {
            lock.unlock();
        }

    }

    static class Philosopher extends Thread {
        private int bites = 10;
        private ChopStick left, right;

        public Philosopher(ChopStick left, ChopStick right) {
            this.left = left;
            this.right = right;
        }

        public void eat() {
            if (pickUp()) {
                chew();
                putDown();
            }
        }

        private void putDown() {
            left.putDown();
            right.putDown();
        }

        private void chew() {
            System.out.println(Thread.currentThread().getName() + "eating");
        }

        private boolean pickUp() {
            if (!left.pickUp())
                return false;

            if (!right.pickUp()) {
                left.putDown();
                return false;
            }

            return true;
        }

        public void run() {
            for (int i = 0; i < bites; i++) {
                eat();
            }
        }

    }

}


/**
 * this solution will stuck into deadlock when all philosopher have left chopstick and all will remain in waiting for right one
 */
class DiningPhilosopherSolution1 {

    static class ChopStick {
        private Lock lock;

        public ChopStick() {
            this.lock = new ReentrantLock();
        }

        public void pickUp() {
            lock.lock();
        }

        public void putDown() {
            lock.unlock();
        }

    }


    static class Philosopher extends Thread {
        private int bites = 10;
        private ChopStick left, right;

        public Philosopher(ChopStick left, ChopStick right) {
            this.left = left;
            this.right = right;
        }

        public void eat() {
            pickUp();
            chew();
            putDown();
        }

        private void putDown() {
            left.putDown();
            right.putDown();
        }

        private void chew() {
            System.out.println(Thread.currentThread().getName() + "eating");
        }

        private void pickUp() {
            left.pickUp();
            right.pickUp();
        }

        public void run() {
            for (int i = 0; i < bites; i++) {
                eat();
            }
        }

    }

}
