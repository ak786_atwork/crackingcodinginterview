package season2.threads_locks;

public class DaemonThreadDemo {

    public static void main(String[] args) {


        System.out.println(Thread.currentThread().isDaemon());
//        Thread.currentThread().setDaemon(true);               //throws java.lang.IllegalThreadStateException

        Thread thread = new Thread();
        System.out.println(thread.isDaemon());
        System.out.println(thread.getPriority());
        thread.setDaemon(true);
        System.out.println(thread.isDaemon());

        //case 2
      /*  MyThread thread = new MyThread();
        thread.setDaemon(true);         //now when main terminated this will also get terminated
        thread.start();

        System.out.println("end of main");*/

    }


    static class MyThread extends Thread {
        @Override
        public void run() {
            int i = 0;
            while (i++ < 10) {
                System.out.println("child thread");
            }
        }
    }
}
