package season2.threads_locks;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolDemo {

    public static void main(String[] args) {
        PrintJob[] printJobs = {
                new PrintJob("ak"),
                new PrintJob("akky"),
                new PrintJob("nkk")
        };

        ExecutorService executorService = Executors.newFixedThreadPool(2);

        for (PrintJob job : printJobs) {
            executorService.submit(job);
        }

        executorService.shutdown();

    }


    static class PrintJob implements Runnable {
        String name;

        public PrintJob(String name) {
            this.name = name;
        }



        @Override
        public void run() {
            System.out.println("job started by " + Thread.currentThread().getName());
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("job completed by " + Thread.currentThread().getName());
        }
    }
}
