package season2.threads_locks;


public class OverrideStartExample {

    public static void main(String[] args) {
        OverrideStart thread = new OverrideStart();
        thread.start();

        System.out.println("main ");
    }


    static class OverrideStart extends Thread {
        public void start() {
            ////no thread will be created
//            super.start();
            System.out.println("start ");
            System.out.println("name " +Thread.currentThread().getName());
        }

        public void run() {
            System.out.println("run ");
            System.out.println("name " +Thread.currentThread().getName());

        }
    }
}
