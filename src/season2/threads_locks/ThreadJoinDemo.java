package season2.threads_locks;

public class ThreadJoinDemo {

    public static void main(String[] args) {

        MyThread.mt = Thread.currentThread();

        //calling join on same thread, program will get stuck
        /*try {
            Thread.currentThread().join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        MyThread myThread = new MyThread();
        myThread.start();

        try {
            myThread.join();
//            myThread.join(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        int i = 0;
        while (i++ < 10) {
            System.out.println("main thread");
        }
    }




    private static class MyThread extends Thread {

        static Thread mt;
        @Override
        public void run() {
            int i = 0;

            //if both threads call join method over each other then program will get stuck
            /*try {
                mt.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            while (i++ < 10) {
                System.out.println("child thread");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
