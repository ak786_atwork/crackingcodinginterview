package season2.threads_locks;

public class YieldDemo {

    public static void main(String[] args) {


        MyThread thread = new MyThread();
        thread.start();


        int i = 0;
        while (i++ < 10) {
            System.out.println("main thread");
        }
    }




    static class MyThread extends Thread {
        @Override
        public void run() {
            int i = 0;
            while (i++ < 10) {
                System.out.println("child thread");
                    Thread.yield();
            }
        }
    }
}
