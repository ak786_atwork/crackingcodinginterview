package season2.threads_locks;

public class RunnableThreadExample {

    public static void main(String[] args) {
        System.out.println("name = "+Thread.currentThread().getName());

        RunnableThread runnableThread = new RunnableThread();
        (new Thread(runnableThread)).start();       //create a new thread
//        runnableThread.run();   // won't create a thread

        MyThread myThread = new MyThread();
        myThread.start();

        System.out.println("main thread quits");

    }

    static class RunnableThread implements Runnable {

        @Override
        public void run() {
            System.out.println("name = "+Thread.currentThread().getName());
        }
    }

    static class MyThread extends Thread {
        @Override
        public void run() {
            try {
                Thread.sleep(2100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("name = "+Thread.currentThread().getName());
        }
    }
}


