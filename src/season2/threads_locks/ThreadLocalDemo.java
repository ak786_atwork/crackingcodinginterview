package season2.threads_locks;

public class ThreadLocalDemo {

    public static void main(String[] args) {

        //overriding initial value method ,, by default it returns null
        ThreadLocal threadLocal = new ThreadLocal(){
            @Override
            protected Object initialValue() {
                return "abc";
            }
        };

        System.out.println(threadLocal.get());
        threadLocal.set("anil");
        System.out.println(threadLocal.get());
        threadLocal.remove();
        System.out.println(threadLocal.get());




    }


}
