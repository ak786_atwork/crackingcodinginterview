package season2.threads_locks;

import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockDemo2 {
    public static void main(String[] args) {

        MyThread thread = new MyThread( "Dhoni");
        thread.start();

        MyThread thread2 = new MyThread( "yuvi");
        thread2.start();

    }


    static class MyThread extends Thread {
        String name;

        static ReentrantLock l = new ReentrantLock();

        public MyThread(String name) {
            super(name);
            this.name = name;
        }

        @Override
        public void run() {
            if (l.tryLock()) {
                System.out.println(Thread.currentThread().getName() +" got lock and performing operations");

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                l.unlock();
            } else {
                System.out.println(Thread.currentThread().getName() +" haven't lock");
            }
        }
    }
}
