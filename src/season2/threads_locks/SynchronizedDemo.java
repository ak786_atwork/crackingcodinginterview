package season2.threads_locks;

public class SynchronizedDemo {

    public static void main(String[] args) {
        Display display = new Display();
        Display display2 = new Display();

        //here thread or thread1 can get the chance it depends on jvm
        MyThread thread = new MyThread(display, "Dhoni");
        thread.start();

        MyThread thread2 = new MyThread(display2, "yuvi");
        thread2.start();

    }



    static class MyThread extends Thread {
        Display display;
        String name;

        public MyThread(Display display, String name) {
            this.display = display;
            this.name = name;
        }

        @Override
        public void run() {
           display.wish(name);
        }
    }

    /**
     * Synchronization is needed only when same java object is being modified by multiple threads
     * 1. class level locks - ( e.g. to execute static synchronize methods )
     * 2. object level locks
    * */

    static class Display {

        //if this method is synchronized only one thread is allowed to execute whether this thread sleep or not
        // if static is added, then output will be regular if we use different display object as thread will be holding class level lock
        public static synchronized void wish(String name) {
            int i = 0;
            while (i++ < 10) {
                System.out.print("good morning: ");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(name);
            }
        }
    }
}
