package season2.threads_locks;

import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockDemo {


    public static void main(String[] args) {
        Display display = new Display();

        MyThread thread = new MyThread(display, "Dhoni");
        thread.start();

        MyThread thread2 = new MyThread(display, "yuvi");
        thread2.start();

    }


    static class MyThread extends Thread {
        Display display;
        String name;

        public MyThread(Display display, String name) {
            this.display = display;
            this.name = name;
        }

        @Override
        public void run() {
            display.wish(name);
        }
    }

    /**
     * Synchronization is needed only when same java object is being modified by multiple threads
     * 1. class level locks - ( e.g. to execute static synchronize methods )
     * 2. object level locks
     */

    static class Display {

        ReentrantLock l = new ReentrantLock();

        public void wish(String name) {
            int i = 0;
            l.lock();
            while (i++ < 10) {
                System.out.print("good morning: ");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(name);
            }
            l.unlock();
        }
    }
}

