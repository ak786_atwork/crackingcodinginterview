package season2.threads_locks;

import java.util.concurrent.*;

public class CallableFutureDemo {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        MyCallable[] myCallables = {
                new MyCallable(10),
                new MyCallable(20),
                new MyCallable(30),
                new MyCallable(40)
        };

        ExecutorService service = Executors.newFixedThreadPool(2);

        for (MyCallable job : myCallables) {
            Future f = service.submit(job);
            System.out.println(f.get());
        }

        service.shutdown();
    }






    static class MyCallable implements Callable {

        public MyCallable(int num) {
            this.num = num;
        }

        int num;


        @Override
        public Object call() throws Exception {
            System.out.println(Thread.currentThread().getName() + " responsible to find sum of "+num + " numbers");
            int total = 0;
            for (int i = 1;i<=num;i++) {
                total += i;
            }

            return total;
        }
    }
}
