package season2.threads_locks;

public class SynchronizedExample {

    public static void main(String[] args) {
        Thread t1 = new Thread();

    }


    static class SynchronizedMethodExample {
        public synchronized void foo(String name) {
            System.out.println(name + " called");
        }
    }
}
