package season2.threads_locks;

public class ThreadInterruptedDemo {

    public static void main(String[] args) {

        MyThread thread = new MyThread();
        thread.start();

        AnotherThread anotherThread = new AnotherThread();
        anotherThread.start();


        int i = 0;
        while (i++ < 5) {
            System.out.println("main thread");
        }
        System.out.println("going to interrupt child");

        anotherThread.interrupt();

        thread.interrupt();
        System.out.println("main exiting");

    }



    static class MyThread extends Thread {
        @Override
        public void run() {
            try {
                int i = 0;
                while (i++ < 10) {
                    System.out.println("i am lazy thread");
                    Thread.sleep(100);
                }
            } catch (InterruptedException e) {
                System.out.println("got interrupted");
            }

            System.out.println("name = "+Thread.currentThread().getName());
        }
    }

    static class AnotherThread extends Thread {
        @Override
        public void run() {
            int i = 0;
            while (i++ < 10) {
                System.out.println("another thread - i am lazy thread");
            }

            System.out.println("going to sleep");

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                System.out.println("got interrupted");
            }
        }
    }
}
