package season2.threads_locks;

import c13_java.SuperClass;

public class DeadLockDemo extends Thread {

    A a = new A();
    B b = new B();

    public static void main(String[] args) {
        DeadLockDemo deadLockDemo = new DeadLockDemo();
        deadLockDemo.m1();
    }

    private void m1() {
        this.start();
        a.d1(b);

    }

    @Override
    public void run() {
        b.d1(a);
    }

    static class A {
        public synchronized void d1(B b) {
            System.out.println("Thread 1 starts excution of d1 method");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread 1 trying to call B's last method");
            b.last();
        }

        public synchronized void last() {
            System.out.println("inside A");
        }
    }

    static class B {
        public synchronized void d1(A a) {
            System.out.println("Thread 2 starts excution of d1 method");
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread 2 trying to call A's last method");
            a.last();
        }

        public synchronized void last() {
            System.out.println("inside B");
        }
    }

}
