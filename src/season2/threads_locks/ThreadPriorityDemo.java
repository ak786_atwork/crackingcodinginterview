package season2.threads_locks;

public class ThreadPriorityDemo {

    public static void main(String[] args) {
        MyThread thread = new MyThread();
        thread.setPriority(Thread.MAX_PRIORITY);        //even though we won't get first all child and then main as o/p because it depends on underlying system if they support or not
        thread.start();     //any order execution - as both have the same priority and it depends on ThreadScheduler algo

        int i = 0;
        while (i++ < 10) {
            System.out.println("main thread");
        }

    }


    static class MyThread extends Thread {
        @Override
        public void run() {
            int i = 0;
            while (i++ < 10) {
                System.out.println("child thread");
            }
        }
    }
}
