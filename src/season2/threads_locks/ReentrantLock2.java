package season2.threads_locks;

import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLock2 {

    public static void main(String[] args) {
        ReentrantLock lock = new ReentrantLock();
        lock.lock();
        lock.lock();

        System.out.println(lock.isLocked());
        System.out.println(lock.isHeldByCurrentThread());
        System.out.println(lock.getQueueLength());

        lock.unlock();

        System.out.println(lock.getHoldCount());
        System.out.println(lock.isLocked());

        lock.unlock();
        System.out.println(lock.isLocked());

    }

}
