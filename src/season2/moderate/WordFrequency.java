package season2.moderate;

import java.util.HashMap;

public class WordFrequency {

    public static void main(String[] args) {
        String[] words = {"abc","Abc","fdshfkj","fdsjf","fkjdslf"};
        String word = "abc";

        System.out.println(Solution1.getFrequency(words, word));
        System.out.println(Solution2.getFrequency(words, word));

    }


    private static class Solution1 {

        public static int getFrequency(String[] words, String word) {
            int count = 0;
            for (String w : words) {
                if (word.trim().toLowerCase().equals(w.trim().toLowerCase()))
                    count++;
            }

            return count;
        }
    }

    private static class Solution2 {
        static HashMap<String, Integer> map;

        public static int getFrequency(String[] words, String word) {
            if (map == null)
                setupDictionary(words);

            return map.get(word.trim().toLowerCase());
        }

        private static HashMap<String, Integer> setupDictionary(String[] words) {
            map = new HashMap<>();
            String temp;
            for (String w : words) {
                temp = w.trim().toLowerCase();
                if (map.containsKey(temp)) {
                    map.put(temp, map.get(temp) + 1);
                } else {
                    map.put(temp, 1);
                }
            }

            return map;
        }

    }

}
