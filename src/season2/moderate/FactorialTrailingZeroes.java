package season2.moderate;

public class FactorialTrailingZeroes {

    public static void main(String[] args) {
        System.out.println(trailingZeroes(1808548329));
        System.out.println(trailingZeros2(1808548329));
    }

    public static int trailingZeroes(int n) {
        if (n < 5) return 0;

        int trailingZeros = 0;

        for (int i = 5; i <= n; i += 5) {
            trailingZeros += getMultiples(i);
        }

        return trailingZeros;
    }

    private static int getMultiples(int i) {
        int multiples = 0;
        while (i != 0) {
            if (i % 5 != 0)
                break;

            multiples++;
            i /= 5;
        }
        return multiples;
    }

    //not covering all solutions
    public static int trailingZeroes1(int n) {
        if (n < 5) return 0;

        int trailingZeros = n / 5;

        int squares = 0;
        int squareMultiples = 1;

        for (int i = 5 * 5; i <= n; i *= i) {
            squares++;
            squareMultiples += squareMultiples;
        }

        return trailingZeros + squareMultiples / 2;
    }

    //submitted to leetcode
    public static int trailingZeros2(int n) {
        int count = 0;

        while (n>=5) {
            count += n/5;
            n /= 5;
        }

        //this snippet has integer overflow issue at 1808548329
       /* for (int i = 5; (n / i) > 0; i = i * 5) {
            System.out.println(Integer.MAX_VALUE + " ans = "+i);
            count += (n / i);
        }*/

        return count;
    }


}
