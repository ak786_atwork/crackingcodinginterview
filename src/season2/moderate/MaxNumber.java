package season2.moderate;

public class MaxNumber {

    public static void main(String[] args) {
        System.out.println(getMax2(Integer.MAX_VALUE,Integer.MIN_VALUE));
        System.out.println(getMax2(90,Integer.MAX_VALUE));
    }


    //this code is not able to handle integer overflow problem eg a = Integer.Max -2 b = -14
    public static int getMax(int a, int b) {
        //if a > b  then c > 0 means sign bit is 0, but we want flipped bit
        int c = a - b;
        int signBit = signBit(c);
        int flipBit = signBit ^ 1;

        return a * flipBit + b * signBit;
    }

    private static int signBit(int c) {
        return (c >> 31) & 1;
    }


    //above solution has integer overflow problem when both numbers are of different sign
    public static int getMax1(int a, int b) {

        int c = a - b;
        int signA = signBit(a);
        int signB = signBit(b);
        int signC = signBit(c);

        //if both signA and signB are same use signC else use signA

        int signBitEqualOrNot = (signA ^ signB);
        int resultBit = (signBitEqualOrNot & signA) ^ ((signBitEqualOrNot ^ 1) & signC);
        int flipBit = resultBit ^ 1;

        return a * flipBit + b * resultBit;
    }

    //from ctci
    public static int getMax2(int a, int b) {

        int c = a - b;
        int signA = signBit(a);
        int signB = signBit(b);
        int signC = signBit(c);

        int use_sign_of_a = (signA ^ signB);
        int use_sign_of_c = (signA ^ signB) ^ 1;
        int k = use_sign_of_a * signA + use_sign_of_c * signC;
        int q = k ^ 1; //flip bit

        return a * q + b * k;
    }



}
