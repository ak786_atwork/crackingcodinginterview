package season2.moderate;

public class NumberSwapper {

    public static void main(String[] args) {
        swapNumber(4,9);
        swapNumber(-4,9);
        swapNumber(Integer.MIN_VALUE,Integer.MIN_VALUE);
    }


    public static void swapNumber1(int a, int b) {
        System.out.println("Before swap a = "+a+" b = "+b);

        a = a+b;
        b = a-b;
        a = a-b;

        System.out.println("After swap a = "+a+" b = "+b);
    }

    //from ctci it will work for other data types also
    public static void swapNumber(int a, int b) {
        System.out.println("Before swap a = "+a+" b = "+b);

        a = a^b;
        b = a^b;
        a = a^b;

        System.out.println("After swap a = "+a+" b = "+b);
    }
}
