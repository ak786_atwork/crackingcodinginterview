package season2.moderate;

import java.util.HashMap;

public class EnglishInt {

    public static void main(String[] args) {
        int[] nums = {1, 12, 123, 1234, 1000, 12345, 123456, 1234567, 12345678, 123456789, 1234567891};
        printTest(nums);
    }

    private static void printTest(int[] nums) {
        for (int number : nums) {
            System.out.println(number + " = " + (new Solution()).numberToWords(number));
        }
    }

    //submitted
    private static class Solution {
        private static class Constants {
            static int ZERO = 0;
            static int TEN = 10;
            static int HUNDRED = 100;
            static int THOUSAND = 1000;
            static int MILLION = THOUSAND * THOUSAND;
            static int BILLION = THOUSAND * MILLION;

            static HashMap<Integer, String> map = new HashMap<Integer, String>() {{
                put(0, "Zero");
                put(1, "One");
                put(2, "Two");
                put(3, "Three");
                put(4, "Four");
                put(5, "Five");
                put(6, "Six");
                put(7, "Seven");
                put(8, "Eight");
                put(9, "Nine");

                put(11, "Eleven");
                put(12, "Twelve");
                put(13, "Thirteen");
                put(14, "Fourteen");
                put(15, "Fifteen");
                put(16, "Sixteen");
                put(17, "Seventeen");
                put(18, "Eighteen");
                put(19, "Nineteen");

                put(10, "Ten");
                put(20, "Twenty");
                put(30, "Thirty");
                put(40, "Forty");
                put(50, "Fifty");
                put(60, "Sixty");
                put(70, "Seventy");
                put(80, "Eighty");
                put(90, "Ninety");

                put(HUNDRED, "Hundred");
                put(THOUSAND, "Thousand");
                put(MILLION, "Million");
                put(BILLION, "Billion");

            }};

        }

        public String numberToWords(int number) {
            if (number == 0) return Constants.map.get(Constants.ZERO);

            return getWord(number);
        }

        public String getWord(int number) {
            StringBuilder sb = new StringBuilder();
            int temp = 0;
            int key = 0;

            if (number < Constants.TEN) {
                if(number != 0)
                    sb.append(Constants.map.get(number));
            } else if (number < Constants.HUNDRED) {
                if (number < 20) {
                    sb.append(Constants.map.get(number));
                } else {
                    temp = number / Constants.TEN;
                    key = temp * Constants.TEN;
                    number = number % Constants.TEN;
                    sb.append(" " + Constants.map.get(key) + " " + getWord(number) + " ");
                }

            } else if (number < Constants.THOUSAND) {
                temp = number / Constants.HUNDRED;
                key = temp * Constants.HUNDRED;
                number = number % Constants.HUNDRED;
                sb.append(" " + Constants.map.get(temp)).append(" ").append(Constants.map.get(Constants.HUNDRED)).append(" ").append(getWord(number) + " ");

            } else if (number < Constants.MILLION) {
                temp = number / Constants.THOUSAND;
                key = temp * Constants.THOUSAND;
                number = number % Constants.THOUSAND;
                sb.append(" " + getWord(temp) + " ").append(Constants.map.get(Constants.THOUSAND)).append(" ").append(getWord(number) + " ");

            } else if (number < Constants.BILLION) {
                temp = number / Constants.MILLION;
                key = temp * Constants.MILLION;
                number = number % Constants.MILLION;
                sb.append(" " + getWord(temp) + " ").append(Constants.map.get(Constants.MILLION)).append(" ").append(getWord(number) + " ");

            } else {
                temp = number / Constants.BILLION;
                key = temp * Constants.BILLION;
                number = number % Constants.BILLION;
                sb.append(" " + getWord(temp) + " ").append(Constants.map.get(Constants.BILLION)).append(" ").append(getWord(number) + " ");
            }

            return sb.toString().trim();
        }
    }

    //best solution from leetcode
    private static class Solution1 {
        public String numberToWords(int num) {
            if ( num == 0) return "Zero";
            int divisor = 1000000000;
            int place = 4, quotient = 0;
            StringBuilder sb = new StringBuilder();
            while( place > 0){
                quotient = num / divisor;
                if( quotient > 0){
                    getHundredsWords(quotient, sb);
                    placeWord(place, sb);
                }
                num = num % divisor;
                divisor /=1000;
                place--;
            }

            return sb.toString().trim();
        }

        public void placeWord(int place, StringBuilder sb){
            switch( place) {
                case 4:
                    sb.append("Billion ");
                    break;
                case 3:
                    sb.append("Million ");
                    break;
                case 2:
                    sb.append("Thousand ");
                    break;
                default:

                    break;
            }

        }

        public void getHundredsWords( int num, StringBuilder sb ){
            int place = 3, quotient = 0;
            int divisor = 100;
            while ( num > 0 ){
                if( place == 2 && num >= 10 && num < 20){
                    divisor /=10;
                }
                quotient = num/divisor;
                num = num%divisor;
                if ( quotient > 0){
                    getWordForNumber(quotient, place, sb);
                }
                divisor /=10;
                place--;
            }
        }

        public void getWordForNumber(int num, int place, StringBuilder sb){

            if( place != 2){
                switch(num){
                    case 1:
                        sb.append("One ");
                        break;
                    case 2:
                        sb.append("Two ");
                        break;
                    case 3:
                        sb.append("Three ");
                        break;
                    case 4:
                        sb.append("Four ");
                        break;
                    case 5:
                        sb.append("Five ");
                        break;
                    case 6:
                        sb.append("Six ");
                        break;
                    case 7:
                        sb.append("Seven ");
                        break;
                    case 8:
                        sb.append("Eight ");
                        break;
                    case 9:
                        sb.append("Nine ");
                        break;
                    default:
                        break;
                }
            }
            else{
                switch(num){
                    case 10:
                        sb.append("Ten ");
                        break;
                    case 11:
                        sb.append("Eleven ");
                        break;
                    case 12:
                        sb.append("Twelve ");
                        break;
                    case 13:
                        sb.append("Thirteen ");
                        break;
                    case 14:
                        sb.append("Fourteen ");
                        break;
                    case 15:
                        sb.append("Fifteen ");
                        break;
                    case 16:
                        sb.append("Sixteen ");
                        break;
                    case 17:
                        sb.append("Seventeen ");
                        break;
                    case 18:
                        sb.append("Eighteen ");
                        break;
                    case 19:
                        sb.append("Nineteen ");
                        break;
                    case 2:
                        sb.append("Twenty ");
                        break;
                    case 3:
                        sb.append("Thirty ");
                        break;
                    case 4:
                        sb.append("Forty ");
                        break;
                    case 5:
                        sb.append("Fifty ");
                        break;
                    case 6:
                        sb.append("Sixty ");
                        break;
                    case 7:
                        sb.append("Seventy ");
                        break;
                    case 8:
                        sb.append("Eighty ");
                        break;
                    case 9:
                        sb.append("Ninety ");
                        break;
                    default:
                        break;
                }
            }
            if( place == 3 && num > 0){
                sb.append("Hundred ");
            }
        }


    }
}
