package season2.moderate;


import java.util.Scanner;

public class SegmentIntersection {




/*


    private static class LineSegment {
        Point p1;
        Point p2;

        public LineSegment(Point p1, Point p2) {
            this.p1 = p1;
            this.p2 = p2;
        }

        public boolean isParallel(LineSegment lineSegment) {
            //check parallel to x axis and y axis
//            if (lineSegment)
        }


    }

    private static class LineEquation {
        //equation of the form ax+by+c = 0
        int a;
        int b;
        int c;

        public LineEquation(Point p1, Point p2) {
            a = p2.x - p1.x;    //a = x2 - x1
            b = p1.y - p2.y;    // b = y1 - y2
            c = p2.y * p1.x - p1.y * p2.x;      // c =y2.x1 - y1.x2
        }

        public boolean isParallel(LineEquation equation) {
            //not considering a = 0, b=0 case as in this case equation doesn't exist

            boolean parallel = false;
            if (a != 0 && b != 0) {
                if ((equation.a/a) == (equation.b/b))
                    parallel = true;
            } else if (a == 0 && equation.a == 0) {
                //parallel to x-axis
                parallel = true;
            } else if (b == 0 && equation.b == 0){
                parallel = true;    //parallel to y axis
            }

            return parallel;
        }

        public boolean isCollinear(LineEquation equation) {
            if (isParallel(equation)) {
                if ((a * equation.c == c * equation.a)) {
                    return true;
                }
            }
            return false;
        }

        public Point findIntersection(LineEquation equation) {
            if ()
        }


    }

    private static class Point {
        int x;
        int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }*/


    //from ctci
    private static class Solution {

        /* Rearranging these so that, in order of x values: start is before end and point1 is before point2. This will make some logic simpler*/
        public Point intersection(Point start1, Point end1, Point start2, Point end2) {
            if (start1.x > end1.x) swap(start1, end1);
            if (start2.x > end2.x) swap(start2, end2);

            if (start1.x > start2.x) {
                swap(start1, start2);
                swap(end1, end2);
            }

            /* compute lines (including slope and y intercept */
            Line line1 = new Line(start1, end1);
            Line line2 = new Line(start2, end2);

            /*if the lines are parallel, they intercept only if they same y intercept and start 2 is on line 1 */

            if (line1.slope == line2.slope) {
                if (line1.yintercept == line2.yintercept && isBetween(start1,start2,end1)) {
                    return start2;
                }
                return null;
            }

            /*get intersection co-ordinate -- using slope intercept form y = mx + c     */
            double x = (line2.yintercept - line1.yintercept) == 0 ? 0 : (line2.yintercept - line1.yintercept) / (line1.slope - line2.slope);
            double y = x * line1.slope + line1.yintercept;
            System.out.println("x = "+x+" y = "+y);
            Point intersection = new Point(x,y);

            /*checks if within line segment range*/
            if (isBetween(start1, intersection, end1) && isBetween(start2, intersection, end2))
                return intersection;

            return null;
        }

        private void swap(Point p1, Point p2) {
            double x = p1.x;
            double y = p1.y;

            p1.setLocation(p2.x, p2.y);
            p2.setLocation(x,y);

        }


        /*checks if middle is between start and end */
        public boolean isBetween(Point start, Point middle, Point end) {
            return isBetween(start.x, middle.x, end.x) && isBetween(start.y, middle.y, end.y);
        }

        private boolean isBetween(double start, double middle, double end) {
            if (start > end) {
                return end <= middle && middle <= start;
            } else {
                return start <= middle && middle <= end;
            }
        }


    }

    private static class Line {
        public double slope, yintercept;

        public Line(Point start, Point end) {
            double deltaY = end.y - start.y;
            double deltaX = end.x - start.x;

            /* 0.0/0.0 = nan and 1.0/0.0 = infinity, so avoid having nan, you can't compare anything with nan */
            slope = deltaY == 0 ? 0 : deltaY/deltaX;
            yintercept = start.y - slope * start.x;

        }
    }

    private static class Point {
        public double x;
        public double y;

        public Point(double x, double y) {
            this.x = x;
            this.y = y;
        }

        public void setLocation(double x, double y) {
            this.x = x;
            this.y = y;
        }


    }


/* x = NaN y = NaN */
    private static class GeeksSolution {
        public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);
//            int test = sc.nextInt();
            int x1, x2,y1,y2;
            Point start1, end1, start2, end2, intersection;

            Solution solution = new Solution();
            start1 = new Point(10,0);
            end1 = new Point(0,10);
            start2 = new Point(0,10);
            end2 = new Point(0,0);
            intersection = solution.intersection(start1,end1,start2,end2);


            /*while (test-- > 0) {
                x1 = sc.nextInt();
                y1 = sc.nextInt();
                x2 = sc.nextInt();
                y2 = sc.nextInt();

                start1 = new Point(x1,y1);
                end1 = new Point(x2,y2);

                x1 = sc.nextInt();
                y1 = sc.nextInt();
                x2 = sc.nextInt();
                y2 = sc.nextInt();

                start2 = new Point(x1,y1);
                end2 = new Point(x2,y2);

                intersection = solution.intersection(start1,end1,start2,end2);

                System.out.println((intersection == null ? 0 : 1));
            }*/

        }
    }

}

//working
// Java program to check if two given line segments intersect
class GFG
{

    static class Point
    {
        int x;
        int y;

        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

    };

    // Given three colinear points p, q, r, the function checks if
// point q lies on line segment 'pr'
    static boolean onSegment(Point p, Point q, Point r)
    {
        if (q.x <= Math.max(p.x, r.x) && q.x >= Math.min(p.x, r.x) &&
                q.y <= Math.max(p.y, r.y) && q.y >= Math.min(p.y, r.y))
            return true;

        return false;
    }

    // To find orientation of ordered triplet (p, q, r).
// The function returns following values
// 0 --> p, q and r are colinear
// 1 --> Clockwise
// 2 --> Counterclockwise
    static int orientation(Point p, Point q, Point r)
    {
        // See https://www.geeksforgeeks.org/orientation-3-ordered-points/
        // for details of below formula.
        int val = (q.y - p.y) * (r.x - q.x) -
                (q.x - p.x) * (r.y - q.y);

        if (val == 0) return 0; // colinear

        return (val > 0)? 1: 2; // clock or counterclock wise
    }

    // The main function that returns true if line segment 'p1q1'
// and 'p2q2' intersect.
    static boolean doIntersect(Point p1, Point q1, Point p2, Point q2)
    {
        // Find the four orientations needed for general and
        // special cases
        int o1 = orientation(p1, q1, p2);
        int o2 = orientation(p1, q1, q2);
        int o3 = orientation(p2, q2, p1);
        int o4 = orientation(p2, q2, q1);

        // General case
        if (o1 != o2 && o3 != o4)
            return true;

        // Special Cases
        // p1, q1 and p2 are colinear and p2 lies on segment p1q1
        if (o1 == 0 && onSegment(p1, p2, q1)) return true;

        // p1, q1 and q2 are colinear and q2 lies on segment p1q1
        if (o2 == 0 && onSegment(p1, q2, q1)) return true;

        // p2, q2 and p1 are colinear and p1 lies on segment p2q2
        if (o3 == 0 && onSegment(p2, p1, q2)) return true;

        // p2, q2 and q1 are colinear and q1 lies on segment p2q2
        if (o4 == 0 && onSegment(p2, q1, q2)) return true;

        return false; // Doesn't fall in any of the above cases
    }

    // Driver code
    public static void main(String[] args)
    {
        Point p1 = new Point(10,0);
        Point q1 =new Point(0,10);
        Point p2 = new Point(0,10);
        Point q2 = new Point(0,0);

        if(doIntersect(p1, q1, p2, q2))
            System.out.println("Yes");
        else
            System.out.println("No");

        p1 = new Point(10, 1); q1 = new Point(0, 10);
        p2 = new Point(0, 0); q2 = new Point(10, 10);
        if(doIntersect(p1, q1, p2, q2))
            System.out.println("Yes");
        else
            System.out.println("No");

        p1 = new Point(-5, -5); q1 = new Point(0, 0);
        p2 = new Point(1, 1); q2 = new Point(10, 10);;
        if(doIntersect(p1, q1, p2, q2))
            System.out.println("Yes");
        else
            System.out.println("No");
    }
}

