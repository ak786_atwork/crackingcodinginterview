package season2.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class ReflectionDemo3 {

    public static void main(String[] args) {

        Class c = Employee.class;

        Constructor[] constructors = c.getDeclaredConstructors();

        for (Constructor f : constructors) {
            System.out.println("name "+f.getName());
            System.out.println("modifiers "+ Modifier.toString(f.getModifiers()));

            Class[] cls = f.getParameterTypes();
            System.out.println("parameter types ------");

            for (Class parameter : cls) {
                System.out.println("name "+parameter.getName());
            }

            System.out.println("");

            Class[] cls1 = f.getExceptionTypes();
            System.out.println("exceptions ------");

            for (Class exception : cls1) {
                System.out.println("name "+exception.getName());
            }

        }


    }
}
