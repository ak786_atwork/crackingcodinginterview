package season2.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class ReflectionDemo2 {

    public static void main(String[] args) {

        Employee e = new Employee();

        Class c = e.getClass();
        Method[] methods = c.getDeclaredMethods();

        for (Method f : methods) {
            System.out.println("name "+f.getName());
            System.out.println("return type "+f.getReturnType().getName());
            System.out.println("modifiers "+ Modifier.toString(f.getModifiers()));

            Class[] cls = f.getParameterTypes();
            System.out.println("parameter types ------");

            for (Class parameter : cls) {
                System.out.println("name "+parameter.getName());
            }

            System.out.println("");

            Class[] cls1 = f.getExceptionTypes();
            System.out.println("exceptions ------");

            for (Class exception : cls1) {
                System.out.println("name "+exception.getName());
            }

        }


    }
}
