package season2.reflection;

import java.io.Serializable;
import java.lang.reflect.Modifier;

public class ReflectionDemo {

    public static void main(String[] args) throws ClassNotFoundException {
        Class c = Class.forName("season2.reflection.Employee");

        System.out.println("class name "+c.getName());
        System.out.println("super class name "+c.getSuperclass().getName());

        Class[] interfaces = c.getInterfaces();

        System.out.println("interfaces list :");
        for (Class c2 : interfaces) {
            System.out.println(c2.getName() +" ");
        }

        int i = c.getModifiers();
        System.out.println("access modifiers : "+ Modifier.toString(i));

    }

}
