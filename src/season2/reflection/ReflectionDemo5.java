package season2.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Calendar;



//we can access only private members and modify, but can't access constructor and method which are private or protected
public class ReflectionDemo5 {

    public static void main(String[] args) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {

//        A a = A.class.newInstance();
        Constructor<A>  constructor = A.class.getConstructor();

        A a = constructor.newInstance();
        a.a = 0;

        Method m = A.class.getMethod("fun");

        m.invoke(a,null);
        System.out.println(a.a);
    }

    private static class A {
        private int a = 8;
        protected A() {

        }

        protected void fun() {
            System.out.println("private method");
        }

    }
}
