package season2.reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class ReflectionDemo1 {

    public static void main(String[] args) throws IllegalAccessException {
        Employee e = new Employee();

        Class c = e.getClass();
        Field[] fields = c.getDeclaredFields();

        for (Field f : fields) {
            System.out.println("name "+f.getName());
            System.out.println("type "+f.getType().getName());
            System.out.println("modifiers "+ Modifier.toString(f.getModifiers()));
            System.out.println("value "+f.get(f));
        }

    }

}
