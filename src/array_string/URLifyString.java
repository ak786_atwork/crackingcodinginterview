package array_string;

import java.util.Arrays;
import java.util.Scanner;

public class URLifyString {
    public static void main(String[] args) {
       /* Scanner sc = new Scanner(System.in);
        int test = sc.nextInt();
        sc.nextLine();
        while(test-- > 0){
            String url  = sc.nextLine();

            int len = sc.nextInt();
            sc.nextLine();

            System.out.println(url.trim().replaceAll(" ","%20"));
        }*/

        System.out.println(urlify("mr john smith    ",13));

    }

    public static String urlify(String url, int trueLength) {
        char[] chars = url.toCharArray();

        int i = trueLength-1;
        int spaceCount = 0;
        while (i>=0) {
            if (chars[i] == ' ')
                spaceCount++;
            i--;
        }

        int newLength = trueLength + spaceCount * 2;
        //ending array in case of excess space
        if (newLength < chars.length) {
            chars[newLength] = '\0';
        }

        int index = newLength;
        for (int j = trueLength-1;j>=0;j--) {
            if (chars[j] != ' ') {
                chars[index-1] = chars[j];
                index--;
            } else {
                //add %20
                chars[index-1] = '0';
                chars[index-2] = '2';
                chars[index-3] = '%';
                index -= 3;
            }
        }
        return Arrays.toString(chars);
    }
}
