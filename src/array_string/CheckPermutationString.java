package array_string;

public class CheckPermutationString {

    public static void main(String[] args) {
        String s = "abchd   76";
        String permutedString = "abchd67   ";
        System.out.println((new CheckPermutationString()).isPermutation(s, permutedString));
    }


    //considering unicode char scheme
    public boolean isPermutation(String s, String permutedString) {
        //for permutation lengths should be same
        if (s.length() != permutedString.length())
            return false;

        int[] charCount = new int[256];
        for (int i = 0;i<s.length();i++) {
            charCount[s.charAt(i)] += 1;
            charCount[permutedString.charAt(i)] -= 1;
        }

        //check if all zeros
        for (int i =0;i<charCount.length;i++) {
            if (charCount[i] != 0)
                return false;
        }
        return true;
    }


}
