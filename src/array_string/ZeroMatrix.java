package array_string;

public class ZeroMatrix {
    public static void main(String[] args) {
        int[][] matrix = {
                {0, 1, 2, 0},
                {3, 4, 5, 2},
                {1, 3, 1, 5}
        };

/*        int[][] matrix = {{
                -361,
                -425,
                -367,
                381,
                -264,
                473,
                411,
                -218,
                -376,
                -74,
                -83,
                329,
                -367,
                313,
                -397,
                402,
                -245,
                -437,
                -177,
                -453,
                324,
                142,
                -319,
                160,
                16,
                488,
                -297,
                120,
                -156,
                489,
                91,
                325,
                115,
                180,
                0,
                -193,
                230,
                424,
                198,
                -75,
                333,
                -408,
                425,
                -103,
                -460,
                -188,
                -43,
                268,
                302,
                -173,
                186
        }};*/

//        int[][] matrix = new int[40][41];

/*
        int row = 0;
        row = (1 << 50) | row;
        for (int i = 30;i<63;i++) {
            row = 0;
            row = (1 << i) | row;
            System.out.println( i +" = "+Long.toBinaryString(row));
        }
*/

        printMatrix(matrix);
        Solution.setZeroes(matrix);
        printMatrix(matrix);
    }

    private static void printMatrix(int[][] matrix) {
        System.out.println();
        for (int i = 0; i < matrix.length; i++) {
            System.out.println();
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
        }
    }

    /*this approach totally works fine with matrix of size <= 31 but for more there is problem while shifting bits i think this is
     * a bug*/
    public static void setZeroes(int[][] matrix) {
        long row = 0;
        long col = 0;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {

                //check if it is 0 or not
                if (matrix[i][j] == 0) {
                    //set the row and col
                    row = (1 << i) | row;
                    col = (1 << j) | col;
                }
//                System.out.println("i = "+i+ "row = " + Long.toBinaryString(row).length());
//                System.out.println( "j = "+j+ "col = " + Long.toBinaryString(col).length());
//                System.out.println("row = " + Long.toBinaryString(row));
//                System.out.println("col = " + Long.toBinaryString(col));

            }
        }

        System.out.println("row = " + Long.toBinaryString(row));
        System.out.println("col = " + Long.toBinaryString(col));

        int rowNumber = 0;
        while (row != 0) {
            int j = (int) (row & 1);
            row >>>= 1;
            if (j == 1) {
                for (int i = 0; i < matrix[0].length; i++) {
                    matrix[rowNumber][i] = 0;
                }
            }
            rowNumber++;
        }

        int colNumber = 0;
        while (col != 0) {
            int j = (int) (col & 1);
            col >>>= 1;
            if (j == 1) {
                for (int i = 0; i < matrix.length; i++) {
                    matrix[i][colNumber] = 0;
                }
            }
            colNumber++;
        }

    }


    //submitted on leetcode
    static class Solution {
        public static void setZeroes(int[][] matrix) {
            boolean isFirstRowZero = false;
            boolean isFirstColZero = false;

            for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix[0].length; j++) {
                    if (matrix[i][j] == 0) {
                        //check for the first row and col
                        if (i == 0)
                            isFirstRowZero = true;
                        if (j == 0)
                            isFirstColZero = true;
                        if (i != 0 && j != 0) {
                            //use the first row and col to set an indicator
                            matrix[0][j] = 0;
                            matrix[i][0] = 0;
                        }

                    }
                }
            }

            //make row zero
            for (int i = 1; i < matrix.length; i++) {
                if (matrix[i][0] == 0)
                    nullifyRow(matrix, i);
            }
            //make col zero
            for (int i = 1; i < matrix[0].length; i++) {
                if (matrix[0][i] == 0)
                    nullifyCol(matrix, i);
            }

            if (isFirstColZero)
                nullifyCol(matrix, 0);
            if (isFirstRowZero)
                nullifyRow(matrix, 0);
        }

        private static void nullifyCol(int[][] matrix, int col) {
            for (int i = 0; i < matrix.length; i++) {
                matrix[i][col] = 0;
            }
        }

        private static void nullifyRow(int[][] matrix, int row) {
            for (int i = 0; i < matrix[0].length; i++) {
                matrix[row][i] = 0;
            }
        }
    }
}
