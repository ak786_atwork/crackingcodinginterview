package array_string;

import util.Utils;

//submitted on leetcode
public class MatrixRotation {

    public static void main(String[] args) {
        int[][] matrix = {
                {1, 2},
                {3, 4}
        };


        Utils.printMatrix(matrix);
//        rotateMatrix(matrix);
        rotate(matrix);
        Utils.printMatrix(matrix);

    }


    public static void rotateMatrix(int[][] matrix) {
        //layers
        //elements
        //swap corner elements that will be 4 only

        int startRow;
        int startCol;
        int endRow;
        int endCol;
        int matrixSize = matrix.length;
        int elementNumber = 0;

        for (int layer = 0; layer < matrixSize / 2; layer++) {
            startRow = layer;
            endRow = matrixSize - layer - 1;
            startCol = layer;
            endCol = matrixSize - layer - 1;
            elementNumber = 0;

            //elements
            for (int j = layer; j < matrixSize - layer - 1; j++) {
                shiftElements(matrix, startRow, endRow, startCol, endCol, j, elementNumber++);
            }
        }


    }

    private static void shiftElements(int[][] matrix, int startRow, int endRow, int startCol, int endCol, int currentCol, int elementNumber) {
        //current element will be matrix[startRow][currentCol]
        int temp = matrix[startRow][startCol + elementNumber];
        //shift elements
        matrix[startRow][startCol + elementNumber] = matrix[endRow - elementNumber][startCol];
        matrix[endRow - elementNumber][startCol] = matrix[endRow][endCol - elementNumber];
        matrix[endRow][endCol - elementNumber] = matrix[startRow + elementNumber][endCol];
        matrix[startRow + elementNumber][endCol] = temp;
    }


    //from ctci
    public static void rotate(int[][] matrix) {
        int size = matrix.length;
        for (int layer = 0; layer < size / 2; layer++) {
            int first = layer;
            int last = size - layer - 1;

            for (int i = first; i < last; i++) {
                int offset = i - first;
                int top = matrix[first][i];

                //left -> top
                matrix[first][i] = matrix[last - offset][first];

                //bottom -> left
                matrix[last - offset][first] = matrix[last][last - offset];

                //right->bottom
                matrix[last][last - offset] = matrix[i][last];

                //top -> right
                matrix[i][last] = top;
            }
        }
    }
}
