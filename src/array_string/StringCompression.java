package array_string;

import java.util.Arrays;

public class StringCompression {

    public static void main(String[] args) {
        String[] tests = {"abcd", "aabbc", "aaaaaaaaaaaaaaaaaaaaaaaa", "aabbaa", "abbbbbba"};
        char[] chars;

        for (String test : tests) {
            chars = test.toCharArray();
            System.out.println(test + " len = " + Solution.compress(chars) + "  o/p =" + Arrays.toString(chars));
        }

    }


    /**
     * string s contains only uppercase and a-z letters
     */
    public static String compress(String s) {
        if (s.isEmpty())
            return s;

        char lastChar = s.charAt(0);
        int count = 1;
        StringBuilder sb = new StringBuilder(s);

        for (int i = 1; i < s.length(); i++) {
            if (lastChar == s.charAt(i)) {
                count++;
            } else {
                lastChar = s.charAt(i);
                count = 1;
            }
        }
        return null;
    }

    /**
     * All characters have an ASCII value in [35, 126].
     * 1 <= len(chars) <= 1000.
     */
    private static class Solution {

        public static int compress(char[] chars) {

            char lastChar = chars[0];
            int count = 1;
            int replaceAtIndex = 0;

            for (int i = 1; i < chars.length; i++) {
                if (lastChar == chars[i]) {
                    count++;
                } else {
                    //put last char and count to replaceIndex and increment
                    chars[replaceAtIndex++] = lastChar;

                    if (count != 1) {
                        replaceAtIndex = appendCount1(chars, count, replaceAtIndex);
                    }

                    lastChar = chars[i];
                    count = 1;
                }
            }

            //add last char
            chars[replaceAtIndex++] = lastChar;

            if (count != 1) {
                replaceAtIndex = appendCount1(chars, count, replaceAtIndex);
            }

            return replaceAtIndex;
        }

        private static int appendCount(char[] chars, int count, int index) {
            String s = String.valueOf(count);

            for (int i = 0; i < s.length(); i++) {
                chars[index++] = s.charAt(i);
            }

            return index;
        }

        //optimize
        private static int appendCount1(char[] chars, int count, int index) {
            //1000th place, 100th place, 10th place
            char[] places = new char[4];
            places[0] = (char) (count / 1000 + 48);
            count %= 1000;
            places[1] = (char) (count / 100 + 48);
            count %= 100;
            places[2] = (char) (count / 10 + 48);
            count %= 10;
            places[3] = (char) (count + 48);

            boolean leadingZeros = true;

            for (int i = 0; i < 4; i++) {
                if (places[i] == '0' && leadingZeros)
                    continue;

                leadingZeros = false;
                chars[index++] = places[i];
            }

            return index;
        }

        private static int appendCount2(char[] chars, int count, int index) {

            if (count < 10) {
                //unit digit
                chars[index++] = (char) (count + 48);
            } else if (count < 100) {
                //double digit
                chars[index++] = (char) (count / 10 + 48);
                count %= 10;
                chars[index++] = (char) (count + 48);

            } else if (count < 1000) {
                // three digit
                chars[index++] = (char) (count / 100 + 48);
                count %= 100;
                chars[index++] = (char) (count / 10 + 48);
                count %= 10;
                chars[index++] = (char) (count + 48);

            } else if (count == 1000) {
                //four digit
                chars[index++] = '1';
                chars[index++] = '0';
                chars[index++] = '0';
                chars[index++] = '0';
            }
            return index;
        }
    }

    //from leetcode 100%
    static class BestSolution {
        public int compress(char[] chars) {
            if (chars == null || chars.length == 0) {
                return 0;
            }
            int i = 0, n = chars.length, index = 0;
            while (i < n) {
                int j = i;
                char ch = chars[i];
                while (j < n && chars[j] == chars[i]) {
                    j++;
                }
                int freq = j - i;
                chars[index++] = ch;
                if (freq == 1) {

                } else if (freq < 10) {
                    chars[index++] = (char) (freq + '0');
                } else if (freq > 9) {
                    String st = String.valueOf(freq);
                    for (char c : st.toCharArray()) {
                        chars[index++] = c;
                    }
                }
                i = j;
            }
            return index;
        }
    }
}


