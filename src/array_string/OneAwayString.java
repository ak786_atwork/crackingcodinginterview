package array_string;

public class OneAwayString {

    public static void main(String[] args) {
        String s1 = "pale";
        String s2 = "bale";


//        System.out.println(check(s1,s2));
//        System.out.println(isOneDistanceAway1(s1,s2));
        System.out.println(Solution.isOneEditAway(s1, s2));

    }

    public static boolean check(String s1, String s2) {
        //if difference between two string is greater than 1 then they can't be 1 edit away
        if (Math.abs(s1.length() - s2.length()) > 1)
            return false;

        if (s1.length() >= s2.length())
            return isOneDistanceAway(s1, s2, 0, 0, 0);
        else
            return isOneDistanceAway(s2, s1, 0, 0, 0);
    }

    public static boolean isOneDistanceAway(String s1, String s2, int index1, int index2, int editCount) {
        if (editCount > 1 || index1 > s1.length() || index2 > s2.length())
            return false;
        if (index1 == s1.length() && index2 == s2.length())
            return true;

        if ((index1 < s1.length() && index2 < s2.length()) && s1.charAt(index1) == s2.charAt(index2)) {
            return isOneDistanceAway(s1, s2, index1 + 1, index2 + 1, editCount);
        } else {
            //insert, delete, replace
            boolean tryInserting = isOneDistanceAway(s1, s2, index1 + 1, index2, editCount + 1);
            boolean tryReplacing = isOneDistanceAway(s1, s2, index1 + 1, index2 + 1, editCount + 1);
            boolean tryDeleting = isOneDistanceAway(s1, s2, index1, index2 + 1, editCount + 1);

            return tryInserting || tryReplacing || tryDeleting;
        }

    }

    //o(n) and o(1) my approach
    public static boolean isOneDistanceAway1(String s1, String s2) {
        int[] charCount = new int[26];
        for (int i = 0; i < s1.length(); i++) {
            charCount[s1.charAt(i) - 'a'] += 1;
        }

        for (int i = 0; i < s2.length(); i++) {
            charCount[s2.charAt(i) - 'a'] -= 1;
        }

        int negativeCount = 0;
        int positiveCount = 0;

        for (int i = 0; i < charCount.length; i++) {
            if (charCount[i] < 0)
                negativeCount++;
            else if (charCount[i] > 0)
                positiveCount++;
        }

        if (positiveCount > 1 || negativeCount > 1)
            return false;
        else
            return true;
    }

    static class Solution {
        public static boolean check(String s1, String s2) {
            //if difference between two string is greater than 1 then they can't be 1 edit away
            if (Math.abs(s1.length() - s2.length()) > 1)
                return false;

            if (s1.length() > s2.length())
                return isOneInsertOrRemoveAway(s1, s2);
            else if (s1.length() < s2.length())
                return isOneInsertOrRemoveAway(s2, s1);
            else return isOneReplaceAway(s1,s2);
        }

        private static boolean isOneReplaceAway(String s1, String s2) {
            if (s1.length() != s2.length()) return false;

            boolean edited = false;

            for (int i = 0; i < s1.length(); i++) {
                if (s1.charAt(i) != s2.charAt(i)) {
                    if (!edited)
                        edited = true;
                    else
                        return false;
                }
            }
            return true;
        }

        private static boolean isOneInsertOrRemoveAway(String s1, String s2) {
            //s1 is big string so we will remove from s1
            boolean removed = false;

            int index1 = 0;
            for (int index2 = 0; index2 < s2.length(); index2++, index1++) {
                if (s1.charAt(index1) != s2.charAt(index2)) {
                    if (!removed) {
                        removed = true;
                        --index2;
                    } else {
                        return false;
                    }
                }
            }

            //s1 = pale s2 = pae -- for this we  need index1 == s1.length()
            return !removed || index1 == s1.length();
        }

        public static boolean isOneEditAway(String s1, String s2) {
            //if difference between two string is greater than 1 then they can't be 1 edit away
            if (Math.abs(s1.length() - s2.length()) > 1)
                return false;

            int index1 = 0;
            int index2 = 0;
            boolean foundDifference = false;

            while (index1 < s1.length() && index2 < s2.length()) {
                if (s1.charAt(index1) != s2.charAt(index2)) {
                    if (foundDifference)
                        return false;
                    foundDifference = true;

                    //in case of replacement
                    if (s1.length() == s2.length())
                        index2++;

                } else {
                    index2++;
                }
                index1++;
            }

            return true;
        }
    }
}
