package array_string;

import java.util.BitSet;

public class PalindromePermutation {

    public static void main(String[] args) {
        String[] s = {"aba", "abba", "abc","abbd","ajds"};

        for (String t : s) {
            System.out.println(t+" "+ isPalindromicPermutation(t));
            System.out.println(t+" "+ isPermutation(t));
            System.out.println(t+" "+ isPermutation1(t));
        }
    }


    public static boolean isPalindromicPermutation(String s) {
        int[] count = new int[256];
        for (int i = 0; i < s.length(); i++) {
            count[s.charAt(i)] += 1;
        }

        int oddCount = 0;
        for (int i = 0; i < count.length; i++) {
            if (count[i] % 2 != 0) {
                oddCount++;
            }
        }
        //1. consider even len - even + even = even --oddoount = 0 -fine
        // odd + odd = even - oddCount > 1 means false it can't form palindrome

        // 2. odd len - odd + even - only one char is allowed to be of odd len
        return oddCount <= 1;
    }


    //let's consider only alphabets
    public static boolean isPermutation(String s) {
        int checker = 0;
        int val = 0;
        int oddCount = 0;

        for (int i = 0; i < s.length(); i++) {
            val = s.charAt(i) - 'a';
            if ((checker & (1 << val)) > 0) {
                //means char has already encountered, subtract its occurence
                checker -= (1 << val);
                oddCount--;
            } else {
                oddCount++;
                checker |= (1 << val);
            }
        }
        return oddCount <= 1;
    }

    //let's consider only alphabets
    public static boolean isPermutation1(String s) {
        int checker = 0;
        int val = 0;

        for (int i = 0; i < s.length(); i++) {
            val = s.charAt(i) - 'a';
            if ((checker & (1 << val)) > 0) {
                //means char has already encountered, subtract its occurence
                checker -= (1 << val);
            } else {
                checker |= (1 << val);
            }
        }
        //checker = 1 in case of only one odd char & checker == 0 if string is even
        return checker == 0 || (checker & (checker-1)) == 0;
    }


    //have to check copied
    public static boolean hasPalindrome(String s) {
        BitSet occurrences = new BitSet();

        for (int i = 0; i < s.length(); i++) {
            occurrences.flip(Character.getNumericValue(s.charAt(i)));
        }

        return occurrences.stream().count() <= 1;
    }
}
