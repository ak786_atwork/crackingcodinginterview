package LinkedList;

import util.Node;

public class LinkedListPermutation {

    public static void main(String[] args) {
        Node head = new Node(1);
        head.next = new Node(1);
//        head.next.next = new Node(2);
//        head.next.next.next = new Node(1);
//        head.next.next.next.next = new Node(1);

        System.out.println(checkPermutation(head));
    }



    public static boolean checkPermutation(Node head) {
        if (head == null || head.next == null)
            return true;

        Node temp =null;
        Node previous =null;
        Node slowPtr = head;
        Node fastPtr = head;

        while (fastPtr != null && fastPtr.next != null) {
            fastPtr = fastPtr.next.next;

            temp = slowPtr.next;
            slowPtr.next = previous;
            previous = slowPtr;

            slowPtr = temp;
        }

        //odd even check
        if (fastPtr == null){
            //even len
            return check(previous, slowPtr);
        } else {
            return check(previous, slowPtr.next);
        }

    }

    private static boolean check(Node head1, Node head2) {
        head1.printNodes();
        head2.printNodes();
        while (head1 != null || head2 != null) {
            if (head1.data != head2.data)
                return false;
            head1 = head1.next;
            head2 = head2.next;
        }
        return true;
    }
}
