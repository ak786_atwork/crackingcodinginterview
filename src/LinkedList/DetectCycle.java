package LinkedList;

import util.Node;

//submitted on leetcode
public class DetectCycle {


    public Node detectCycle(Node head) {
        //no cycle
        if (head == null || head.next == null)
            return null;

        Node sptr = head;
        Node fptr = head;
        boolean start = true;

        while (fptr != null && fptr.next != null && (sptr != fptr || start)) {
            sptr = sptr.next;
            fptr = fptr.next.next;
            start = false;
        }

        //no cycle, loop terminated due to null
        if (fptr != sptr)
            return null;

        while (head != sptr) {
            sptr = sptr.next;
            head = head.next;
        }
        return sptr;
    }
}
