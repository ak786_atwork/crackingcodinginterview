package LinkedList;

import util.Node;

public class FindKthFromLast {

    public static void main(String[] args) {
        Node head = new Node(1);
        head.next = new Node(2);
        head.next.next = new Node(3);
        head.next.next.next = new Node(4);

        System.out.println(getItem(head, 0));
        System.out.println(getItem(head, 1));
        System.out.println(getItem(head, 2));
        System.out.println(getItem(head, 3));
        System.out.println(getItem(head, 4));
        System.out.println(getItem(head, 5));
        System.out.println(getItem(head, 6));
    }

    public static int getItem(Node head, int k) {
        if (head == null || k == 0)
            return -1;
        Node fastNode = head;
        Node slowNode = head;

        int count = k;
        //fast forward k node
        while (count > 0) {
            // checking if n-k node exists
            if (fastNode == null)
                return -1;
            fastNode = fastNode.next;
            count--;
        }

        while (fastNode != null) {
            fastNode = fastNode.next;
            slowNode = slowNode.next;
        }

        return slowNode.data;
    }
}
