package LinkedList;

import util.Node;

import java.util.HashSet;

//submitted on geeksforgeeks
public class RemoveDupFromUnsortedLinkedList {

    public static void main(String[] args) {
        Node head = new Node(3);
        head.next = new Node(3);
        head.next.next = new Node(3);

        (new RemoveDupFromUnsortedLinkedList()).removeDuplicates(head).printNodes();
        (new RemoveDupFromUnsortedLinkedList()).removeDuplicates1(head).printNodes();
    }

    // Function to remove duplicates from the given linked list
    public Node removeDuplicates(Node head) {
        HashSet<Integer> set = new HashSet<>();
        Node currentPtr = head;
        Node previousPtr = null;
        while (currentPtr != null) {
            if (set.contains(currentPtr.data)) {
                //remove node
                previousPtr.next = currentPtr.next;
                currentPtr = previousPtr.next;
            } else {
                //add data to set and update pointers
                set.add(currentPtr.data);
                previousPtr = currentPtr;
                currentPtr = currentPtr.next;
            }
        }
        return head;
    }

    //extra space not allowed and number range is 0-30
    public Node removeDuplicates1(Node head) {
        int checker = 0;
        Node currentPtr = head;
        Node previousPtr = null;

        while (currentPtr != null) {
            if ((checker & (1 << currentPtr.data)) > 0) {
                //means number already exists
                // delete the current node and update pointers
                previousPtr.next = currentPtr.next;
                currentPtr = previousPtr.next;
            } else {
                checker |= 1 << currentPtr.data;
                previousPtr = currentPtr;
                currentPtr = currentPtr.next;
            }
        }

        return head;
    }
}




/**
 * Approaches
 * 1. Brute force - comparing each elements in list and deleting if found duplicate using two pointers - O(n2)
 * 2. Using Set to track elements O(n)
 * 3. Using Bit vector technique - range 0-30 for int O(n)
 * */
