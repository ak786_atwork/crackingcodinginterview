package LinkedList;

import util.Node;

// submitted on leetcode
public class DeleteNode {

    public static void main(String[] args) {
        Node head = new Node(1);
        head.next = new Node(2);
        head.next.next = new Node(3);
        head.next.next.next = new Node(4);

        delete(head.next.next.next);

        head.printNodes();
    }

    public static void delete(Node node) {
        node.data = node.next.data;
        node.next = node.next.next;
    }
}
