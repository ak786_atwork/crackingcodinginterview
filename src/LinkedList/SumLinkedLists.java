package LinkedList;

import util.Node;


//submitted on leetcode
public class SumLinkedLists {


    public static Node sum(Node head1, Node head2) {
        if (head1 == null) return head2;
        if (head2 == null) return head1;

        int carry = 0;
        Node resultHead = head1;
        Node lastNode = null;
        int sum = 0;

        while (head1 != null && head2 != null) {
            sum = head1.data + head2.data + carry;
            head1.data = sum % 10;
            carry = sum / 10;

            lastNode = head1;
            head1 = head1.next;
            head2 = head2.next;
        }

        head1 = head1 == null ? head2 : head1;
        lastNode.next = head1;

        while (head1 != null) {
            sum = carry + head1.data;
            head1.data = sum % 10;
            carry = sum / 10;

            lastNode = head1;
            head1 = head1.next;
        }

        if (carry != 0)
            lastNode.next = new Node(carry);

        return resultHead;
    }
}
