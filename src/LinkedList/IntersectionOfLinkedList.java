package LinkedList;

import util.Node;
//submitted on leetcode
public class IntersectionOfLinkedList {

    public static void main(String[] args) {
        Node head1 = new Node(2);
        head1.next = new Node(6);
        head1.next.next = new Node(4);

        Node head2 = new Node(1);
        head2.next = new Node(5);
        head2.next.next = head1.next.next;

        System.out.println(head2.next.next == getIntersectionNode(head1, head2));
    }


    public static Node getIntersectionNode(Node head1, Node head2) {
        if (head1 == null || head2 == null) return null;

        Node temp1 = head1;
        Node temp2 = head2;
        int updateAllowed = 2;

        while (temp1 != temp2) {
            temp1 = temp1.next;
            temp2 = temp2.next;

            //equal linkedlist with no intersection
            if (temp1 == null && temp2 == null)
                return null;

            //no intersection
            if (updateAllowed < 0)
                return null;
            else {
                if (temp1 == null) {
                    updateAllowed--;
                    temp1 = head2;
                }

                if (temp2 == null) {
                    updateAllowed--;
                    temp2 = head1;
                }
            }
        }
        return temp1;
    }

    private static class Solution {
        public Node getIntersectionNode(Node head1, Node head2) {
            if (head1 == null || head2 == null)
                return null;

            int len1 = 0;
            int len2 = 0;
            Node temp1 =head1;
            Node temp2 =head2;
            Node tail1 = null;
            Node tail2 = null;

            while (temp1 != null || temp2 != null) {
                if (temp1 != null) {
                    len1++;
                    tail1 = temp1;
                    temp1 = temp1.next;
                }

                if (temp2 != null) {
                    len2++;
                    tail2 = temp2;
                    temp2 = temp2.next;
                }
            }

            //no intersection
            if (tail1 != tail2)
                return null;

            int diff = len1 - len2;

            if (diff == 0) {
                return findCommonNode(head1,head2);
            } else if (diff > 0) {
                //len1 is larger, means move this to
                head1 = move(head1,diff);
                return findCommonNode(head1,head2);
            } else {
                // list2 is bigger
                head2 = move(head2, -diff);
                return findCommonNode(head1, head2);
            }
        }

        private Node findCommonNode(Node head1, Node head2) {
            while (head1 != null && head2 != null) {
                if (head1 == head2) {
                    return head1;
                }
                head1 = head1.next;
                head2 = head2.next;
            }
            return null;
        }

        private int findLength(Node root) {
            int count = 0;
            while (root != null) {
                count++;
                root = root.next;
            }
            return count;
        }

        private Node move(Node root, int count) {
            while (count > 0){
                count--;
                root = root.next;
            }
            return root;
        }
    }
}
