package random;

import java.util.Random;

public class RandomPickWithWeight {



    /**
     * Your Solution object will be instantiated and called as such:
     * Solution obj = new Solution(w);
     * int param_1 = obj.pickIndex();
     */


    private static class Solution {
        private int[] presum;

        private Random random;

        public Solution(int[] w) {
            random = new Random();
            presum = new int[w.length + 1];

            for (int i = 1; i <= w.length; i++) {
                presum[i] = presum[i - 1] + w[i - 1];
            }
        }

        public int pickIndex() {
            int number = random.nextInt(presum[presum.length - 1]);

            int start = 1;
            int end = presum.length - 1;

            while (start <= end) {
                int mid = start + (end - start) / 2;
                if (number < presum[mid])
                    end = mid - 1;
                else
                    start = mid + 1;
            }
            return start - 1;
        }
    }

}
