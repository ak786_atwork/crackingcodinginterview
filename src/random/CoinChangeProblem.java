package random;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CoinChangeProblem {

    public static void main(String[] args) {
        int amount = 255;
//        int[] coins = {470, 18, 66, 301, 403, 112, 360};
//        int[] coins = {186,419,83,408};
        int[] coins = {2,5,7};
//        int[] coins = {1, 2, 5};
//        System.out.println((new CoinChangeProblem()).coinChange(coins, amount));
//        System.out.println((new CoinChangeProblem()).findMinCoins(amount, coins, 0, 0, new HashMap<String, Integer>()));
//        System.out.println((new CoinChangeProblem()).findMinCoins1(amount, coins, 0, 0, new HashMap<String, Integer>()));
//        System.out.println((new CoinChangeProblem()).check(255,0, coins, 0));

        System.out.println((new CoinChangeProblem.Solution()).coinChange(coins,amount));
    }

    public int coinChange(int[] coins, int amount) {
//        Arrays.sort(coins);
        return findCoins(amount, coins, coins.length - 1, 0, 0);
    }

    public int findCoins(int amount, int[] coins, int index, int currentAmount, int totalCoins) {

        System.out.println("amount = " + amount + " coin = " + index + " current amount = " + currentAmount + " totaal coins = " + totalCoins);
        int temp = -1;
        if (amount == currentAmount)
            return totalCoins;
        if (index < 0)
            return -1;
        if (amount > currentAmount) {
//            if ((amount-currentAmount) % coins[index] == 0)
//                return totalCoins;
            //start trying from max possible value
            int count = (amount - currentAmount) / coins[index];
            int minCoins = Integer.MAX_VALUE;
            while (count >= 0) {
                temp = findCoins(amount, coins, index - 1, currentAmount + coins[index] * count, totalCoins + count);
                if (temp < minCoins)
                    minCoins = temp;
//                    count = -1;
//                    break;

                count--;
            }
        }
        return temp;
    }

    public int check(int amount, int currentAmount, int[] coins, int totalCoins) {
        System.out.println("amount = " + amount + "  current amount = " + currentAmount + " totaal coins = " + totalCoins);


        if (amount == currentAmount)
            return totalCoins;
        else if (currentAmount > amount)
            return Integer.MAX_VALUE;
        int minCoins = Integer.MAX_VALUE;
        int temp = -1;
        int[] result = new int[coins.length];
        for (int i = coins.length - 1; i >= 0; i--) {
            result[i] = check(amount, currentAmount + coins[i], coins, totalCoins + 1);
//            if (temp < minCoins)
//                minCoins = temp;
        }
        return getMin(result);
    }

    private int getMin(int[] result) {
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < result.length; i++) {
            if (min > result[i]) {
                min = result[i];
            }
        }
        return min;
    }

    public int findMinCoins(int amount, int[] coins, int totalCoins, int index, HashMap<String, Integer> map) {
        if (amount == 0)
            return totalCoins;
        else if (amount < 0 || index >= coins.length)
            return Integer.MAX_VALUE;

        String key1 = (amount - coins[index]) + ":" + index;
        String key2 = amount + ":" + (index + 1);

        if (!map.containsKey(key1)) {
            map.put(key1, findMinCoins(amount - coins[index], coins, totalCoins + 1, index, map));
        }

        if (!map.containsKey(key2)) {
            map.put(key2, findMinCoins(amount, coins, totalCoins, index + 1, map));
        }

        return Math.min(map.get(key1), map.get(key2));

    }

    public int findMinCoins1(int amount, int[] coins, int totalCoins, int index, HashMap<String, Integer> map) {
        if (amount == 0)
            return 0;
        else if (amount < 0 || index >= coins.length)
            return Integer.MAX_VALUE;

        String key1 = (amount - coins[index]) + ":" + index;
        String key2 = amount + ":" + (index + 1);

        if (!map.containsKey(key1)) {
            map.put(key1, 1 + findMinCoins(amount - coins[index], coins, totalCoins + 1, index, map));
        }

        if (!map.containsKey(key2)) {
            map.put(key2, findMinCoins(amount, coins, totalCoins, index + 1, map));
        }

        return Math.min(map.get(key1), map.get(key2));
    }

    //from youtube submitted on leetcode
    static class Solution {
        public int coinChange(int[] coins, int amount) {
            int[] dp = new int[amount + 1];
            Arrays.fill(dp, Integer.MAX_VALUE);
            dp[0] = 0;


            for (int coinIndex = 0; coinIndex < coins.length; coinIndex++) {
                for (int i = 1; i <= amount; i++) {
                    if ((i - coins[coinIndex]) >= 0) {
                        if (dp[i-coins[coinIndex]] != Integer.MAX_VALUE)
                            dp[i] = Math.min(dp[i], 1 + dp[i - coins[coinIndex]]);
                    }
                }
            }
            return dp[dp.length - 1] == Integer.MAX_VALUE? -1:dp[dp.length-1];
        }
    }
//TODO check this code
    //fastest solution from leetcode
    static class FastSolution {
        int minCount = Integer.MAX_VALUE;

        public int coinChange(int[] coins, int amount) {
            Arrays.sort(coins);
            count(amount, coins.length - 1, coins, 0);
            return minCount == Integer.MAX_VALUE ? -1 : minCount;
        }

        void count(int amount, int index, int[] coins, int count) {
            if(amount % coins[index] == 0) {
                int newCount = count + amount / coins[index];
                if(newCount < minCount)
                    minCount = newCount;
            }

            if(index == 0)
                return;

            for (int i = amount / coins[index]; i >= 0; i--) {
                int newAmount = amount - i * coins[index];
                int newCount = count + i;

                int nextCoin = coins[index-1];
                if(newCount + (newAmount + nextCoin -1) / nextCoin >= minCount)
                    break;

                count(newAmount, index - 1, coins, newCount);
            }
        }
    }
}

