package random;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class TwoCityScheduling {

    public static void main(String[] args) {
        int[][] costs = {
                {10, 20},
                {30, 200},
                {400, 50},
                {30, 20}
        };

        System.out.println((new Solution2()).twoCitySchedCost(costs));
    }

    private static class Solution {
        static int minCost = Integer.MAX_VALUE;

        public int twoCitySchedCost(int[][] costs) {
            schedule(0, 0, 0, 0, costs, costs.length / 2);
            return minCost;
        }

        public void schedule(int person, int personInA, int personInB, int currentCost, int[][] costs, int totalPerson) {
            if (personInA == totalPerson && personInB == totalPerson) {
                if (minCost > currentCost)
                    minCost = currentCost;
                return;
            }

            if (personInA > totalPerson || personInB > totalPerson)
                return;

            //sending person to city A
            schedule(person + 1, personInA + 1, personInB, currentCost + costs[person][0], costs, totalPerson);
            //sending person to city B
            schedule(person + 1, personInA, personInB + 1, currentCost + costs[person][1], costs, totalPerson);
        }
    }

    //from comment
    private static class Solution1 {

        public int twoCitySchedCost(int[][] costs) {

            int totalCost = 0;
            for (int[] cost : costs)
                totalCost += cost[0];

            int[] refund = new int[costs.length];
            for (int i = 0; i < costs.length; i++)
                refund[i] = costs[i][1] - costs[i][0];

            Arrays.sort(refund);

            for (int i = 0; i < refund.length / 2; i++)
                totalCost += refund[i];

            return totalCost;
        }
    }


    //from comment
    private static class Solution2 {

        private static Comparator comparator = (o1, o2) -> {
            Integer[] a = (Integer[]) o1;
            Integer[] b = (Integer[]) o2;
            return (a[1] - a[0]) - (b[1] - b[0]);
        };

        public int twoCitySchedCost(int[][] costs) {
            //sorting according to profit(ascending order) of choosing city A over city B
            Arrays.sort(costs, (a, b) -> (a[1] - a[0]) - (b[1] - b[0]));

            int totalCost = 0;
            //first taking cityB cost and then city A
            for (int i = 0; i < costs.length; i++)
                totalCost += (i < costs.length / 2) ? costs[i][1] : costs[i][0];

            return totalCost;
        }
    }
}
