package math_logic;

public class EggDroppingSimulation {

    private int totalFloors;
    private int interval;
    private int countDrops;
    private int breakingPoint;

    public EggDroppingSimulation(int totalFloors) {
        this.totalFloors = totalFloors;
        //generalization interval = x(x+1)/2 = totalFloors
        interval = (int) Math.ceil((-1+Math.sqrt(1+8*totalFloors))/2);
        System.out.println("interval = "+interval);
    }

    public int getEggDrops(int floors) {
        countDrops = 0;
        breakingPoint = floors;
        int currentInterval = interval;
        int previousFloor = 0;
        int egg1 = currentInterval;

        // drop egg1 at decreasing intervals
        while (egg1 <= floors && !isEggBroken(egg1)) {
            currentInterval--;
            previousFloor = egg1;
            egg1 = previousFloor + currentInterval;
        }

        //drop egg at 1 unit increments
        int egg2 = previousFloor + 1;
        while (egg2 < floors && !isEggBroken(egg2)) {
            egg2++;
        }

        return countDrops;
    }

    public boolean isEggBroken(int floor) {
        countDrops++;
        return floor >= breakingPoint;
    }

    static class Driver {
        public static void main(String[] args) {
            EggDroppingSimulation eggDroppingSimulation = new EggDroppingSimulation(100);
            System.out.println(eggDroppingSimulation.getEggDrops(56));
            System.out.println(eggDroppingSimulation.getEggDrops(60));
        }
    }
}
