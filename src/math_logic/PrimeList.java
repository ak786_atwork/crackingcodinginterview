package math_logic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//The sieve of eratosthenes algo
public class PrimeList {

    public static void main(String[] args) {
        System.out.println((new PrimeList()).generatePrimes(100));
    }

    public List<Integer> generatePrimes(int n) {
        boolean[] primesFlag = new boolean[n + 1];
        Arrays.fill(primesFlag, true);
        primesFlag[0] = false;
        primesFlag[1] = false;

        for (int i = 2; i < n; i++) {
            if (primesFlag[i]) {
                crossMultiples(i, primesFlag,n);
            }
        }
        return getList(primesFlag);
    }

    private List<Integer> getList(boolean[] primesFlag) {
        List<Integer> primesList = new ArrayList<>();
        for (int i = 2;i<primesFlag.length;i++) {
            if (primesFlag[i])
                primesList.add(i);
        }
        System.out.println(primesList.size());
        return primesList;
    }

    private void crossMultiples(int prime, boolean[] primesFlag, int n) {
        for (int i = prime * prime; i <= n; i += prime) {
            primesFlag[i] = false;
        }
    }
}
