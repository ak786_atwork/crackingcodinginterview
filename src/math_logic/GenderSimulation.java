package math_logic;

import java.util.Random;

public class GenderSimulation {

    public static void main(String[] args) {
        int[] families = {1,2,5,10,15,21,28,100, 500, 1000, 2000, 2000, 100000};

        for (int family : families)
            System.out.println("population = "+family+"  ratio = "+runFamilies(family));
    }

    public static double runFamilies(int n) {
        int boys = 0;
        int girls = 0;

        for (int i = 0;i<n;i++) {
            int[] genders = runOneFamily();
            girls += genders[0];
            boys += genders[1];
        }

        return girls/(double)(girls + boys);
    }

    private static int[] runOneFamily() {
        Random random = new Random();
        int girls = 0;
        int boys = 0;

        while (girls != 1) {
            if (random.nextBoolean()){
                girls++;
            } else {
                boys++;
            }
        }
        return new int[]{girls, boys};
    }


}
